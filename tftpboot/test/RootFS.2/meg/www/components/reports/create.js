var chart;
var chartData = [];

function generateChartGraph() {  
    var len = Reports.json.length;
    console.log("length of data:" + len.toString());
    if (len <= 2)
    {
        Main.alert(language.report_no_data);
        return 1;
    }
    
    var clintDate = new Date();
    var gmtOffClint = clintDate.getTimezoneOffset()
    gmtOffClint = -(gmtOffClint*60);
    var gmtOffServer = parseInt(Reports.json[0].gmtoff);
    var gmtOffDiff = (gmtOffClint) - (gmtOffServer);

    var date = new Date();
    for (i = 0; i < len; i++)
    {
        var utime = parseInt(Reports.json[i].year);
        date = new Date((utime * 1000) - (gmtOffDiff * 1000));
        Reports.json[i].year = String(date);
      //  console.log(String(i) + "utime->" + String(utime) + "  year->" + date)
    }
    // SERIAL CHART    
    chart = new AmCharts.AmSerialChart();

    chart.pathToImages = "images/";
    chart.dataProvider = Reports.json;
    chart.categoryField = "year";    
    chart.startDuration = 0.5;
    chart.addListener("dataUpdated", zoomChart);

    var predefinedColors = 
            ["#1E5799", //Blue
            "#73880a", //Green
            "#a849a3", //purple
            "#cc0000", //lime
            "#006e2e", //brown
            "#28343b", //black
            ];

    // category
    var categoryAxis = chart.categoryAxis;
    categoryAxis.parseDates = true; // as our data is date-based, we set parseDates to true
    categoryAxis.minPeriod = "mm"; // our data is daily, so we set minPeriod to DD
    categoryAxis.dashLength = 0;
    categoryAxis.gridAlpha = 0.15;
    categoryAxis.axisColor = "#DADADA"; 
    categoryAxis.gridPosition = "start";
    categoryAxis.position = "top";

    var chartCursor = new AmCharts.ChartCursor();
    chartCursor.cursorPosition = "mouse";
    chartCursor.categoryBalloonDateFormat = "JJ:NN, DD MMMM YYYY";
    chart.addChartCursor(chartCursor);
    
    var chartScrollbar = new AmCharts.ChartScrollbar();
    chart.addChartScrollbar(chartScrollbar);
    
    var legend = new AmCharts.AmLegend();
    legend.marginLeft = 110;
    legend.useGraphSettings = true;
    chart.addLegend(legend);
    var average = 5;
    var fatih = "#e10000";

    for(var i = 0; i < Reports.selectnames.length; i++) {

        var sensortype = Reports.selectnames[i].sensortype;

        var valueAxis = new AmCharts.ValueAxis();
        valueAxis.axisColor = predefinedColors[i];
        valueAxis.offset = 40 * i;
        valueAxis.axisThickness = 1;
        valueAxis.gridAlpha = 0.15;
        valueAxis.dashLength = 0;
        if(sensortype == 2 || sensortype == 6) {
            valueAxis.minimum = -1;
            valueAxis.maximum = 2;
            valueAxis.integersOnly = true;
        }
        chart.addValueAxis(valueAxis);

        var graph = new AmCharts.AmGraph();
        graph.type = "smoothedLine"; // this line makes the graph smoothed line.
        graph.valueAxis = valueAxis; // we have to indicate which value axis should be used
        graph.lineColor = predefinedColors[i]; //"#d1655d";
        graph.negativeLineColor = "#999";
        graph.title = Reports.selectnames[i].name + " " + Reports.selectnames[i].hwaddr;
        graph.balloonText= Reports.selectnames[i].name + "<br><b>[[value]] " + Reports.selectnames[i].unit + "</b>",
        graph.valueField = "sensor" + Reports.selectnames[i].value;
        graph.bullet = "round";
        graph.bulletSize = 8;
        graph.hideBulletsCount = 50;
        graph.bulletBorderThickness = 1;
        graph.colorField = "customBullet" + Reports.selectnames[i].value;
        chart.addGraph(graph);
    }

    
    chart.exportConfig = {  
        menuTop: 'auto',
        menuLeft: 'auto',
        menuRight: '21px',
        menuBottom: '21px',
        menuItems: [{
            textAlign: 'center',
            onclick: function () {},
            icon: 'images/export.png',
            iconTitle: language.icon_title,
            title: language.export,
            items: [{
                title: 'JPG',
                format: 'jpg'
            }, {
                title: 'PNG',
                format: 'png'
            }, {
                title: 'SVG',
                format: 'svg',
            }, {
                title: 'PDF',
                format: 'pdf'
            }]
        }],
        menuItemStyle: {
            backgroundColor: '#ffffff',
            rollOverBackgroundColor: '#EFEFEF',
            color: '#000000',
            rollOverColor: 'E10000',
            paddingTop: '5px',
            paddingRight: '5px',
            paddingBottom: '5px',
            paddingLeft: '5px',
            marginTop: '0px',
            marginRight: '0px',
            marginBottom: '0px',
            marginLeft: '0px',
            textAlign: 'left',
            textDecoration: 'none'
        }
    };

    chart.write("chartdiv");
    chart.zoomOut();

    return 0;

};

function zoomChart() {
    chart.zoomToIndexes(0, 50);
}