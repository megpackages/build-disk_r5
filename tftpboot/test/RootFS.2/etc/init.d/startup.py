#!/usr/bin/python 

import os
import subprocess
from time import sleep
canList = [
		'modprobe can-raw' , 
		'modprobe ti_hecc' ,  
		'ip link set can0 down', 
		'canconfig can0 bitrate 52000 sjw 2 phase-seg1 8 phase-seg2 7 prop-seg 8',
		'ip link set can0 up'
		]

modulesList = [
			"insmod /home/kernelObjects/cdc-acm.ko" , 
			"insmod /home/kernelObjects/usbserial.ko vendor=0x22b8 product=0x2d93" , 
			"insmod /home/kernelObjects/mos7840.ko",
			"insmod /home/kernelObjects/pca953x.ko"
			]

for module in modulesList:
	os.system(module)

for i in canList:
	os.system(i)


