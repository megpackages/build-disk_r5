import sqlite3, sys
db = sqlite3.connect( "/media/config/SPXconfig.sqlite" )  
c = db.cursor()
#c.execute( "select * from sensors" )
c.execute( sys.argv[1] )
result = c.fetchall()
resultstr=""
if len(result) == 1 and len(result[0]) == 1:
    print result[0][0]
else :
    for row in range( 0, len( result ) ):
        if row != 0:
            resultstr += "#"
        for column in range( 0, len( result[row] ) ):
            if column != 0:
                resultstr += "$"
            resultstr += str( result[row][column] )
    print resultstr

db.commit()
db.close()

def test(*farg, **kw):
    for i in farg:
        print "farg:", i
    for k in kw:
        print "kw[", k, "]:", kw[k]