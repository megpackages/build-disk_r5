Settings.gsmData = {};

Settings.callGsm = function() {	
	$("div#settings div.form").html("");
	var html = '<div class="alert"></div>'
				+ '<ul class="element" id="gsmform" style="display:none;">'
				+	'<li id="type"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="enable"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="server"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="retry"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="pingperiod"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="ppptime"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="ip"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="username"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="password"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="apn"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="buttons"><div class="left"></div><div class="right"></div></li>'
				+ '</ul>';
	$("div#settings div.form").html(html);
	Settings.getGsm();
};

Settings.getGsm = function() {
	var data = $.getJSON("http://" + hostName + "/cgi-bin/networksettings.cgi?getPppSettings");	 
	data.success(function() {
		Settings.gsmData = jQuery.parseJSON(data.responseText);
		console.log(Settings.gsmData);
		Settings.createGsmForm();
		Main.unloading();
	});
	data.error(function(jqXHR, textStatus, errorThrown){
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
		Main.unloading();
	});
};

Settings.postGsm = function() {
	Main.loading();
	json = JSON.stringify(Settings.gsmData);

	var url = "http://" + hostName + "/cgi-bin/networksettings.cgi?updatePppSettings";
	$.ajax({
	    url : url,
	    type: "POST",
	    data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			Main.unloading();
			//Settings.callSmtp();
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Settings.createGsmForm = function() {
	var item = Settings.gsmData;

	var primary_interface = item.primary_interface;
    var ppp_enabled = item.ppp_enabled;
	var ping_server_ip = item.ping_server_ip;
	var ping_period = item.ping_period;
	var max_backup_time = item.max_backup_time;
	var static_ip = item.static_ip;
	var username = item.username;
	var password = item.password;
	var apn = item.apn;
	var retry = item.max_ping_retry_count;


	if(item.primary_interface == undefined) {
		primary_interface = language.Settings_gsm_eth;
		ppp_enabled = 0;
		ping_server_ip = "";
		ping_period = "";
		max_backup_time = "";
		static_ip = "";
		username = "";
		password = "";
		apn = "";
		retry = "";
	}

	var typeSelect = document.createElement("select");
	typeSelect.setAttribute("id","typeSelect");
	typeSelect.setAttribute("id","type");
	var option0 = document.createElement("option");
	option0.setAttribute("value",language.Settings_gsm_eth);
	var option1 = document.createElement("option");
	option1.setAttribute("value",language.Settings_gsm_ppp);
	option0.innerHTML = language.Settings_gsm_eth;
	option1.innerHTML = language.Settings_gsm_ppp;

	if(primary_interface == language.Settings_gsm_eth) {
		option0.setAttribute("selected","selected");
	} else {
		option1.setAttribute("selected","selected");
	}

	typeSelect.appendChild(option0);
	typeSelect.appendChild(option1);

	$("li#type div.left").html(language.Settings_gsm_type);
	$("li#type div.right").append(typeSelect);

	var enabledInput = document.createElement("input");
	enabledInput.setAttribute("type","checkbox");
	enabledInput.setAttribute("id","enabled");
	if(ppp_enabled == 1) {
		enabledInput.checked = true;
	}
	$("li#enable div.left").html(language.Settings_gsm_enable);
	$("li#enable div.right").append(enabledInput);

	var serverIpInput = document.createElement("input");
	serverIpInput.setAttribute("type","text");
	serverIpInput.setAttribute("id","pingserverip");
	serverIpInput.setAttribute("value",ping_server_ip);
	$("li#server div.left").html(language.Settings_gsm_ping_server_ip);
	$("li#server div.right").append(serverIpInput);


	var retryIpInput = document.createElement("input");
	retryIpInput.setAttribute("type","text");
	retryIpInput.setAttribute("id","retry");
	retryIpInput.setAttribute("value",retry);
	$("li#retry div.left").html(language.Settings_gsm_ping_server_retry);
	$("li#retry div.right").append(retryIpInput);


	var pingPeriodInput = document.createElement("input");
	pingPeriodInput.setAttribute("type","text");
	pingPeriodInput.setAttribute("id","pingperiod");
	pingPeriodInput.setAttribute("value",ping_period);
	$("li#pingperiod div.left").html(language.Settings_gsm_ping_interval);
	$("li#pingperiod div.right").append(pingPeriodInput);



	var backupTimeInput = document.createElement("input");
	backupTimeInput.setAttribute("type","text");
	backupTimeInput.setAttribute("id","maxbackuptime");
	backupTimeInput.setAttribute("value",max_backup_time);
	$("li#ppptime div.left").html(language.Settings_gsm_maxbackuptime);
	$("li#ppptime div.right").append(backupTimeInput);

	var staticIpInput = document.createElement("input");
	staticIpInput.setAttribute("type","text");
	staticIpInput.setAttribute("id","staticip");
	staticIpInput.setAttribute("value",static_ip);
	$("li#ip div.left").html(language.Settings_gsm_staticip);
	$("li#ip div.right").append(staticIpInput);

	var usernameInput = document.createElement("input");
	usernameInput.setAttribute("type","text");
	usernameInput.setAttribute("id","username");
	usernameInput.setAttribute("value",username);
	$("li#username div.left").html(language.Settings_gsm_username);
	$("li#username div.right").append(usernameInput);

	var passwordInput = document.createElement("input");
	passwordInput.setAttribute("type","password");
	passwordInput.setAttribute("id","password");
	$("li#password div.left").html(language.Settings_gsm_password);
	$("li#password div.right").append(passwordInput);

	var apnInput = document.createElement("input");
	apnInput.setAttribute("type","text");
	apnInput.setAttribute("id","apn");
	apnInput.setAttribute("value",apn);
	$("li#apn div.left").html(language.Settings_gsm_apn);
	$("li#apn div.right").append(apnInput);

	var saveInput = document.createElement("input");
	saveInput.setAttribute("type","submit");
	saveInput.setAttribute("id","save");
	saveInput.setAttribute("value", language.Settings_smtp_save);
	$("li#buttons div.left").html("");
	$("li#buttons div.right").append(saveInput);

	$("div#settings ul.element").css("display","block");
};

Settings.validateGsmForm = function(page) {	
	$("div#settings div.alert").html("");
	var alertHtml = $("div#settings div.alert").html();
	$("li#server").removeClass("alert");
	$("li#retry").removeClass("alert");
	$("li#pingperiod").removeClass("alert");
	$("li#ppptime").removeClass("alert");
	$("li#ip").removeClass("alert");
	$("li#username").removeClass("alert");
	$("li#password").removeClass("alert");
	$("li#apn").removeClass("alert");

	var	primary_interface = $("li#type select option:selected").val();
	var	ppp_enabled = $("li#enable input").prop("checked");
	var ping_server_ip = $("li#server input").val();
	var retry = $("li#retry input").val();
	var ping_period = $("li#pingperiod input").val();
	var max_backup_time = $("li#ppptime input").val();
	var static_ip = $("li#ip input").val();
	var username = $("li#username input").val();
	var password = $("li#password input").val();
	var apn = $("li#apn input").val();
	
	var no = /^\d{0-4}$/;

	var iChars = "!@#$%^&*()+=[]\\\';,_-/{}|\":<>?ğĞüÜşŞıİçÇöÖ";
	var trChars = "?ğĞüÜşŞıİçÇöÖ";
	var filter = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/i;
	var validateip = /^\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}$/;
	var value = true;

	if(ping_server_ip.length == 0) {
	    $("li#server").addClass("alert");
	    alertHtml = language.Settings_smtp_error_noempty;
		$("div#settings div.alert").html(alertHtml);
	    value = false;
	} else {
		for (var i = 0; i < ping_server_ip.length; i++) {
		    if (trChars.indexOf(ping_server_ip.charAt(i)) != -1) {
			    $("li#server").addClass("alert");
			    alertHtml = language.Settings_xml_alert_special;
				$("div#settings div.alert").html(alertHtml);
			    $("li#server input").focus();
			    value = false;
		        break;
		    }
		}
	}

	if(ping_period.length == 0) {
	    $("li#pingperiod").addClass("alert");
	    alertHtml = alertHtml + "<br />" + language.Settings_smtp_error_noempty;
		$("div#settings div.alert").html(alertHtml);
	    value = false;
	} else if(!isFinite(ping_period)) {
		$("li#pingperiod").addClass("alert");
		alertHtml = alertHtml + "<br />" + language.Settings_gsm_error_number;
		$("div#settings div.alert").html(alertHtml);			
		value = false;
	}

	if(max_backup_time.length == 0) {
	    $("li#ppptime").addClass("alert");
	    alertHtml = alertHtml + "<br />" + language.Settings_smtp_error_noempty;
		$("div#settings div.alert").html(alertHtml);
	    value = false;
	} else if(!isFinite(max_backup_time)) {
		$("li#ppptime").addClass("alert");
		alertHtml = alertHtml + "<br />" + language.Settings_gsm_error_number;
		$("div#settings div.alert").html(alertHtml);			
		value = false;
	}


	if(retry.length != 0)
	{
		if(!isFinite(retry)) {
				$("li#retry").addClass("alert");
				alertHtml = alertHtml + "<br />" + language.Settings_gsm_error_retry;
				$("div#settings div.alert").html(alertHtml);			
				value = false;
			}
	}
	else
		retry = "2";

	if(static_ip.length !=0 )
	{
		if(!static_ip.match(validateip)) {
		    $("li#ip").addClass("alert");
		    alertHtml = alertHtml + "<br />" + language.Settings_tcpip_iperror;
			$("div#settings div.alert").html(alertHtml);
		    $("li#ip input").focus();
		    value = false;
	    }
	}

	if(username.length != 0)
	{
		for (var i = 0; i < username.length; i++) {
		    if (iChars.indexOf(username.charAt(i)) != -1) {
			    $("li#username").addClass("alert");
			    alertHtml = alertHtml + "<br />" + language.Settings_users_alert_special;
				$("div#settings div.alert").html(alertHtml);
			    $("li#username input").focus();
			    value = false;
		        break;
		    }
		}
	}

	if(apn.length == 0) {
	    $("li#apn").addClass("alert");
	    alertHtml = alertHtml + "<br />" + language.Settings_smtp_error_noempty;
		$("div#settings div.alert").html(alertHtml);
	    $("li#apn input").focus();
	    value = false;
	} else {
		for (var i = 0; i < apn.length; i++) {
		    if (iChars.indexOf(apn.charAt(i)) != -1) {
			    $("li#apn").addClass("alert");
			    alertHtml = alertHtml + "<br />" + language.Settings_users_alert_special;
				$("div#settings div.alert").html(alertHtml);
			    $("li#apn input").focus();
			    value = false;
		        break;
		    }
		}
	}

	if(password.length == 0) {
		password = Settings.gsmData.password;
	}

	if(value) {
		
		if(ppp_enabled == true) {
			ppp_enabled = "1";
		} else {
			ppp_enabled = "0";
		}

		Settings.gsmData = {
			primary_interface: primary_interface,
			ppp_enabled: ppp_enabled,
			ping_server_ip: ping_server_ip,
			ping_period: ping_period,
			max_backup_time: max_backup_time,
			username: username,
			password: password,
			apn: apn,
			max_ping_retry_count: retry
		};
	}

	console.log(Settings.gsmData);

	return value;
};

$(function() {
	$("div#settings").on( "click", "ul#gsmform input#save", function() {
		if(Settings.validateGsmForm()) {
			Settings.postGsm();
		}
	});
});