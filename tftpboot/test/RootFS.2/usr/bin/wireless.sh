#!/bin/sh

iface=`ifconfig -a | grep wlan | cut -d ' ' -f1`
if [ X$iface = X ]
then
	echo LOADING DRIVER
	insmod /boot/libertas_sdio.ko 2>&1 > /dev/null
	echo
	sleep 2
	iface=`ifconfig -a | grep wlan | cut -d ' ' -f1`
fi
echo BRINGING INTERFACE $iface UP
ifconfig $iface up

real_ssid=
while [ X$real_ssid = X ]
do
	echo
	echo AVAILABLE NETWORKS
	echo ##################
	iwlist $iface scan 2&>1 | grep ESSID | cut -d ':' -f 2
	echo
	echo -n Enter the network you want to connect to:\ 
	read ssid
	real_ssid=`iwlist $iface scan 2&>1 | grep ESSID | cut -d ':' -f 2 | grep $ssid | sed 's/"//g'`
done

cp /usr/etc/wpa_supplicant/wpa_psk_conf_header ~/wpa_psk.conf
echo
echo -n Enter passphrase for net ${real_ssid}:" " 
#read passph
wpa_passphrase $real_ssid $passph > /tmp/wpa_psk_net
psk=`grep "psk=" /tmp/wpa_psk_net | grep -v "#psk="`
ssid=`grep "ssid=" /tmp/wpa_psk_net`
echo 	$ssid >> ~/wpa_psk.conf
echo 	$psk >> ~/wpa_psk.conf
echo '}' >> ~/wpa_psk.conf
wpa_supplicant -Dnl80211 -i${iface} -c ~/wpa_psk.conf &
rm /tmp/wpa_psk_net
sleep 2
udhcpc -i $iface
sleep 1


