
Devices.getConnectors = function()
{
    var html =  '<div class="addButtons">'
                    + '<input class="addButton" type="submit" id="moduletree">'
                    +'<input class="addButton" type="submit" id="discovery">'
                    +'<input class="addButton" type="submit" id="addDevice">'
                    +'<input class="addButton" type="submit" id="moveDevice">'
               +'</div>'
               +'<div class="block">'
                    +'<table cellpadding="0" cellspacing="0" border="0" width="100%" id="connectorListHead" class="tableHead">'
                        +'<tr>'
                            +'<td></td>'
                        +'</tr>'
                    +'</table>'
                    +'<div id="connectorList" class="tableDiv">'
                        +'<table cellpadding="0" cellspacing="0" border="0" width="100%">'
                        +'</table>'
                    +'</div>'
                +'</div>'
                +'<div class="space"></div>'
                +'<div class="block">'
                    +'<table cellpadding="0" cellspacing="0" border="0" width="100%" id="portListHead" class="tableHead">'
                        +'<tr>'
                            +'<td></td>'
                        +'</tr>'
                    +'</table>'
                    +'<div id="portList" class="tableDiv">'
                        +'<table cellpadding="0" cellspacing="0" border="0" width="100%">'
                        +'</table>'
                    +'</div>'
                +'</div>'
                +'<div class="space"></div>'
                +'<div class="block">'
                    +'<table cellpadding="0" cellspacing="0" border="0" width="100%" id="deviceListHead" class="tableHead">'
                        +'<tr>'
                            +'<td></td>'
                        +'</tr>'
                    +'</table>'
                    +'<div id="deviceList" class="tableDiv">'
                        +'<table cellpadding="0" cellspacing="0" border="0" width="100%">'
                        +'</table>'
                    +'</div>'
                +'</div>'
                + '<div class="information"><div class="infos">'
                    + '<div class="blk" id="infoGotosensors"><input id="gotosensors" type="submit"><b></b></div>'
                    + '<div class="blk" id="infoEdit"><input id="edit" type="submit"><b></b></div>'
                    + '<div class="blk" id="infoDel"><input id="delete" type="submit"><b></b></div>'
                + '</div>';
    $("div#devices div.form").html("");
    $("div#devices div.form").html(html);

    $("div#devices div.information div#infoGotosensors b").html(language.Settings_edits_info_gotosensors);
    $("div#devices div.information div#infoEdit b").html(language.Settings_edits_info_edit);
    $("div#devices div.information div#infoDel b").html(language.Settings_edits_info_delete);
    

    $("div#devices div.information div#infoGotosensors input").val(language.Settings_edits_gotoSensors);
    $("div#devices div.information div#infoEdit input").val(language.Settings_edits_edit);
    $("div#devices div.information div#infoDel input").val(language.Settings_edits_delete);


    $("div#devices input#moduletree").val(language.devices_table_moduletree_button);
    $("div#devices input#discovery").val(language.devices_table_discovery);
    $("div#devices input#addDevice").val(language.devices_table_adddevice);
    $("div#devices input#moveDevice").val(language.devices_table_movedevice);

    $("div#devices table#connectorListHead td").html(language.devices_table_connectors);
    $("div#devices table#portListHead td").html(language.devices_table_ports);
    $("div#devices table#deviceListHead td").append(language.devices_table_devices);
    Main.loading();
    SelectType = undefined;
    var connectorData = $.getJSON("http://" + hostName + "/cgi-bin/mdlmngr.cgi?getConnectors");  
    connectorData.success(function() {
        Main.unloading();
        Devices.connectors = JSON.parse(connectorData.responseText);
        if (Devices.connectors.length == 0)
        {
            Main.unloading();
            //Main.alert(language.connector_list_empty);
            $("div#connectorList table").html("");
            $("div#portList table").html("");
            $("div#deviceList table").html("");
            return;
        }
        Devices.createConnector();
        var url = $("div#connectorList table tr td:first a").attr("data-id");
        if ($("div#connectorList table tbody tr").length > 0 )
            Devices.getPorts(url);  
    });
    connectorData.error(function(jqXHR, textStatus, errorThrown){   
        Main.unloading();
            Main.alert(language.server_error + ":" + jqXHR.responseText);
    });
};

Devices.createEditWindow = function()
{
    // var inputCalib = $("div#editWindow div.buttons input#calibration");
    // if (inputCalib != undefined)
    //     inputCalib.remove();
    var inputOutputSettings = $("div#editWindow div.buttons input#outputsettings");
    if (inputOutputSettings != undefined)
        inputOutputSettings.remove();
    $("#editWindow table.tableEditList").html("");
    var windowType = $("div#editWindow").attr("window-type");
    var object     = $("div#editWindow").attr("object");
    var objectType = $("div#editWindow").attr("object-type");
    var item = undefined;
    
    if (windowType == "add" && object == "devices")
    {
        item = ADDEVICE.concat(MDLMNGR[object].params[objectType]);
    }
    else if (windowType == "move" && object == "devices")
    {
        item = MOVEDEVICE;
    }
    else
    {
        item = MDLMNGR[object].params[objectType];
    }
    
    for (var i = 0; i < item.length; i++)
    {
        var tr = document.createElement("tr");
        tr.setAttribute("id","tr" + item[i].name);
        var dflt = item[i]["default"];
        var minval = item[i].min;
        var maxval = item[i].max;
        var td0 = document.createElement("td");
        td0.setAttribute("width","40%");
        td0.innerHTML = eval("language." + object + "_" + item[i].name);

        var td1 = document.createElement("td");
        
        var isB = false;
        if (windowType == "add" || windowType == "add-reffile")
        {
            if (item[i].access == access.r)
            {
                continue;
            }
            
            if (windowType == "add-reffile" || ($("div#edits ul.filters li option:selected").attr("device-active") == "1"))
            {
                if (item[i].name == "read_period")
                {
                    continue;
                }
                
                if (item[i].name == "read_enable")
                {
                    continue;
                }
            }
        }
        else if (windowType == "update")
        {
            if (item[i].access != access.rw)
                isB = true;
            
        }
        else if (windowType == "move")
        {
            isB = false;
        }
        
        if (item[i].html == "text")
        {
            if (isB)
            {
                var b = document.createElement("b");
                b.setAttribute("type","text");  
                td1.appendChild(b);
            }
            else
            {
                var input = document.createElement("input");
                input.setAttribute("type","text");  
                input.setAttribute("data-type", item[i].type);
                input.setAttribute("data-name", item[i].name);
                if (dflt != undefined)
                    input.setAttribute("default", dflt);
                if (minval != undefined)
                    input.setAttribute("minval", minval);
                if (maxval != undefined)
                    input.setAttribute("maxval", maxval);
                td1.appendChild(input);
            }
        }
        else if (item[i].html == "password")
        {
            if (isB)
            {
                var b = document.createElement("b");
                b.setAttribute("type","text");  
                td1.appendChild(b);
            }
            else
            {
                var input = document.createElement("input");
                input.setAttribute("type","password");  
                input.setAttribute("data-type", item[i].type);
                input.setAttribute("data-name", item[i].name);
                if (dflt != undefined)
                    input.setAttribute("default", dflt);
                if (minval != undefined)
                    input.setAttribute("minval", minval);
                if (maxval != undefined)
                    input.setAttribute("maxval", maxval);
                td1.appendChild(input);
            }
        }
        else if (item[i].html == "checkbox")
        {
            if (isB)
            {
                var b = document.createElement("b");
                b.setAttribute("type","text");  
                td1.appendChild(b);
            }
            else
            {
                var input = document.createElement("input");
                input.setAttribute("type","checkbox");  
                input.setAttribute("data-type", item[i].type);
                input.setAttribute("data-name", item[i].name);
                if (dflt != undefined)
                    input.setAttribute("default", dflt);
                if (minval != undefined)
                    input.setAttribute("minval", minval);
                if (maxval != undefined)
                    input.setAttribute("maxval", maxval);
                td1.appendChild(input);
            }
        }
        else if (item[i].html == "select")
        {
            if (isB)
            {
                var b = document.createElement("b");
                b.setAttribute("type","text");  
                td1.appendChild(b);
            }
            else
            {
                var select = document.createElement("select");  
                select.setAttribute("func", item[i].func);
                select.setAttribute("data-name", item[i].name);
                td1.appendChild(select);
            }
        }

        tr.appendChild(td0);
        tr.appendChild(td1);
        $("#editWindow table.tableEditList").append(tr);
    }
    
    Devices.createSelectElements();
    
    // Exceptional cases
    if (object == "devices")
    {
        if (eval("language." + objectType + "_addr") != undefined)
        {
            $("div#editWindow table.tableEditList tr#traddr td")[0].innerHTML = eval("language." + objectType + "_addr");
            if (deviceTypes[objectType].addable != true)
            {
                $("div#editWindow table.tableEditList tr#traddr").css("display", "none");
            }
            var addrSet = eval("addrTypes." + objectType);
            var addrIn = $($("div#editWindow table.tableEditList tr#traddr td input")[0]);
            addrIn.attr("data-type",addrSet.type);
            if (addrSet.min != undefined)
                addrIn.attr("minval", addrSet.min);
            if (addrSet.max != undefined)
                addrIn.attr("maxval", addrSet.max);
        }
        else
        {
            $("div#editWindow table.tableEditList tr#traddr").remove();
        }
    }
    
    if (object == "sensors")
    {
        if (objectType == "binary_ping")
        {
            $("div#editWindow table.tableEditList tr").css("display", "none");
            $("div#editWindow table.tableEditList tr input").css("display", "inline-block");
            $("div#editWindow table.tableEditList tr select").css("display", "inline-block");
            $("div#editWindow table.tableEditList tr#tralarm_active").css("display", "table-row");
            $("div#editWindow table.tableEditList tr#trrecord_period").css("display", "table-row");
            $("div#editWindow table.tableEditList tr#trname").css("display", "table-row");
            $("div#editWindow table.tableEditList tr#trip").css("display", "table-row");
        }
        else if (objectType == "binary_telnet")
        {
            $("div#editWindow table.tableEditList tr").css("display", "none");
            $("div#editWindow table.tableEditList tr input").css("display", "inline-block");
            $("div#editWindow table.tableEditList tr select").css("display", "inline-block");
            $("div#editWindow table.tableEditList tr#tralarm_active").css("display", "table-row");
            $("div#editWindow table.tableEditList tr#trrecord_period").css("display", "table-row");
            $("div#editWindow table.tableEditList tr#trname").css("display", "table-row");
            $("div#editWindow table.tableEditList tr#trip").css("display", "table-row");
            $("div#editWindow table.tableEditList tr#trport").css("display", "table-row");
        }
    }
};

Devices.createSelectElements = function()
{
    var selects = $("div#editWindow table.tableEditList tr select");
    for (var i = 0; i < selects.length; i++)
    {
        eval("Devices." + selects[i].getAttribute("func"));
    }
};

Devices.nop = function()
{
    return;
};

Devices.DeviceFieldsToDefaults = function()
{
    $("div#editWindow table.tableEditList input").val("");
    var lst = $("div#editWindow table.tableEditList input");
    for (var i = 0; i < lst.length; i++)
    {
        var item = $(lst[i]);
        var dflt = item.attr("default");
        if (dflt != undefined)
        {
            var type = item.attr("type");
            if (type == "text" || type == "password")
                item.val(dflt);
            else if (type == "checkbox")
                item.prop("checked", dflt == "1" ? true:false);
        }
    }
};

Devices.getEditConnectors = function(url)
{
    Main.loading();
    var connectorData = $.getJSON("http://" + hostName + "/cgi-bin/mdlmngr.cgi?" + url);     
    connectorData.success(function() {
        Main.unloading();
        var item = JSON.parse(connectorData.responseText);
        Devices.createEditConnector(item);
    });
    connectorData.error(function(jqXHR, textStatus, errorThrown){   
        Main.unloading();
            Main.alert(language.server_error + ":" + jqXHR.responseText);
    });
};

Devices.getEditPort = function(url)
{
    Main.loading();
    var portData = $.getJSON("http://" + hostName + "/cgi-bin/mdlmngr.cgi?" + url);  
    portData.success(function() {
        Main.unloading();
        var item = JSON.parse(portData.responseText);
        Devices.createEditPort(item);
    });
    portData.error(function(jqXHR, textStatus, errorThrown){
        Main.unloading();
            Main.alert(language.server_error + ":" + jqXHR.responseText);   
    });
};

Devices.getEditDevice = function(url)
{
    Main.loading();
    var deviceData = $.getJSON("http://" + hostName + "/cgi-bin/mdlmngr.cgi?" + url);    
    deviceData.success(function() {
        Main.unloading();
        var item = JSON.parse(deviceData.responseText);
        Devices.createEditDevice(item);
    });
    deviceData.error(function(jqXHR, textStatus, errorThrown){  
        Main.unloading();
            Main.alert(language.server_error + ":" + jqXHR.responseText);
    });
};

Devices.createEditDevice = function(device)
{
    var type = Devices.deviceTypeToObjectType(device.type);
    $("div#editWindow").attr("window-type", "update");
    $("div#editWindow").attr("object", "devices");
    $("div#editWindow").attr("object-type", type);
    Devices.createEditWindow();
    Devices.FillEditDeviceFields(type,device);  
};

Devices.deviceTypeToObjectType = function(type)
{
    var tt = parseInt(type);
    var val = "";
    for (var key in deviceTypes)
    {
        if (type == deviceTypes[key].value)
        {
            val = key;
            break;
        }
    }
    return val;
};

Devices.FillEditDeviceFields = function(type, device)
{
    for (var key in deviceTypes)
    {
        if (type == key)
        {
            var params = eval("deviceParams." + key);
            for (var i = 0; i < params.length; i++)
            {
                if (params[i].name == "read_period" && !parseInt(device.active))
                {
                    $("div#editWindow table.tableEditList tr#trread_period").remove();
                    continue;
                }
                if (params[i].name == "read_enable" && !parseInt(device.active))
                {
                    $("div#editWindow table.tableEditList tr#trread_enable").remove();
                    continue;
                }
                    
                if (params[i].access == access.rw)
                {
                    if (params[i].html == "select" && params[i].name == "version")
                    {
                        $("div#editWindow table.tableEditList tr#tr" + params[i].name + " select option#" + 
                        snmpVersions["version"][parseInt(device[params[i].name])]).prop("selected", true);
                    }
                    else
                        $("div#editWindow tr#tr" + params[i].name + " input").val(eval("device." + params[i].name));

                }
                else
                    $("div#editWindow tr#tr" + params[i].name + " b").html(eval("device." + params[i].name));

            }
            var objectid = $("div#editWindow").attr("objectid");
            $("div#editWindow table.tableEditList tr#traddr input").val(objectid.split("/")[2]);
            if (type == "modbus_tcp")
                $("div#editWindow table.tableEditList tr#traddr input").val(device.modbus_addr);
            if (type == "snmp")
            {
                if (device.version != "2")
                {
                    $("div#editWindow tr#trsecurity_name").css("display", "none");
                    $("div#editWindow tr#trpassword").css("display", "none");
                }
                else
                {
                    $("div#editWindow tr#trsecurity_name").css("display", "table-row");
                    $("div#editWindow tr#trpassword").css("display", "table-row");
                }
                $("div#editWindow table.tableEditList tr#trversion b").html(eval("language." + snmpVersions.version[parseInt(device.version)]));
            }
            $("div#editWindow table.tableEditList tr#trtype b").html(eval("language." + type));
            break;
        }
    }
};


/////////////////// FOR SENSORS
Devices.getEditSensor = function(url)
{
    Main.loading();
    var deviceData = $.getJSON("http://" + hostName + "/cgi-bin/mdlmngr.cgi?" + url);    
    deviceData.success(function() {
        Main.unloading();
        var item = JSON.parse(deviceData.responseText);
        Devices.createEditSensor(item);
    });
    deviceData.error(function(jqXHR, textStatus, errorThrown){    
        Main.unloading();
            Main.alert(language.server_error + ":" + jqXHR.responseText);
    });
};

Devices.createEditSensor = function(sensor)
{
    var type = Devices.sensorTypeToObjectType(sensor.type);
    $("div#editWindow").attr("window-type", "update");
    $("div#editWindow").attr("object", "sensors");
    $("div#editWindow").attr("object-type", type);
    var html = $("div#editWindow td#modulsensorname").html();
    $("div#editWindow td#modulsensorname").html(html + " &nbsp;        " + language.value + " : " + sensor.value);
    Devices.createEditWindow();
    Devices.FillEditSensorFields(type,sensor);  
    // Devices.addCalibrationOption(sensor);
};

Devices.setOutputSettings = function(objectid)
{
    Main.loading();
    // var interval = $("div#calibWindow table.tableEditList tr#interval select option:selected").attr("value");
    var objectId = objectid.split("/");
    var prop = {output_interval: interval};
    var json = JSON.stringify(prop);
    var url = "http://" + hostName + "/cgi-bin/mdlmngr.cgi?updateSensor&hwaddr=" + objectId[0] + "&portnum=" + objectId[1] + "&devaddr=" + objectId[2] + "&ionum=" + objectId[3];   

    $.ajax({
        url : url,
        type: "POST",
        data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
        success: function(data, textStatus, jqXHR)
        {
            $("div#overlay2").css("display", "none");
            // $("div#calibWindow").css("display", "none");
            Main.unloading();
        },
        error : function (jqXHR, textStatus, errorThrown)
        {
            $("div#overlay2").css("display", "none");
            // $("div#calibWindow").css("display", "none");    
            Main.unloading();
        }
    });
    
};

Devices.sensorTypeToObjectType = function(type)
{
    var tt = parseInt(type);
    var val = "";
    for (var key in sensorTypes)
    {
        if (type == sensorTypes[key])
        {
            val = key;
            break;
        }
    }
    return val;
};

Devices.FillEditSensorFields = function(type, sensor)
{   
    if (sensor.active == "1")
    {
        $("div#editWindow tr#trread_period").remove();
        $("div#editWindow tr#trread_enable").remove();
    }
    //else
    //{
    //  $("div#editWindow tr#trresolution").remove();
    //}
    
    for (var key in sensorTypes)
    {
        if (type == key)
        {
            var params = eval("sensorParams." + key);
            for (var i = 0; i < params.length; i++)
            {
                
                if (sensor.active == "1")
                {
                    if (params[i].name == "read_period")
                        continue;
                    if (params[i].name == "read_enable")
                        continue;
                }
                else
                {
                    if (params[i].name == "resolution")
                        continue;
                }

                if (params[i].name == "alarm_value")
                {   
                    $($("div#editWindow table.tableEditList tr#tralarm_value select option")[0]).html(sensor.zero_name);
                    $($("div#editWindow table.tableEditList tr#tralarm_value select option")[1]).html(sensor.one_name);
                }
                
                if (params[i].access == access.rw)
                {
                    var value = eval("sensor." + params[i].name);
                    var elementType = params[i].html;
                    if (elementType == "text" || elementType == "password")
                        $("div#editWindow tr#tr" + params[i].name + " input").val(value);
                    else if (elementType == "checkbox")
                        $("div#editWindow tr#tr" + params[i].name + " input").prop("checked", value == "1" ? true:false);
                    else if (elementType == "select") {
                        $("div#editWindow tr#tr" + params[i].name + " select").prop("selectedIndex", parseInt(value));
                        $("div#editWindow tr#tr" + params[i].name + " select").trigger("change");
                    }


                }
                else
                    $("div#editWindow tr#tr" + params[i].name + " b").html(eval("sensor." + params[i].name));
                    
                
                if (params[i].name == "issigned")
                {
                    if (parseInt(sensor.issigned) == 1)
                        $("div#editWindow table.tableEditList tr#trissigned b").html(language.sensors_signed);
                    else
                        $("div#editWindow table.tableEditList tr#trissigned b").html(language.sensors_unsigned);
                }

                if (params[i].name == "periodtype")
                {
                    var value = "" + (Math.log(sensor.periodtype) /  Math.log(2));
                    $("div#editWindow tr#trperiodtype select").prop("selectedIndex", parseInt(value));
                }
            }
            var objectid = $("div#editWindow").attr("objectid");
            $("div#editWindow table.tableEditList tr#trtype b").html(key);    
            $("div#editWindow table.tableEditList tr#trread_write b").html(eval("language.Settings_edits_" + readWrite[parseInt(sensor.read_write)]));
            break;
        }
    }
       if(sensor.filter_type == 1) { 
           $("div#editWindow tr#trchange_rate input").val(sensor.filter_value); 
       }else if(sensor.filter_type == 0){
           $("div#editWindow tr#trresolution select").prop("selectedIndex", parseInt(sensor.filter_value));
       }
   
};

Devices.getAllDevices = function(type)
{
    var portData = $.getJSON("http://" + hostName + "/cgi-bin/mdlmngr.cgi?getDevices");  
    portData.success(function() {
        Devices.allDevices = JSON.parse(portData.responseText);
    if (type == "moduletree")
    {
        var doc = new jsPDF("p");
        var strToWrite = Devices.createConnectorsText(doc);
        console.log(strToWrite);
        doc.setFontSize(12);

        var splitted = strToWrite.split("\n");
        var lineCount = 0;
        for(var i=0; i < splitted.length; ++i)
        {
            doc.text(splitted[i], 12, lineCount*10);
            lineCount++;
            if(lineCount == 24)
            {
                lineCount = 3;
                doc.text(" ", 12, 25*10);
                doc.text(" ", 12, 26*10);
                doc.text(" ", 12, 27*10);
                doc.addPage();
                doc.text(" ", 12, 0*10);
                doc.text(" ", 12, 1*10);
                doc.text(" ", 12, 2*10);
            }
        }
        Main.unloading();
        doc.output('datauri');
    }
    });
    portData.error(function(jqXHR, textStatus, errorThrown){
        Main.unloading();
            Main.alert(language.server_error + ":" + jqXHR.responseText);   
    });
};

Devices.getAllPorts = function()
{
    var portData = $.getJSON("http://" + hostName + "/cgi-bin/mdlmngr.cgi?getPorts");    
    portData.success(function(){
        Devices.allPorts = JSON.parse(portData.responseText);
        Devices.getAllDevices("moduletree");
    });
    portData.error(function(jqXHR, textStatus, errorThrown){
        Main.unloading();
            Main.alert(language.server_error + ":" + jqXHR.responseText);   
    });
};

Devices.createSensorsText = function(devOid)
{
    var tab = "                                ";
    var text = "";
    var len = Main.fullItems.length;

    var tmpList = [];
    for (var i = 0; i < len; i++)
    {

        var splitted = Main.fullItems[i].objectid.split("/");
        var ownerDevOid = splitted[0] + "/" + splitted[1] + "/" + splitted[2];
        if (ownerDevOid == devOid)
        {
            tmpList[parseInt(Main.fullItems[i].ionum)] = Main.fullItems[i];
        }
    }

    var k = 0;
    len = tmpList.length;
    var sensorList = [];
    for (var i = 0; i < len; i++)
    {
        if (tmpList[i] == undefined)
            continue;
        sensorList[k++] = tmpList[i];
    }

    len = sensorList.length;
    for (var i = 0; i < len; i++)
    {
        text  = text + tab + language.devices_moduletree_sensor + ": " + sensorList[i].sensorname + " [" + sensorList[i].objectid +  "]\n"; 
    }
    return text;
};

Devices.createDevicesText = function(portOid)
{
    var tab = "                ";
    var text = "";
    var len = Devices.allDevices.length;
    for (var i = 0; i < len; i++)
    {
        var splitted = Devices.allDevices[i].objectid.split("/");
        var ownerPortOid = splitted[0] + "/" + splitted[1];
        if (ownerPortOid == portOid)
        {
            text  = text + tab + language.devices_moduletree_device + ": " + Devices.allDevices[i].name + " [" + Devices.allDevices[i].objectid +  "]\n";   
            text = text + Devices.createSensorsText(Devices.allDevices[i].objectid);    
        }
    }
    return text;
};

Devices.createPortsText = function(connOid)
{
    var tab = "        ";
    var text = "";
    var len = Devices.allPorts.length;
    var tmpList = [];
    for (var i = 0; i < len; i++)
    {
        if (Devices.allPorts[i].objectid.split("/")[0] == connOid)
        {
            tmpList[parseInt(Devices.allPorts[i].number)] = Devices.allPorts[i];
        }
    }

    var k = 0;
    len = tmpList.length;
    var portList = [];
    for (var i = 0; i < len; i++)
    {
        if (tmpList[i] == undefined)
            continue;
        portList[k++] = tmpList[i];
    }

    len = portList.length;
    for (var i = 0; i < len; i++)
    {
        text  = text + tab + language.devices_moduletree_port + ": " + portList[i].number + " [" + portList[i].objectid +  "]\n";   
        text = text + Devices.createDevicesText(portList[i].objectid);  
    }
    return text;
};

Devices.createConnectorsText = function(doc)
{
    var text = " \n \n \n";
    var len = Devices.connectors.length;
    for (var i = 0; i < len; i++)
    {
        text  = text + language.devices_moduletree_connector + ": " + Devices.connectors[i].description + " [" + Devices.connectors[i].objectid +  "]\n";
        text = text + Devices.createPortsText(Devices.connectors[i].objectid) + " \n";
    }
    console.log(text);
    return text;
};

Devices.createModulTree = function()
{
    Main.loading();
    Devices.getAllPorts();
};

Devices.getDiscovery = function()
{
    Main.loading();

    var url = "http://" + hostName + "/cgi-bin/megweb.cgi?makeDiscovery";

    $.ajax({
        url : url,
        type: "POST",
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
        success: function(data, textStatus, jqXHR)
        {
            Main.unloading();
            Main.alert(language.make_discovery);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            Main.unloading();
            Main.alert(jqXHR.responseText);
        }
    });
};

Devices.updateDevice = function(objectid,jSon)
{
    if ($("div#editWindow").attr("object-type") == "modbus_tcp" ) 
        if (jSon.addr != undefined)
            delete jSon.addr;
    var objectId = objectid.split("/");
    Main.loading();
    var json = JSON.stringify(jSon);
    var url = "http://" + hostName + "/cgi-bin/mdlmngr.cgi?updateDevice&hwaddr=" + objectId[0] + "&portnum=" + objectId[1] + "&devaddr=" + objectId[2]; 

    $.ajax({
        url : url,
        type: "POST",
        data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
        success: function(veri, textStatus, jqXHR)
        {
            var jsON = jQuery.parseJSON(veri);
            if (jsON["return"] == "true")
            {
                Main.unloading();
                var url = "getDevices" + $("div#portList table td.active a").attr("data-id");
                if (jSon.name != undefined)
                    for (var i = 0; i < deviceList.length; i++)
                    {
                        if (deviceList[i].objectid == objectid)
                            deviceList[i].name = jSon.name;
                    }
                Devices.getDevices(url);
            }
            else
            {
                Main.unloading();
                Main.alert(language.devices_update_fail + ":" + jsON["return"]);
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            Main.unloading();
            Main.alert(jqXHR.responseText);
        }
    });
};

Devices.updateConnector = function(objectid,json)
{
    Main.loading();
    var json = JSON.stringify(json);
    var url = "http://" + hostName + "/cgi-bin/mdlmngr.cgi?updateConnector&hwaddr=" + objectid; 

    $.ajax({
        url : url,
        type: "POST",
        data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
        success: function(veri, textStatus, jqXHR)
        {
            var jsON = jQuery.parseJSON(veri);
            if (jsON["return"] == "true")
            {
                var url = $("div#portList table td.active a").attr("data-id");
                var connectorData = $.getJSON("http://" + hostName + "/cgi-bin/mdlmngr.cgi?getConnectors");  
                connectorData.success(function() {
                    Main.unloading();
                    var index = $("div#connectorList table td.active a").attr("data-td");
                    index = index.split("_");
                    index = index[1];
                    Devices.connectors = JSON.parse(connectorData.responseText);
                    Devices.createConnector(index);
                });
            }
            else
            {
                Main.alert(language.connectors_update_fail + ":" + jsON["return"]);
                Main.unloading();
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            Main.unloading();
            Main.alert(jqXHR.responseText);
        }
    });
};

Devices.updatePort = function(objectid,json)
{
    var objectId = objectid.split("/");
    Main.loading();
    var json = JSON.stringify(json);
    var url = "http://" + hostName + "/cgi-bin/mdlmngr.cgi?updatePort&hwaddr=" + objectId[0] + "&portnum=" + objectId[1];   

    $.ajax({
        url : url,
        type: "POST",
        data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
        success: function(veri, textStatus, jqXHR)
        {
            var jsON = jQuery.parseJSON(veri);
            if (jsON["return"] == "true")
            {
                var url = $("div#connectorList table td.active a").attr("data-id");
                var portData = $.getJSON("http://" + hostName + "/cgi-bin/mdlmngr.cgi?" + url);  
                portData.success(function() {
                    Main.unloading();
                    var index = $("div#portList table td.active a").attr("data-td");
                    index = index.split("_");
                    index = index[1];
                    Devices.ports = JSON.parse(portData.responseText);
                    Devices.createPort(index);
                    Devices.createPort();

                });
            }
            else
            {
                Main.alert(language.ports_update_fail + ":" + jsON["return"]);
                Main.unloading();
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            Main.unloading();
            Main.alert(jqXHR.responseText);
        }
    });
};

Devices.removeConnector = function(url)
{
    Main.loading();
    var url = "http://" + hostName + "/cgi-bin/mdlmngr.cgi?" + url; 

    $.ajax({
        url : url,
        type: "POST",
        data : {},
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
        success: function(data, textStatus, jqXHR)
        {
                Devices.getConnectors();
        },
        error: function(jqXHR, textStatus, errorThrown) {
            Main.unloading();
            Main.alert(jqXHR.responseText);
        }
    });
};

Devices.removeDevice = function(url)
{
    Main.loading();
    var url = "http://" + hostName + "/cgi-bin/mdlmngr.cgi?" + url; 

    $.ajax({
        url : url,
        type: "POST",
        data : {},
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
        success: function(data, textStatus, jqXHR)
        {
                var url = "getDevices" + $("div#portList table td.active a").attr("data-id");
                Devices.getDevices(url);
        },
        error: function(jqXHR, textStatus, errorThrown) {
            Main.unloading();
            Main.alert(jqXHR.responseText);
        }
    });
};

Devices.addDevice = function(item)
{
    Main.loading();
    var json = JSON.stringify(item);
    var hwaddr = $("div#editWindow table tr#trhwaddr select option:selected").attr("value");
    var portnum = $("div#editWindow table tr#trnumber select option:selected").attr("value");
    var url = "http://" + hostName + "/cgi-bin/mdlmngr.cgi?addDevice&hwaddr=" + hwaddr + "&portnum=" + portnum; 
 
    $.ajax({
        url : url,
        type: "POST",
        data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
        success: function(veri, textStatus, jqXHR)
        {
            jsON = jQuery.parseJSON(veri);
            if (jsON["return"] == "true")
            {
                var url = $("div#portList table td.active a").attr("data-id");
                var deviceData = $.getJSON("http://" + hostName + "/cgi-bin/mdlmngr.cgi?getConnectors");     
                deviceData.success(function() {
                    Main.unloading();
                    var url = "getDevices" + $("div#portList table td.active a").attr("data-id");
                    Devices.getDevices(url);
                });
            }
            else
            {
                Main.alert(language.devices_add_fail + ":" + jsON["return"]);
                Main.unloading();

            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            Main.unloading();
            Main.alert(jqXHR.responseText);
        }
    });
};

Devices.getPorts = function(url)
{
    Main.loading();
    var portData = $.getJSON("http://" + hostName + "/cgi-bin/mdlmngr.cgi?" + url);  
    portData.success(function() {
        Main.unloading();
        Devices.ports = JSON.parse(portData.responseText);
        if (Devices.ports.length == 0)
        {
            Main.unloading();
            //Main.alert(language.port_list_empty);
            $("div#portList table").html("");
            $("div#deviceList table").html("");
            return;
        }
        Devices.createPort();
        if(url == undefined) {
            var url = "getDevices" + $("div#portList table tr td:first a").attr("data-id");
            Devices.getDevices(url);
        }
    });
    portData.error(function(jqXHR, textStatus, errorThrown){
        Main.unloading();
            Main.alert(language.server_error + ":" + jqXHR.responseText);   
    });
};

Devices.getDevices = function(url)
{
    Main.loading();
    var deviceData = $.getJSON("http://" + hostName + "/cgi-bin/mdlmngr.cgi?" + url);    
    deviceData.success(function() {
        Main.unloading();
        Devices.devices = JSON.parse(deviceData.responseText);
        if (Devices.devices.length == 0)
        {
            Main.unloading();
            //Main.alert(language.device_list_empty);
            $("div#deviceList table").html("");
            return;
        }
        Devices.createDevice();
    });
    deviceData.error(function(jqXHR, textStatus, errorThrown){
        Main.unloading();
            Main.alert(language.server_error + ":" + jqXHR.responseText);   
    });
};

Devices.getConnectorsForAddDevice = function()
{
    var type = $("div#editWindow tr#trtype select option:selected").attr("value");
    Main.loading();
    var connectorData = $.getJSON("http://" + hostName + "/cgi-bin/megweb.cgi?getConnectorsForAddDevice&device_type=" + type);   
    connectorData.success(function() {
        var items = JSON.parse(connectorData.responseText);
        $("div#editWindow table.tableEditList tr#trhwaddr select").html("");
        $("div#editWindow table.tableEditList tr#trnumber select").html("");
        if(items.length == 0)
        {
            Main.unloading();
            $("div#editWindow td.alert").html(language.notconnector);
        }
        else
        {
            Devices.createAddConnectors(items, type);
        }
    });
    connectorData.error(function(jqXHR, textStatus, errorThrown){   
        Main.unloading();
            Main.alert(language.server_error + ":" + jqXHR.responseText);   
    });
};

Devices.getPortsForAddDevice = function(url, type)
{
    Main.loading();
    var portData = $.getJSON("http://" + hostName + "/cgi-bin/megweb.cgi?" + url);   
    portData.success(function() {
        Main.unloading();
        var items = JSON.parse(portData.responseText);
        if(items.length == 0) {
            $("div#editWindow td.alert").html(language.notport);
            $("div#editWindow table.tableEditList tr#trnumber select").html("");
        } else {
            Devices.createAddPorts(items, type);
        }
    });
    portData.error(function(jqXHR, textStatus, errorThrown){
        Main.unloading();
            Main.alert(language.server_error + ":" + jqXHR.responseText);   
    });
};

Devices.getMovableConnectors = function()
{
    Main.loading();
    var connectorData = $.getJSON("http://" + hostName + "/cgi-bin/megweb.cgi?getMovableConnectors");    
    connectorData.success(function() {
        var items = JSON.parse(connectorData.responseText);
        $("div#editWindow table.tableEditList tr#trconnector_from select").html("");
        $("div#editWindow table.tableEditList tr#trport_from select").html("");
        $("div#editWindow table.tableEditList tr#trmovable select").html("");
        if(items.length == 0)
        {
            Main.unloading();
            $("div#editWindow td.alert").html(language.notmovableconnector);
        }
        else
        {
            for(var i = 0; i < items.length; i++)
            {
                var item = items[i];
                var hwaddr = item.hwaddr;
                var option = document.createElement("option");
                option.innerHTML = hwaddr + "(" + item.description + ")";
                option.setAttribute("objectid", hwaddr);
                option.setAttribute("style", "width: 60px;");
                $("div#editWindow table.tableEditList tr#trconnector_from select").append(option);
            }
            objectid = $("div#editWindow table.tableEditList tr#trconnector_from select option:first").attr("objectid");
            Devices.getMovablePorts(objectid);
        }
    });
    connectorData.error(function(jqXHR, textStatus, errorThrown){   
        Main.unloading();
            Main.alert(language.server_error + ":" + jqXHR.responseText);   
    });
};

Devices.getMovablePorts = function(objectid)
{
    Main.loading();
    var portData = $.getJSON("http://" + hostName + "/cgi-bin/megweb.cgi?getMovablePorts&hwaddr="+objectid);  
    portData.success(function() {
        Main.unloading();
        var items = JSON.parse(portData.responseText);
        
        $("div#editWindow table.tableEditList tr#trport_from select").html("");
        $("div#editWindow table.tableEditList tr#trmovable select").html("");
        if(items.length == 0)
        {
            $("div#editWindow td.alert").html(language.notmovableport);
        }
        else
        {
            for(var i = 0; i < items.length; i++)
            {
                var item = items[i];
                var number = item.number;
                var value = number;
                if (item.description != undefined)
                    value += "(" + item.description + ")";
                var objectid = item.objectid;
                var option = document.createElement("option");
                option.innerHTML = value;   
                option.setAttribute("objectid", objectid);
                option.setAttribute("value", number);
                $("div#editWindow table.tableEditList tr#trport_from select").append(option);
            }
            objectid = $("div#editWindow table.tableEditList tr#trport_from select option:first").attr("objectid");
            Devices.getMovableDevices(objectid);
        }
    });
    portData.error(function(jqXHR, textStatus, errorThrown){
        Main.unloading();
            Main.alert(language.server_error + ":" + jqXHR.responseText);   
    });
};

Devices.getMovableDevices = function(objectid)
{
    Main.loading();
    var obj = objectid.split("/");
    var deviceData = $.getJSON("http://" + hostName + "/cgi-bin/mdlmngr.cgi?getDevices&hwaddr=" + obj[0] + "&portnum=" + obj[1]);    
    deviceData.success(function() {
        Main.unloading();
        var items = JSON.parse(deviceData.responseText);
        
        $("div#editWindow table.tableEditList tr#trmovable select").html("");
        if(items.length == 0)
        {
            $("div#editWindow td.alert").html(language.notmovabledevice);
        }
        else
        {
            var url = null;
            for(var i = 0; i < items.length; i++)
            {
                var item = items[i];
                var objectid = item.objectid;
                var option = document.createElement("option");
                option.innerHTML = objectid + "(" + item.name + ")";    
                option.setAttribute("objectid", objectid);
                option.setAttribute("type", item.type);
                $("div#editWindow table.tableEditList tr#trmovable select").append(option);
            }
            deviceType = $("div#editWindow table.tableEditList tr#trmovable select option:first").attr("type");
            Devices.getConnectorsForMoveDevice(deviceType);
        }
    });
    deviceData.error(function(jqXHR, textStatus, errorThrown){
        Main.unloading();
            Main.alert(language.server_error + ":" + jqXHR.responseText);   
    });
};


Devices.getConnectorsForMoveDevice = function(deviceType)
{
    Main.loading();
    var connectorData = $.getJSON("http://" + hostName + "/cgi-bin/megweb.cgi?getConnectorsForMoveDevice&device_type=" + deviceType);    
    connectorData.success(function() {
        var items = JSON.parse(connectorData.responseText);
        $("div#editWindow table.tableEditList tr#trconnector_to select").html("");
        $("div#editWindow table.tableEditList tr#trport_to select").html("");
        if(items.length == 0)
        {
            Main.unloading();
            $("div#editWindow td.alert").html(language.notconnector);
        }
        else
        {
            for(var i = 0; i < items.length; i++)
            {
                var item = items[i];
                var hwaddr = item.hwaddr;
                var option = document.createElement("option");
                option.innerHTML = hwaddr + "(" + item.description + ")";
                option.setAttribute("objectid", hwaddr);
                option.setAttribute("style", "width: 60px;");
                $("div#editWindow table.tableEditList tr#trconnector_to select").append(option);
            }
            objectid = $("div#editWindow table.tableEditList tr#trconnector_to select option:first").attr("objectid");
            devoid = $("div#editWindow table.tableEditList tr#trmovable select option:first").attr("objectid");
            Devices.getPortsForMoveDevice(devoid, objectid, deviceType);
        }
    });
    connectorData.error(function(jqXHR, textStatus, errorThrown){   
        Main.unloading();
            Main.alert(language.server_error + ":" + jqXHR.responseText);   
    });
};

Devices.getPortsForMoveDevice = function(devoid, objectid, deviceType)
{
    Main.loading();
    var portData = $.getJSON("http://" + hostName + "/cgi-bin/megweb.cgi?getPortsForMoveDevice&hwaddr=" + objectid + "&devoid=" + devoid + "&device_type=" + deviceType);    
    portData.success(function() {
        Main.unloading();
        var items = JSON.parse(portData.responseText);
        if(items.length == 0)
        {
            $("div#editWindow td.alert").html(language.notport);
            $("div#editWindow table.tableEditList tr#trport_to select").html("");
        }
        else
        {
            for(var i = 0; i < items.length; i++)
            {
                var item = items[i];
                var number = item.number;
                var value = number;
                if (item.description != undefined)
                    value += "(" + item.description + ")";
                var objectid = item.objectid;
                var option = document.createElement("option");
                option.innerHTML = value;   
                option.setAttribute("objectid", objectid);
                option.setAttribute("value", number);
                option.setAttribute("maxDevAddr", item.maxDevAddr);
                $("div#editWindow table.tableEditList tr#trport_to select").append(option);
            }
        }
    });
    portData.error(function(jqXHR, textStatus, errorThrown){
        Main.unloading();
            Main.alert(language.server_error + ":" + jqXHR.responseText);   
    });
};

Devices.MoveDevice = function()
{
    var devoid     = $("div#editWindow table.tableEditList tr#trmovable select option:selected").attr("objectid");
    var objectid   = devoid.split("/");
    var hwaddrfrom = objectid[0];
    var portfrom   = objectid[1];
    var devfrom    = objectid[2];
    var portoid    = $("div#editWindow table.tableEditList tr#trport_to select option:selected").attr("objectid");
    objectid       = portoid.split("/");
    var hwaddrto   = objectid[0];
    var portto     = objectid[1];
    
    Main.loading();
    var moveDevice = "http://" + hostName + "/cgi-bin/mdlmngr.cgi?moveDevice"
                               + "&hwaddrfrom=" + hwaddrfrom
                               + "&portfrom="   + portfrom
                               + "&devfrom="    + devfrom
                               + "&hwaddrto="   + hwaddrto
                               + "&portto="     + portto;     

    $.ajax({
            url : moveDevice,
            type : "POST",
            data : {},
            dataType : 'text',
            contentType : 'text/json; charset=utf-8',
            success : function(Data, textStatus, jqXHR)
            {
                Main.unloading();
                $("div#overlay").css("display","none");
                $("div#editWindow").css("display","none");
                var item = JSON.parse(Data);
                if (item["return"] == "true")
                    Main.alert(language.Settings_movedevice_success);
                else
                    Main.alert(language.Settings_movedevice_failed + ":" + item["return"]);
                
            },
            error : function (jqXHR, textStatus, errorThrown)
            {   
                Main.unloading();
                Main.alert(language.Settings_movedevice_failed + ":" + jqXHR.responseText);
            }
        });
};

Devices.getRefFiles = function()
{
    var url = "get" + Reffiles[$("div#editWindow tr#trtype select option:selected").attr("id")] +"RefFiles";
    var reffilesData = $.getJSON("http://" + hostName + "/cgi-bin/megweb.cgi?" + url);   
    reffilesData.success(function() {
        var items = JSON.parse(reffilesData.responseText);
            $("div#editWindow table.tableEditList tr#trref_file select").html("");
        for (var i =0 ; i < items.length; i++)
        {
            var opt = document.createElement("option");
            opt.setAttribute("value", items[i].name);
            opt.innerHTML = items[i].name;
            $("div#editWindow table.tableEditList tr#trref_file select").append(opt);
        }
    });
    reffilesData.error(function(jqXHR, textStatus, errorThrown){    
            Main.alert(language.server_error + ":" + jqXHR.responseText);
    });
};

Devices.createSnmpVersionSelect = function()
{
    var len = $("div#editWindow table.tableEditList tr#trversion select").children("option").length;
    if (len != 0)
    {
        $("div#editWindow tr#trsecurity_name").css("display", "none");
        $("div#editWindow tr#trpassword").css("display", "none");
        return;
    }
    for (var i = 0; i < snmpVersions.version.length; i++)
    {
        var opt = document.createElement("option");
        opt.setAttribute("value", i.toString());
        opt.innerHTML = eval("language." + snmpVersions.version[i]);
        opt.setAttribute("id", snmpVersions.version[i]);
        $("div#editWindow table.tableEditList tr#trversion select").append(opt);
    }

    $("div#editWindow tr#trsecurity_name").css("display", "none");
    $("div#editWindow tr#trpassword").css("display", "none");
};

Devices.createSelectDeviceType = function()
{
    var len = $("div#editWindow table.tableEditList tr#trtype select").children("option").length;
    if (len != 0)
        return;
    for (var key in deviceTypes)
    {
        if (deviceTypes[key].addable)
        {
            var opt = document.createElement("option");
            opt.setAttribute("id", key);
            opt.setAttribute("value", (deviceTypes[key].value).toString());
            opt.innerHTML = eval("language." + key);
            $("div#editWindow table.tableEditList tr#trtype select").append(opt);
        }
    }
    
    if (SelectType != undefined)
        $("div#editWindow table.tableEditList tr#trtype select option#" + deviceTypesToStr0(SelectType)).prop("selected", true);
};

Devices.createAlarmValSelect = function()
{
    var len = $("div#editWindow table.tableEditList tr#tralarm_value select").children("option").length;
    if (len != 0)
        return;
    for (var i = 0; i < alarmVals.length; i++)
    {
        var opt = document.createElement("option");
        opt.setAttribute("id", alarmVals[i]);
        opt.setAttribute("value", i);
        opt.innerHTML = eval("language.Settings_edits_" + alarmVals[i]);
        $("div#editWindow table.tableEditList tr#tralarm_value select").append(opt);
    }
};

Devices.createPoweronAplySelect = function()
{
    var len = $("div#editWindow table.tableEditList tr#trapply select").children("option").length;
    if (len != 0)
        return;
    for (var i = 0; i < poweronVals.length; i++)
    {
        var opt = document.createElement("option");
        opt.setAttribute("id", poweronVals[i]);
        opt.setAttribute("value", i);
        opt.innerHTML = eval("language.Settings_edits_apply_" + poweronVals[i]);
        $("div#editWindow table.tableEditList tr#trapply select").append(opt);
    }
};

Devices.createReadWriteSelect = function()
{
    var len = $("div#editWindow table.tableEditList tr#trread_write select").children("option").length;
    if (len != 0)
        return;
    for (var i = 0; i < readWrite.length; i++)
    {
        var opt = document.createElement("option");
        opt.setAttribute("id", readWrite[i]);
        opt.setAttribute("value", i);
        opt.innerHTML = eval("language.Settings_edits_" + readWrite[i]);
        $("div#editWindow table.tableEditList tr#trread_write select").append(opt);

    }
};

Devices.createResolutionSelect = function()
{
    var len = $("div#editWindow table.tableEditList tr#trresolution select").children("option").length;
    if (len != 0)
        return;
    for (var i = 0; i < Resolution.length; i++)
    {
        var opt = document.createElement("option");
        opt.setAttribute("id", Resolution[i]);
        opt.setAttribute("value", i);
        opt.innerHTML = eval("language.Settings_edits_" + Resolution[i]);
        $("div#editWindow table.tableEditList tr#trresolution select").append(opt);

    }
};
Devices.createFilterSelect = function()
{
    var len = $("div#editWindow table.tableEditList tr#trfilter_type select").children("option").length;
    if (len != 0)
        return;
    for (var i = 0; i < FilterType.length; i++)
    {
        var opt = document.createElement("option");
        opt.setAttribute("id", FilterType[i]);
        opt.setAttribute("value", i);
        opt.innerHTML = eval("language.Settings_edits_" + FilterType[i]);
        $("div#editWindow table.tableEditList tr#trfilter_type select").append(opt);

    }
    $("div#editWindow table.tableEditList tr#trfilter_type select").trigger("change")
    $("div#editWindow table.tableEditList tr#trfilter_type select").change(function(){
           if($(this).val() == 0){
             $("div#editWindow table.tableEditList tr#trresolution").show();
             $("div#editWindow table.tableEditList tr#trchange_rate").hide();

           } else {
                $("div#editWindow table.tableEditList tr#trresolution").hide();
                $("div#editWindow table.tableEditList tr#trchange_rate").show();
            }
    })
};

Devices.createPeriodTypeSelect = function()
{
    var len = $("div#editWindow table.tableEditList tr#trperiodtype select").children("option").length;
    if (len != 0)
        return;

    for (var i = 0; i < PeriodType.length; i++)
    {
        var opt = document.createElement("option");
        opt.setAttribute("id", PeriodType[i]);
        opt.setAttribute("value", i);
        opt.innerHTML = eval("language.Settings_edits_" + PeriodType[i]);
        $("div#editWindow table.tableEditList tr#trperiodtype select").append(opt);

    }
};

Devices.createSensorTypeSelect = function()
{
    var len = $("div#editWindow table.tableEditList tr#trtype select").children("option").length;
    if (len != 0)
        return;
    for (var key in sensorTips)
    {
        var opt = document.createElement("option");
        opt.setAttribute("id",key);
        opt.setAttribute("value", sensorTips[key].value);
        opt.innerHTML = eval("language.Settings_edits_type_" + key);
        $("div#editWindow table.tableEditList tr#trtype select").append(opt);

    }
    if (SelectType != undefined)
        $("div#editWindow table.tableEditList tr#trtype select option#" + sensorTypesToStr(SelectType)).prop("selected", true);
};

Devices.createBaudrateSelect = function()
{
    var len = $("div#editWindow table.tableEditList tr#trboudrate select").children("option").length;
    if (len != 0)
        return;
    for (var i = 0; i < Baudrates.length; i++)
    {
        var opt = document.createElement("option");
        opt.setAttribute("id", Baudrates[i]);
        opt.setAttribute("value", Baudrates[i]);
        opt.innerHTML = Baudrates[i].toString();
        $("div#editWindow table.tableEditList tr#trboudrate select").append(opt);
    }   
};

Devices.createDatabitsSelect = function()
{
    var len = $("div#editWindow table.tableEditList tr#trdatabits select").children("option").length;
    if (len != 0)
        return;
    for (var i = 0; i < Databits.length; i++)
    {
        var opt = document.createElement("option");
        opt.setAttribute("id", Databits[i]);
        opt.setAttribute("value", Databits[i]);
        opt.innerHTML = Databits[i].toString();
        $("div#editWindow table.tableEditList tr#trdatabits select").append(opt);
    }   
};

Devices.createStopbitsSelect = function()
{
    var len = $("div#editWindow table.tableEditList tr#trstopbits select").children("option").length;
    if (len != 0)
        return;
    for (var i = 0; i < Stopbits.length; i++)
    {
        var opt = document.createElement("option");
        opt.setAttribute("id", Stopbits[i]);
        opt.setAttribute("value", Stopbits[i]);
        opt.innerHTML = Stopbits[i].toString();
        $("div#editWindow table.tableEditList tr#trstopbits select").append(opt);
    }   
};

Devices.createParitySelect = function()
{
    var len = $("div#editWindow table.tableEditList tr#trparity select").children("option").length;
    if (len != 0)
        return;
    for (var i = 0; i < Parities.length; i++)
    {
        var opt = document.createElement("option");
        opt.setAttribute("id", Parities[i]);
        opt.setAttribute("value", i);
        opt.innerHTML = eval("language.Settings_edits_parity_" + Parities[i]);
        $("div#editWindow table.tableEditList tr#trparity select").append(opt);
    }   
};

Devices.createAddConnectors = function(items, index)
{
    $("div#editWindow table.tableEditList tr#trhwaddr select").html("");
    var url = null;
    for(var i = 0; i < items.length; i++) {
        var item = items[i];
        var hwaddr = item.hwaddr;
        var option = document.createElement("option");
        option.innerHTML = hwaddr + "(" + item.description + ")";
        option.setAttribute("data-url", "getPortsForAddDevice&device_type=" + index + "&hwaddr="+hwaddr);
        option.setAttribute("data-index",index);
        option.setAttribute("value",hwaddr);
        option.setAttribute("active", item.active);
        option.setAttribute("style", "width: 60px;");
        $("div#editWindow table.tableEditList tr#trhwaddr select").append(option);
        if(i == 0) {
            if (item.active == "0") // pasif konnectörde read_period device eklede gerekmiyor
                $("div#editWindow table.tableEditList tr#trread_period").css("display", "none");
            else
                $("div#editWindow table.tableEditList tr#trread_period").css("display", "table-row");
            
            url = "getPortsForAddDevice&device_type=" + index + "&hwaddr="+hwaddr;
        }
    }
    Devices.getPortsForAddDevice(url, index);
};

Devices.createAddPorts = function(items, index)
{
    $("div#editWindow table.tableEditList tr#trnumber select").html("");
    var url = null;
    for(var i = 0; i < items.length; i++) {
        var item = items[i];
        var number = item.number;
        var value = number;
        if (item.description != undefined)
        value += "(" + item.description + ")";
        var objectid = item.objectid;
        var option = document.createElement("option");
        option.innerHTML = value;   
        option.setAttribute("data-number", number);
        option.setAttribute("data-objectid", objectid);
        option.setAttribute("value", number);
        option.setAttribute("maxDevAddr", item.maxDevAddr);
        $("div#editWindow table.tableEditList tr#trnumber select").append(option);
    }
};

Devices.validateTypes = function(json)
{
    var type = $("div#editWindow").attr("window-type");
    var object = $("div#editWindow").attr("object");
    var object_type = $("div#editWindow").attr("object-type");


    var inputs = "div#editWindow table.tableEditList tr input";
    var selects = "div#editWindow table.tableEditList tr select";
    var elements = $(selects + ", " + inputs);
    
    if (object == "sensors")
    {
        if (object_type == "binary_ping")
        {
            elements = [
                    $("div#editWindow table.tableEditList tr#tralarm_active input"),
                    $("div#editWindow table.tableEditList tr#trrecord_period input"),
                    $("div#editWindow table.tableEditList tr#trname input"),
                    $("div#editWindow table.tableEditList tr#trip  input")
                    ];
        }
        else if (object_type == "binary_telnet")
        {
            elements = [
                    $("div#editWindow table.tableEditList tr#tralarm_active input"),
                    $("div#editWindow table.tableEditList tr#trrecord_period input"),
                    $("div#editWindow table.tableEditList tr#trname input"),
                    $("div#editWindow table.tableEditList tr#trip  input"),
                    $("div#editWindow table.tableEditList tr#trport  input")
                    ];
        }
    }
    
    var readF = undefined;
    var writeF = undefined;
    
	var isvalid = true;
    for (var i = 0; i < elements.length; i++)
    {
        var element 	= $(elements[i]);
        var dataType 	= element.attr("data-type");
        var dataName    = element.attr("data-name");
        var dflt = element.attr("default");
        var minval 		= element.attr("minval");
        var maxval 		= element.attr("maxval");
        var jsON = {
            type: dataType,
            min: minval,
            max: maxval
        };
        
        if (element.is("input") && element.attr("type") != "checkbox")
        {
            if (dataName == "read_function_code")
                readF = true;
            if (dataName == "write_function_code")
                writeF = true;
                
            var value = element.val();
			
			if(value.length == 0)
			{
	        	if (dataName == "read_function_code")
	        		readF = false;
	        	if (dataName == "write_function_code")
	        		writeF = false;
        		
				if (dflt != undefined)
				{
					json[dataName] = dflt;
					continue;
				}			
			}

			jsON.val = value;
			jsON.ismandatory = "true";
			jsON.obj  		 = element;
			jsON.objfocus    = element;
			jsON.page        = $("div#editWindow td.alert");
			jsON.errprefix   = eval("language." + object + "_" + dataName) + " ";
			
			//validation of element
			if(!Validator.validate(jsON))
				isvalid = false;
			else if(isvalid)
				json[dataName] = value;
			
        }
        else if (element.is("select"))
        {
            json[dataName] = element.children("option:selected").attr("value");
        }
        else if (element.is("input") && element.attr("type") == "checkbox")
        {
            json[dataName] = element.prop("checked") ? "1":"0";
        }
    }

    if (object_type == "modbus_tcp")
    {
        json["modbus_addr"] = json.addr;
        json["addr"] = (parseInt($("div#editWindow table tr#trnumber select option:selected").attr("maxDevAddr")) + 1).toString();

    }


    if (isvalid)
    {
        if (object_type == "binary_ping")
        {
            if (type == "add")
            {
                json["alarm_value"] = "0";
                json["ip"] = json["ip"];
                json["type"] = sensorTypes.binary.toString();
                json["one_name"] = "open";
                json["zero_name"] = "close";
            }
        }
        else if (object_type == "binary_telnet")
        {
            if (type == "add")
            {
                json["alarm_value"] = "0";
                json["ip"] = json["ip"];
                json["port"] = json["port"];
                json["type"] = sensorTypes.binary.toString();
                json["one_name"] = "open";
                json["zero_name"] = "close";
            }
        }
     
        if (object == "sensors" && (type == "add" || type == "add-reffile"))
        {
            var devType = 1;
            if (type == "add")
                devType = parseInt($("div#edits ul.filters li option:selected").attr("device-type"));
            else if (type == "add-reffile")
                devType = parseInt($("ul#selectform li#type option:selected").attr("value"));
            var sensorType = parseInt(json.type);
            json["type"] = Devices.createSensorType(devType, sensorType).toString();
            firstSelect = sensorTypesToStr2(sensorType) + "Name";
        }
        
        if (object == "sensors")
        {
            if (isFloat(strToSensorType(object_type)))
            {
                var alarmLow = parseFloat($("div#editWindow table.tableEditList tr input[data-name='alarm_low']").val());
                var warningLow = parseFloat($("div#editWindow table.tableEditList tr input[data-name='warning_low']").val());
                var warningHigh = parseFloat($("div#editWindow table.tableEditList tr input[data-name='warning_high']").val());
                var alarmHigh = parseFloat($("div#editWindow table.tableEditList tr input[data-name='alarm_high']").val());
                
                if (!((alarmLow <= warningLow) && (warningLow <= warningHigh) && (warningHigh <= alarmHigh) && (alarmLow <= warningLow)))
                {
                    $("div#editWindow table.tableEditList tr input[data-name='alarm_low']").addClass("alert");
                    $("div#editWindow table.tableEditList tr input[data-name='warning_low']").addClass("alert");
                    $("div#editWindow table.tableEditList tr input[data-name='warning_high']").addClass("alert");
                    $("div#editWindow table.tableEditList tr input[data-name='alarm_high']").addClass("alert");
                    alertHtml = $("div#editWindow td.alert").html() + "<br />" +  language.Settings_edits_notvalidlimits;
                    $("div#editWindow td.alert").html(alertHtml);
                    isvalid = false;
                }   
            }
            
            if ($("div#editWindow table.tableEditList tr input[data-name='read_period']").length != 0) // pasifse
            {
                var recordPeriod = parseInt($("div#editWindow table.tableEditList tr input[data-name='record_period']").val());
                var readPeriod = parseInt($("div#editWindow table.tableEditList tr input[data-name='read_period']").val());
                if (recordPeriod < readPeriod)
                {
                    $("div#editWindow table.tableEditList tr input[data-name='record_period']").addClass("alert");
                    $("div#editWindow table.tableEditList tr input[data-name='read_period']").addClass("alert");
                    alertHtml = $("div#editWindow td.alert").html() + "<br />" +  language.Settings_edits_readperiodgreaterthanrecordperiod;
                    $("div#editWindow td.alert").html(alertHtml);
                    isvalid = false;
                }
            }
            
            if (readF != undefined && writeF != undefined)
            {
                if (readF && writeF)
                    json["read_write"] = "2";
                else if (readF)
                    json["read_write"] = "0";
                else if (writeF)
                    json["read_write"] = "1";
                else
                {
                    $("div#editWindow table.tableEditList tr input[data-name='read_function_code']").addClass("alert");
                    $("div#editWindow table.tableEditList tr input[data-name='write_function_code']").addClass("alert");
                    alertHtml = $("div#editWindow td.alert").html() + "<br />" +  language.Settings_edits_read_write_function_code_define;
                    $("div#editWindow td.alert").html(alertHtml);
                    isvalid = false;
                }
            }
        }
    }
    return isvalid;
};

Devices.createEditPort = function(port)
{
    var type = Devices.portTypeToObjectType(port.type);
    $("div#editWindow").attr("window-type", "update");
    $("div#editWindow").attr("object", "ports");
    $("div#editWindow").attr("object-type", type);  
    Devices.createEditWindow();
    Devices.FillEditPortFields(type,port);
};

Devices.FillEditPortFields = function(type, port)
{
    for (var key in portTypes)
    {
        if (type == key)
        {
            var params = eval("portParams." + key);
            for (var i = 0; i < params.length; i++)
            {
                if (params[i].access == access.rw)
                {
                    if (params[i].html == "select")
                    {
                        if (params[i].name == "parity")
                        {
                            $("div#editWindow table.tableEditList tr#trparity select option#" + Parities[parseInt(port[params[i].name])]).prop("selected", true);
                        }
                        else
                            $("div#editWindow table.tableEditList tr#tr" + params[i].name + " select option#" + port[params[i].name]).prop("selected", true);
                    }
                    else
                        $("div#editWindow tr#tr" + params[i].name + " input").val(eval("port." + params[i].name));
                }
                else
                    $("div#editWindow tr#tr" + params[i].name + " b").html(eval("port." + params[i].name));
            }
            $("div#editWindow table.tableEditList tr#trtype b").html(eval("language.ports_" + type));
            break;
        }
    }
};

Devices.portTypeToObjectType = function(type)
{
    var tt = parseInt(type);
    var val = "";
    for (var key in portTypes)
    {
        if (type == portTypes[key].value)
        {
            val = key;
            break;
        }
    }
    return val;
};

Devices.createEditConnector = function(connector)
{
    var type = Devices.connectorTransportToObjectType(connector.transport);
    $("div#editWindow").attr("window-type", "update");
    $("div#editWindow").attr("object", "connectors");
    $("div#editWindow").attr("object-type", type);
    Devices.createEditWindow();
    Devices.FillEditConnectorFields(type,connector);    
};

Devices.FillEditConnectorFields = function(type, connector)
{
    for (var key in connectorTypes)
    {
        if (type == key)
        {
            var params = eval("connectorParams." + key);
            for (var i = 0; i < params.length; i++)
            {
                if (params[i].access == access.rw)
                    $("div#editWindow tr#tr" + params[i].name + " input").val(eval("connector." + params[i].name));
                else
                    $("div#editWindow tr#tr" + params[i].name + " b").html(eval("connector." + params[i].name));
            }
            $("div#editWindow table.tableEditList tr#trtransport b").html(eval("language.connectors_" + type));
            $("div#editWindow table.tableEditList tr#tractive b").html(connector.active == "1" ? language.active_connector: language.passive_connector);
            break;
        }
    }
};

Devices.connectorTransportToObjectType = function(type)
{
    var tt = parseInt(type);
    var val = "";
    for (var key in connectorTypes)
    {
        if (type == connectorTypes[key].value)
        {
            val = key;
            break;
        }
    }
    return val;
};

Devices.createConnector = function(index)
{
    $("div#connectorList table").html("");
    if (Devices.connectors.length == 0)
    {
        Main.unloading();
        return;
    }
    for(var i = 0; i < Devices.connectors.length; i++) {

        var name = Devices.connectors[i].hwaddr;
        var ID = Devices.connectors[i].objectid.split("/");
        var getPort = "getPorts&hwaddr="+ ID[0];
        var description = Monitoring.characterLimit(Devices.connectors[i].description, 18);
        var tr = document.createElement("tr");
        var tdName = document.createElement("td");
        
        if(index == undefined) {
            index = 0;
        }

        if(i == index) {
            tdName.setAttribute("class","active connector_" + i);
        } else {
            tdName.setAttribute("class", "connector_" + i);
        }

        if(Devices.connectors[i].status=="1")
        {
            var attr = document.createAttribute("style"); 
            attr.value="color:#FFFFFF;background:rgb(255,168,76);filter:linear-gradient(to bottom,  rgba(255,168,76,1) 0%,rgba(255,123,13,1) 100%)";
            tdName.setAttributeNode(attr);
            tdName.attributes.background = "rgb(255,168,76)";
            tdName.attributes.background = "url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZmYTg0YyIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNmZjdiMGQiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+)";
            tdName.attributes.background = "-moz-linear-gradient(top,  rgba(255,168,76,1) 0%, rgba(255,123,13,1) 100%)";
            tdName.attributes.background = "-webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,168,76,1)), color-stop(100%,rgba(255,123,13,1)))";
            tdName.attributes.background = "-webkit-linear-gradient(top,  rgba(255,168,76,1) 0%,rgba(255,123,13,1) 100%)";
            tdName.attributes.background = "-o-linear-gradient(top,  rgba(255,168,76,1) 0%,rgba(255,123,13,1) 100%)";
            tdName.attributes.background = "-ms-linear-gradient(top,  rgba(255,168,76,1) 0%,rgba(255,123,13,1) 100%)";
            
        }
        else if(Devices.connectors[i].status=="2")
        {
            var attr = document.createAttribute("style");
            attr.value="color:#FFFFFF;background:rgb(255,168,76);filter:linear-gradient(to bottom,  rgba(255,168,76,1) 0%,rgba(255,123,13,1) 100%)";
            tdName.setAttributeNode(attr);
            tdName.attributes.background = "rgb(255,168,76)";
            tdName.attributes.background = "url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZmYTg0YyIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNmZjdiMGQiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+)";
            tdName.attributes.background = "-moz-linear-gradient(top,  rgba(255,168,76,1) 0%, rgba(255,123,13,1) 100%)";
            tdName.attributes.background = "-webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,168,76,1)), color-stop(100%,rgba(255,123,13,1)))";
            tdName.attributes.background = "-webkit-linear-gradient(top,  rgba(255,168,76,1) 0%,rgba(255,123,13,1) 100%)";
            tdName.attributes.background = "-o-linear-gradient(top,  rgba(255,168,76,1) 0%,rgba(255,123,13,1) 100%)";
            tdName.attributes.background = "-ms-linear-gradient(top,  rgba(255,168,76,1) 0%,rgba(255,123,13,1) 100%)";
            
        }
    

        var a = document.createElement("a");
        a.setAttribute("data-id", getPort);
        a.setAttribute("data-name", name);
        a.setAttribute("data-td", ".connector_" + i);
        a.innerHTML = description;
        var editURL = "&hwaddr=" + Devices.connectors[i].hwaddr; 
        var delURL = "removeConnector&hwaddr=" + Devices.connectors[i].hwaddr; 
        var tdButtons = document.createElement("span");
        tdButtons.setAttribute("class","buttons");
        var edit = document.createElement("input");
        var del = document.createElement("input");
        edit.setAttribute("type","submit");
        edit.setAttribute("id","edit");
        edit.setAttribute("data-id","connector_" + Devices.connectors[i].id);
        edit.setAttribute("data-url", editURL);
        edit.setAttribute("objectid", Devices.connectors[i].objectid);
        edit.setAttribute("value", language.Settings_edits_edit);
        del.setAttribute("type","submit");
        del.setAttribute("id","delete");
        del.setAttribute("data-id","connector_" + Devices.connectors[i].id);
        del.setAttribute("data-url", delURL);
        del.setAttribute("value", language.Settings_edits_delete);
        tdButtons.appendChild(edit);
        tdButtons.appendChild(del);
        tdName.appendChild(a);
        tdName.appendChild(tdButtons);
        tr.appendChild(tdName);
        $("div#connectorList table").append(tr);

    }
};

Devices.SortPorts = function()
{
    var tmpList = [];
    var len = Devices.ports.length;
    for (var i = 0; i < len; i++)
    {
        tmpList[parseInt(Devices.ports[i].number)] = Devices.ports[i];
    }

    var tmpList2 = [];
    var k = 0;
    for (var i = 0; i < tmpList.length; i++)
    {
        if (tmpList[i] != undefined)
            tmpList2[k++] = tmpList[i];
    }

    return tmpList2;
};

Devices.createPort = function(index)
{
    $("div#portList table").html("");
    Devices.ports = Devices.SortPorts();
    for(var i = 0; i < Devices.ports.length; i++) {
        var name = Monitoring.characterLimit(Devices.ports[i].objectid,18);
        var number = Devices.ports[i].number;
        var type = Devices.ports[i].type;
        var ID = Devices.ports[i].objectid.split("/");
        var getDevice = "&hwaddr=" +ID[0]+ "&portnum="+ ID[1];
        var tr = document.createElement("tr");
        var tdName = document.createElement("td");
        tdName.setAttribute("data-id",getDevice);
        tdName.setAttribute("data-number",number);

        if(index == undefined) {
            index = 0;
        }
        
        if(i == index) {
            tdName.setAttribute("class","active port_" + i);
        } else {
            tdName.setAttribute("class", "port_" + i);
        }
        var a = document.createElement("a");
        a.setAttribute("data-id", getDevice);
        a.setAttribute("data-name", name);
        a.setAttribute("data-type", type);
        a.setAttribute("data-td", ".port_" + i);
        a.innerHTML = name;
        var tdButtons = document.createElement("span");
        tdButtons.setAttribute("class","buttons");
        var edit = document.createElement("input");
        edit.setAttribute("type","submit");
        edit.setAttribute("id","edit");
        edit.setAttribute("data-id","port_"+Devices.ports[i].id);
        edit.setAttribute("data-url",getDevice);
        edit.setAttribute("objectid", Devices.ports[i].objectid);
        edit.setAttribute("value",language.Settings_edits_edit);
        tdButtons.appendChild(edit);
        tdName.appendChild(a);
        if(Devices.ports[i].type == 2) tdName.appendChild(tdButtons);
        if(Devices.ports[i].status=="1")
        {
            var attr = document.createAttribute("style");
            attr.value="color:#FFFFFF;background:rgb(255,168,76);filter:linear-gradient(to bottom,  rgba(255,168,76,1) 0%,rgba(255,123,13,1) 100%)";
            tdName.setAttributeNode(attr);
            tdName.attributes.background = "rgb(255,168,76)";
            tdName.attributes.background = "url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZmYTg0YyIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNmZjdiMGQiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+)";
            tdName.attributes.background = "-moz-linear-gradient(top,  rgba(255,168,76,1) 0%, rgba(255,123,13,1) 100%)";
            tdName.attributes.background = "-webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,168,76,1)), color-stop(100%,rgba(255,123,13,1)))";
            tdName.attributes.background = "-webkit-linear-gradient(top,  rgba(255,168,76,1) 0%,rgba(255,123,13,1) 100%)";
            tdName.attributes.background = "-o-linear-gradient(top,  rgba(255,168,76,1) 0%,rgba(255,123,13,1) 100%)";
            tdName.attributes.background = "-ms-linear-gradient(top,  rgba(255,168,76,1) 0%,rgba(255,123,13,1) 100%)";
            
            
        }
        else if(Devices.ports[i].status=="2")
        {
            var attr = document.createAttribute("style");
            attr.value="color:#FFFFFF;background:rgb(255,168,76);filter:linear-gradient(to bottom,  rgba(255,168,76,1) 0%,rgba(255,123,13,1) 100%)";
            tdName.setAttributeNode(attr);
            tdName.attributes.background = "rgb(255,168,76)";
            tdName.attributes.background = "url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZmYTg0YyIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNmZjdiMGQiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+)";
            tdName.attributes.background = "-moz-linear-gradient(top,  rgba(255,168,76,1) 0%, rgba(255,123,13,1) 100%)";
            tdName.attributes.background = "-webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,168,76,1)), color-stop(100%,rgba(255,123,13,1)))";
            tdName.attributes.background = "-webkit-linear-gradient(top,  rgba(255,168,76,1) 0%,rgba(255,123,13,1) 100%)";
            tdName.attributes.background = "-o-linear-gradient(top,  rgba(255,168,76,1) 0%,rgba(255,123,13,1) 100%)";
            tdName.attributes.background = "-ms-linear-gradient(top,  rgba(255,168,76,1) 0%,rgba(255,123,13,1) 100%)";
            
            
        }
        tr.appendChild(tdName);
        $("div#portList table").append(tr);
    }
};

Devices.createDevice = function()
{
    $("div#deviceList table").html("");
    for(var i = 0; i < Devices.devices.length; i++) {
        var name = Monitoring.characterLimit(Devices.devices[i].name, 18);
        var ID = Devices.devices[i].objectid.split("/");
        var getDevice = "getDevice&hwaddr=" +ID[0]+ "&portnum="+ ID[1] + "&devaddr=" + ID[2];
        var tr = document.createElement("tr");
        var tdName = document.createElement("td");
        tdName.setAttribute("id", "tdName");
        tdName.setAttribute("data-id",getDevice);
        tdName.setAttribute("data-name", Devices.devices[i].objectid);
        tdName.innerHTML = name;
        if (Devices.devices[i].name.length > 18)
            tdName.setAttribute("data-title", Devices.devices[i].name);

        var editURL = "&hwaddr=" +ID[0]+ "&portnum="+ ID[1] + "&devaddr=" + ID[2];
        var moveURL = "moveDevice&hwaddr=" +ID[0]+ "&portnum="+ ID[1] + "&devaddr=" + ID[2];
        var delURL = "removeDevice&hwaddr=" +ID[0]+ "&portnum="+ ID[1] + "&devaddr=" + ID[2];

        var tdButtons = document.createElement("span");
        tdButtons.setAttribute("class","buttons");
        var edit = document.createElement("input");
        var gotosensors = document.createElement("input");
        var del = document.createElement("input");
        edit.setAttribute("type","submit");
        edit.setAttribute("id","edit");
        edit.setAttribute("data-id","device_" + Devices.devices[i].id);
        edit.setAttribute("data-url",editURL);
        edit.setAttribute("objectid", Devices.devices[i].objectid);
        edit.setAttribute("data-type", Devices.devices[i].type);
        edit.setAttribute("value",language.Settings_edits_edit);
        gotosensors.setAttribute("type","submit");
        gotosensors.setAttribute("id","gotosensors");
        gotosensors.setAttribute("data-id","device_" + Devices.devices[i].id);
        gotosensors.setAttribute("data-url",Devices.devices[i].objectid);
        gotosensors.setAttribute("value",language.Settings_edits_gotoSensors);
        del.setAttribute("type","submit");
        del.setAttribute("id","delete");
        del.setAttribute("data-id","device_" + Devices.devices[i].id);
        del.setAttribute("data-url",delURL);
        del.setAttribute("value",language.Settings_edits_delete);
        tdButtons.appendChild(gotosensors);
        tdButtons.appendChild(edit);
        tdButtons.appendChild(del);
        tdName.appendChild(tdButtons);
        
        if(Devices.devices[i].status=="1")
        {
            var attr = document.createAttribute("style");
            attr.value="color:#FFFFFF;background:rgb(255,168,76);filter:linear-gradient(to bottom,  rgba(255,168,76,1) 0%,rgba(255,123,13,1) 100%)";
            tdName.setAttributeNode(attr);
            tdName.attributes.background = "rgb(255,168,76)";
            tdName.attributes.background = "url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIwJSIgeTI9IjEwMCUiPgogICAgPHN0b3Agb2Zmc2V0PSIwJSIgc3RvcC1jb2xvcj0iI2ZmYTg0YyIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgICA8c3RvcCBvZmZzZXQ9IjEwMCUiIHN0b3AtY29sb3I9IiNmZjdiMGQiIHN0b3Atb3BhY2l0eT0iMSIvPgogIDwvbGluZWFyR3JhZGllbnQ+CiAgPHJlY3QgeD0iMCIgeT0iMCIgd2lkdGg9IjEiIGhlaWdodD0iMSIgZmlsbD0idXJsKCNncmFkLXVjZ2ctZ2VuZXJhdGVkKSIgLz4KPC9zdmc+)";
            tdName.attributes.background = "-moz-linear-gradient(top,  rgba(255,168,76,1) 0%, rgba(255,123,13,1) 100%)";
            tdName.attributes.background = "-webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,168,76,1)), color-stop(100%,rgba(255,123,13,1)))";
            tdName.attributes.background = "-webkit-linear-gradient(top,  rgba(255,168,76,1) 0%,rgba(255,123,13,1) 100%)";
            tdName.attributes.background = "-o-linear-gradient(top,  rgba(255,168,76,1) 0%,rgba(255,123,13,1) 100%)";
            tdName.attributes.background = "-ms-linear-gradient(top,  rgba(255,168,76,1) 0%,rgba(255,123,13,1) 100%)";
            
        }
        tr.appendChild(tdName);
        $("div#deviceList table").append(tr);
    }
};

Devices.createSensorType = function(devType, sensorType)
{
    var sensorT = sensorTypesToStr(sensorType);
    var deviceT = deviceTypesToStr(devType);
    var name = "";
    if (devType == deviceTypes.internal.value || devType == deviceTypes.pnp.value )
        name =  "sensorTypes." + sensorT;
    else
        name = "sensorTypes." + sensorT + "_" + deviceT;
    return eval(name);
};

$(function() {

    $("div#devices").on( "click", "div#connectorList table td a", function() {
        var tdClass = $(this).attr("data-td");
        if($("div#connectorList table").find(tdClass).hasClass("active") == false) {
            $("div#portList table").html("");
            var url = $(this).attr("data-id");
            $("div#connectorList table td").removeClass("active");
            $("div#connectorList table").find(tdClass).addClass("active");
            Devices.getPorts(url);
        }
    });

    $("div#devices").on( "click", "div#portList table td a", function() {
        var tdClass = $(this).attr("data-td");
        if($("div#portList table").find(tdClass).hasClass("active") == false) {
            $("div#deviceList table").html("");
            var url = $(this).attr("data-id");
            $("div#portList table td").removeClass("active");
            $("div#portList table").find(tdClass).addClass("active");
            Devices.getDevices("getDevices" + url);
        }
    }); 

    $("div#devices").on( "click", "div#connectorList span input#edit", function() {
        $("div#editWindow td.alert").html("");
        var url = $(this).attr("data-url");
        $("div#overlay").css("display","block");
        $("div#editWindow").css("display","block");
        $("div#editWindow input#buttonUpdate").val(language.update);
        $("div#editWindow input#buttonCancel").val(language.cancel);
        $("div#editWindow td#modulsensorname").html(language.updateconnector + ": " + $(this).attr("objectid"));
        $("div#editWindow").attr("objectid", $(this).attr("objectid"));
        Devices.getEditConnectors("getConnector" + url);
    });

    $("div#devices").on( "click", "div#connectorList table tr td input#delete", function() {
        var url = $(this).data("url");

        $("div#overlay").css("display","block");
        $("div#alertDelete").css("display","block");

        $("div#alertDelete input#buttonRemove").val(language.remove);
        $("div#alertDelete input#buttonRemove").data("url", url);
        $("div#alertDelete input#buttonRemove").removeClass("sensorDelete");
        $("div#alertDelete input#buttonRemove").removeClass("deviceDelete");
        $("div#alertDelete input#buttonRemove").addClass("connectorDelete");
        $("div#alertDelete input#buttonClose").val(language.close);
        $("div#alertDelete span.text").html(language.Settings_delete_connector);    
    });

    $("div#devices").on( "click", "div#alertDelete input#buttonClose", function() {
        $("div#overlay").css("display","none");
        $("div#alertDelete").css("display","none");
    });

    $("div#devices").on( "click", "div#alertDelete input.connectorDelete", function() {
        Main.loading();
        var url = $(this).data("url");
        $("div#overlay").css("display","none");
        $("div#alertDelete").css("display","none");
        $("div#alertDelete input#buttonRemove").removeClass("sensorDelete");
        Devices.removeConnector(url);
    }); 

    $("div#devices").on( "click", "div#deviceList table tr td input#delete", function() {
        var url = $(this).data("url");

        $("div#overlay").css("display","block");
        $("div#alertDelete").css("display","block");
        $("div#alertDelete input#buttonRemove").val(language.remove);
        $("div#alertDelete input#buttonRemove").data("url", url);
        $("div#alertDelete input#buttonRemove").removeClass("sensorDelete");
        $("div#alertDelete input#buttonRemove").removeClass("connectorDelete");
        $("div#alertDelete input#buttonRemove").addClass("deviceDelete");
        $("div#alertDelete input#buttonClose").val(language.close);
        $("div#alertDelete span.text").html(language.Settings_delete_device);   
    });

    $("div#devices").on( "click", "div#alertDelete input.deviceDelete", function() {
        Main.loading();
        var url = $(this).data("url");
        $("div#overlay").css("display","none");
        $("div#alertDelete").css("display","none");
        Devices.removeDevice(url);
    }); 

    $("div#devices").on( "click", "div#portList table tr td input#edit", function() {
        $("div#editWindow td.alert").html("");
        var url = $(this).attr("data-url");
        $("div#overlay").css("display","block");
        $("div#editWindow").css("display","block");
        $("div#editWindow input#buttonUpdate").val(language.update);
        $("div#editWindow input#buttonCancel").val(language.cancel);
        $("div#editWindow td#modulsensorname").html(language.updateport + ": " + $(this).attr("objectid"));
        $("div#editWindow").attr("objectid", $(this).attr("objectid"));
        Devices.getEditPort("getPort" + url);
    });

    $("div#devices").on( "click", "div#deviceList table tr td input#edit", function() {
        $("div#editWindow td.alert").html("");
        var url = $(this).data("url");
        $("div#overlay").css("display","block");
        $("div#editWindow").css("display","block");
        $("div#editWindow input#buttonUpdate").val(language.update);
        $("div#editWindow input#buttonCancel").val(language.cancel);
        $("div#editWindow td#modulsensorname").html(language.updatedevice + ": " + $(this).attr("objectid"));
        $("div#editWindow").attr("objectid", $(this).attr("objectid"));
        Devices.getEditDevice("getDevice" + url);
    });
    
    
    $("div#devices").on( "click", "div#deviceList table tr td input#gotosensors", function() {
        $("div#editWindow td.alert").html("");
        var objectid = $(this).data("url");
        $("div#devices ul.tabs li.active").removeClass("active");
        $("div#devices ul.tabs li#Devices_sensors").addClass("active");
        Settings.callEdits(objectid);
    });

    $("div#devices").on( "click", "div.addButtons input#moduletree", function() {
        Devices.createModulTree();
    });

    $("div#devices").on( "click", "div.addButtons input#discovery", function() {
        Devices.getDiscovery();
    });

    $("div#devices").on( "click", "div.addButtons input#addDevice", function() {
        SelectType = undefined;
        $("div#editWindow tr#trtype select").prop("selectedIndex", 0);
        $("div#editWindow td.alert").html("");
        $("div#overlay").css("display","block");
        $("div#editWindow td.alert").html("");
        $("div#editWindow").css("display","block");
        $("div#editWindow input#buttonUpdate").val(language.save);
        $("div#editWindow input#buttonCancel").val(language.cancel);    
        $("div#editWindow td#modulsensorname").html(language.adddevice);
        $("div#editWindow").attr("window-type", "add");
        $("div#editWindow").attr("object", "devices");
        $("div#editWindow").attr("object-type", "modbus_rtu");  
        Devices.createEditWindow();
        Devices.DeviceFieldsToDefaults();       
    });
    
    $("div#devices").on( "click", "div.addButtons input#moveDevice", function() {
        $("div#editWindow tr#trtype select").prop("selectedIndex", 0);
        $("div#editWindow td.alert").html("");
        $("div#overlay").css("display","block");
        $("div#editWindow td.alert").html("");
        $("div#editWindow").css("display","block");
        $("div#editWindow input#buttonUpdate").val(language.move);
        $("div#editWindow input#buttonCancel").val(language.cancel);    
        $("div#editWindow td#modulsensorname").html(language.devices_table_movedevice);
        $("div#editWindow").attr("window-type", "move");
        $("div#editWindow").attr("object", "devices");
        $("div#editWindow").attr("object-type", "devices");  
        Devices.createEditWindow();
    });

    $("div#devices").on("mouseover", "div#connectorList a", function() {
        var title = $(this).attr("data-name");
        if(title != undefined ) {
            var t = document.createElement("b");
            t.setAttribute("id","tooltip");
            t.innerHTML = title;
            $(this).append(t);
        }
    });

    $("div#devices").on("mouseout", "div#connectorList a", function() {
        $("#tooltip").remove();
    });

    /// EDIT WINDOW EVENTS

    $("div#content").on( "change", "div#editWindow tr#trtype select", function() {
        if ($("div#editWindow").attr("object") == "devices")
        {
            $("div#editWindow").attr("object-type", $("div#editWindow tr#trtype select option:selected").attr("id"));
            $("div#editWindow td.alert").html("");
            SelectType = deviceTypes[$("div#editWindow").attr("object-type")].value;
            Devices.createEditWindow();
            Devices.DeviceFieldsToDefaults();
        }
        else if ($("div#editWindow").attr("object") == "sensors")
        {
            var sensType = parseInt($("div#editWindow tr#trtype select option:selected").attr("value"));
            var windowType = $("div#editWindow").attr("window-type");
            var devType = "";
            if (windowType == "add")
                devType = parseInt($("div#edits ul.filters li option:selected").attr("device-type"));
            else if (windowType == "add-reffile")
                devType = parseInt($("ul#selectform li#type option:selected").attr("value"));
            $("div#editWindow td.alert").html("");
            $("div#editWindow input").removeClass("alert");
            var sensorType = Devices.createSensorType(devType, sensType);
            $("div#editWindow").attr("object-type", sensorTypesToStr(sensorType));
            SelectType = sensType;
            Devices.createEditWindow();
            Devices.DeviceFieldsToDefaults();
        }
    });
    
    $("div#content").on( "change", "div#editWindow table.tableEditList tr#trhwaddr select", function() {
        var hwaddr = $("div#editWindow tr#trhwaddr select option:selected").attr("value");
        $("div#editWindow td.alert").html("");
        if ($("div#editWindow tr#trhwaddr select option:selected").attr("active") == "0") // pasif konnectörde read_period device eklede gerekmiyor
            $("div#editWindow table.tableEditList tr#trread_period").css("display", "none");
        else
            $("div#editWindow table.tableEditList tr#trread_period").css("display", "table-row");
        var type = $("div#editWindow tr#trtype select option:selected").attr("value");
        var url = "getPortsForAddDevice&device_type=" + type + "&hwaddr="+hwaddr;
        Devices.getPortsForAddDevice(url, type);
    });
    
    
    $("div#content").on( "change", "div#editWindow table.tableEditList tr#trconnector_from select", function() {
        var hwaddr = $("div#editWindow tr#trhwaddr select option:selected").attr("value");
        $("div#editWindow td.alert").html("");
        objectid = $("div#editWindow table.tableEditList tr#trconnector_from select option:selected").attr("objectid");
        Devices.getMovablePorts(objectid);
    });
    
    $("div#content").on( "change", "div#editWindow table.tableEditList tr#trport_from select", function() {
        var hwaddr = $("div#editWindow tr#trhwaddr select option:selected").attr("value");
        $("div#editWindow td.alert").html("");
        objectid = $("div#editWindow table.tableEditList tr#trport_from select option:selected").attr("objectid");
        Devices.getMovableDevices(objectid);
    });
    
    
    $("div#content").on( "change", "div#editWindow table.tableEditList tr#trconnector_to select", function() {
        var hwaddr = $("div#editWindow tr#trhwaddr select option:selected").attr("value");
        $("div#editWindow td.alert").html("");
        objectid = $("div#editWindow table.tableEditList tr#trconnector_to select option:selected").attr("objectid");
        devoid = $("div#editWindow table.tableEditList tr#trmovable select option:selected").attr("objectid");
        devType = $("div#editWindow table.tableEditList tr#trmovable select option:selected").attr("type");
        Devices.getPortsForMoveDevice(devoid, objectid, devType);
    });


    $("div#content").on( "change", "div#editWindow tr#trversion select", function() {
        if ($(this).prop("selectedIndex") == 2)//v3
        {
            $("div#editWindow tr#trsecurity_name").css("display", "table-row");
            $("div#editWindow tr#trpassword").css("display", "table-row");
        }
        else
        {       
            $("div#editWindow tr#trsecurity_name").css("display", "none");
            $("div#editWindow tr#trpassword").css("display", "none");
        }
    });
    $("div#content").on( "click", "div#editWindow input#buttonCancel", function() {
        $("div#overlay").css("display","none");
        $("div#editWindow").css("display","none");
        $("div#editWindow input#buttonUpdate").css("display","inline-block");
        $("div#editWindow td#modulsensorname").html("");
        $("div#editWindow td.alert").html("");
        $("div#editWindow input").removeClass("alert");
    });

    $("div#content").on( "click", "div#editWindow input#buttonUpdate", function() {
        $("div#editWindow td.alert").html("");
        $("div#editWindow input").removeClass("alert");
        var url = $(this).attr("data-url");
        var page = $("div#editWindow").attr("window-type");
        var group = $("div#editWindow").attr("object");
        var objectid = $("div#editWindow").attr("objectid");    
        var item = {};
        
        if (page == "add")
        {
            if(group == "connectors")
            {
            }
            else if(group == "ports")
            {
            }
            else if(group == "devices")
            {
                if (Devices.validateTypes(item))
                {   
                    Devices.addDevice(item);
                    $("div#overlay").css("display","none");
                    $("div#editWindow").css("display","none");
                }
            }
            else if(group == "sensors")
            {
                if (Devices.validateTypes(item))
                {   
                    Devices.addSensor(item);
                    $("div#overlay").css("display","none");
                    $("div#editWindow").css("display","none");
                }
            }
        }
        else if (page == "update")
        {
            if (Devices.validateTypes(item))
            {
                if(group == "connectors")
                {
                    Devices.updateConnector(objectid,item);
                }
                else if(group == "ports")
                {
                    Devices.updatePort(objectid,item);
                }
                else if(group == "devices")
                {   
                    Devices.updateDevice(objectid, item);
                }
                else if(group == "sensors")
                {   
                    Devices.updateSensor(objectid, item);
                }
                $("div#overlay").css("display","none");
                $("div#editWindow").css("display","none");
            }
        }
        else if (page == "add-reffile")
        {
            if (Devices.validateTypes(item))
            {
                var sensorType = parseInt(item.type);
                var deviceType = DeviceTypeFromSensType(sensorType);
                var sensorDataType = DataTypeFromSensType(sensorType);
                var strSensorDataType = sensorTypesToStr2(sensorDataType);
                if (eval("RefFilesCreate." + strSensorDataType + "Count") == 0)
                    $("div#settings div#generate div#Header table").html("");
                RefFilesCreate.addSensor(item);
                RefFilesCreate.activateFields(deviceType, sensorDataType, displayStatus);
                $("div#overlay").css("display","none");
                $("div#editWindow").css("display","none");
            }
        }
        else if (page == "move")
        {
            if (group == "devices")
            {
                Devices.MoveDevice();
            }
            
        }
    });
    
    $("div#devices").on("mouseover", "div#deviceList table tr td#tdName", function(){
        var title = $(this).attr("data-title");
        if(title != undefined ) {
            var t = document.createElement("b");
            t.setAttribute("id","tooltip");
            t.innerHTML = title;
            $(this).append(t);
        }
    });
    
    $("div#devices").on("mouseout", "div#deviceList table tr td#tdName", function(){
        $("#tooltip").remove();
            $("div#settings div#deviceList").css("overflow", "hidden");
            $("div#settings div#deviceList").css("overflow", "auto");
    });
    
    $("#content").on("click", "div#editWindow div.buttons input#outputsettings", function() {
        var objectid = $("div#editWindow").attr("objectid");
        var interval = $(this).attr("interval");
        // Devices.createCalibWindow("2", objectid, interval); 
    });
});