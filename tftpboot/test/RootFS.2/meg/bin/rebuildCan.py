#!/usr/bin/python 

import os
import subprocess
from time import sleep

canList = [
		'ip link set can0 down', 
		'canconfig can0 bitrate 52000 sjw 2 phase-seg1 8 phase-seg2 7 prop-seg 8',
		'ip link set can0 up'
		]

for i in canList:
	os.system(i)
