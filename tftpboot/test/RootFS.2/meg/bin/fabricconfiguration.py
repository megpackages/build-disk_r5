#!/usr/bin/python

import sqlite3
import os,time,subprocess,sys,signal
from UserString import MutableString
from threading import Timer,Thread
import string
from __builtin__ import len

def main(argv):
    db = sqlite3.connect("/meg/etc/SPXconfig.sqlite")
    c = db.cursor()

    try:
        c.execute("update network_settings set ip_addr='192.168.1.128',dhcp_enabled='0', gateway_addr='192.168.1.1', subnet_mask='255.255.255.0'")
    except Exception as e:
        print "Exception:", str(e)
        
    db.commit()
    db.close()

    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))

