#!/usr/bin/python

import sqlite3
import random
import os,time,subprocess,sys,signal
from UserString import MutableString
from threading import Timer,Thread
import string
import json
from __builtin__ import len



M_SP_TB_1 = 30
M_ME_TF_1 = 36
M_DOUBLEPOINT = 31
M_BITSTRING = 33

def isBinarySensor(iType):
    return (iType & 0x0000FFFF == 2)

def isFloatSensor(iType):
    return (iType & 0x0000FFFF == 1)

def isOutputSensor(iType):
    return (iType & 0x0000FFFF == 3)

def isBitStringSensor(iType):
    return (iType & 0x0000FFFF == 5)
    
def getuMsg(sensorType, leng = 0):
    if (isBinarySensor(sensorType)):
        return M_SP_TB_1
    elif (isFloatSensor(sensorType)):
        return M_ME_TF_1
    elif (isOutputSensor(sensorType)):
        return M_SP_TB_1
    elif (isBitStringSensor(sensorType)):
        if (leng == 2):
            return M_DOUBLEPOINT
        else:
            return M_BITSTRING

def getstrmsg(sensorType, leng = 0):
    if (isBinarySensor(sensorType)):
        return "single-point information with time tag"
    elif (isFloatSensor(sensorType)):
        return "floating point with time tag"
    elif (isOutputSensor(sensorType)):
        return "single-point information with time tag"
    elif (isBitStringSensor(sensorType)):
        if (leng == 2):
            return "double-point information with time tag"
        else:
            return "bitstring of 32 bits with time tag"
            
def GetDeviceObjectId(obj):
    a = obj.split('/')
    return a[0] + "/" + a[1] + "/" + a[2]


def GetPayload(formatType, address):
    payload = ""
    if (formatType == 0):        
        payload += str((address & 0x00FF0000) >> 16) + ";"
        payload += str((address & 0x0000FF00) >> 8) + ";"
        payload += str(address & 0x000000FF)
    elif (formatType == 1):
        payload += str((address & 0x00FFFF00) >> 8) + ";"
        payload += str(address & 0x000000FF)
    else :
        payload += str((address & 0x00FF0000) >> 16) + ";"
        payload += str((address & 0x0000FFFF))
        
    return payload 

def GetSensorProperty(sensorlist, obj):
    for sensor in sensorlist:
        if (sensor[0] == obj):
            return sensor

def GetDeviceProperty(devlist, obj):
    objectId = GetDeviceObjectId(obj)
    for dev in devlist:
        if (dev[1] == objectId):
            return dev
    
    
def ExportTable(c):
    c.execute("select * from mdlmngr_devices")
    devlist = c.fetchall()
    
    c.execute("select * from mdlmngr_sensors")
    sensorlist = c.fetchall()
    
    c.execute("select * from sensorexport_export_formats where protocol_name='iec60870'")
    row = c.fetchall()
    formatType = 0
    for i in row:
        formatType = i[1]

    firstPaylaod = ";PTA;STA;BAY;CIHAZ;SIG;CASDU1;CASDU2;"
    if (formatType == 0):
        firstPaylaod += "IOA1;IOA2;IOA3"
    else :
        firstPaylaod += "IOA1;IOA2"
        
    firstPaylaod += ";T1;T1 Description "   
    print firstPaylaod
    c.execute("select * from sensorexport_iec60870")
    exportlist = c.fetchall()
    
    for sensor in exportlist:
        propSensor = GetSensorProperty(sensorlist, sensor[0])
        if not propSensor:
#            print "Exception:sensor does not exist. id:", sensor[0]
            continue

        propDevice = GetDeviceProperty(devlist, propSensor[1])
        
        struMsg = 0
        strMsg = ""
        if (isBitStringSensor(propSensor[3])):
            c.execute("select * from mdlmngr_sensors_bitstring where id='" + str(propSensor[0]) + "'")
            row = c.fetchall()
            for i in row :
                for k in i:
                    struMsg = str(getuMsg(propSensor[3], i[1]))
                    strMsg = getstrmsg(propSensor[3], i[1]) 
        else:
            strMsg = str(getstrmsg(propSensor[3]))
            struMsg = str(getuMsg(propSensor[3]))
                       
        
        sss = ";ITU;" + str(propDevice[4]) + ";;" + str(propDevice[3]) + ";" + str(propDevice[5]) + ";1;0;" + \
                GetPayload(formatType, int(sensor[1])) + ";TI " + struMsg + \
                ";" + strMsg + ";;" + sensor[1]      
        print sss        
         


def main(argv):
    db = sqlite3.connect("/meg/etc/SPXconfig.sqlite")
    c = db.cursor()
    
    try:
        ExportTable(c)
    except Exception as e:
        print "Exception:", str(e)
        
    db.commit()
    db.close()

    return 0

if __name__ == "__main__":
    sys.exit(main(sys.argv))
