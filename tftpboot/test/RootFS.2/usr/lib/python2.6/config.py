#!/usr/bin/python
import os
import sqlite3
import subprocess

config_path = "/media/config/SPXconfig.sqlite"
config_backup_path = "/media/config/SPXconfig.sqlite.backup"
data_path = "/media/mmc/SPXdata.sqlite"
journal_path = "/media/mmc/journal.sqlite"

def getNetworkSettings():
    macAddr     = "n/a"
    ipAddr      = "n/a"
    subnetMask  = "n/a"
    gatewayAddr = "n/a"
    dhcpEnabled = "n/a"
    dnsIp       = "n/a"
    p = subprocess.Popen(['readIp.sh', ''], stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    out, err = p.communicate()
    networkInfo=out.split('#')
    if len(networkInfo) == 7:
        macAddr     = networkInfo[0]
        ipAddr      = networkInfo[1]
        subnetMask  = networkInfo[2]
        gatewayAddr = networkInfo[3]
        dhcpEnabled = networkInfo[4]
        dnsIp       = networkInfo[5]

    sqlinsert = "insert into network_settings(mac_addr ,ip_addr ,gateway_addr ,subnet_mask ,dhcp_enabled ,xml_server_port ,dns_server_ip)\
    values('" + macAddr + "','" + ipAddr + "','" + gatewayAddr + "','" + subnetMask + "','" + dhcpEnabled + "','1000','" + dnsIp + "');" 
    
    sqlupdate = "update network_settings set mac_addr='" + macAddr + "' , ip_addr='" + ipAddr + "' , gateway_addr='" + gatewayAddr + "' , subnet_mask='" +subnetMask + "' , dhcp_enabled='" +  dhcpEnabled + "', xml_server_port='1000',  dns_server_ip='" + dnsIp +"' where rowid = '1';"    
    return [sqlinsert, sqlupdate]

def updateNetworkSettings():
    networksettings = getNetworkSettings()
    print networksettings
    db = sqlite3.connect(config_path)
    c = db.cursor()
    try:
        c.execute("select min(rowid) from network_settings;");
        r = c.fetchall()
        if len(r) == 1 and len(r[0]) == 1:
            try:
                test = int(r[0][0])
            except BaseException as e:
                raise e
            print "update"
            c.execute(networksettings[1]);
    except BaseException as e:
        print str(e)
        print "insert"
        c.execute(networksettings[0]);
    db.commit()
    db.close()
        
UpdateNetworkSettings = getNetworkSettings()

config = [
["CREATE TABLE mdlmngr_connectors (\
    id INTEGER PRIMARY KEY AUTOINCREMENT,\
    objectid TEXT NOT NULL,\
    hwaddr TEXT NOT NULL,\
    type INTEGER NOT NULL,\
    transport INTEGER NOT NULL, \
    ping_period INTEGER,\
    version TEXT,\
    description TEXT,\
    active INTEGER\
);"],

["CREATE TABLE mdlmngr_connectors_transport_can (\
    id INTEGER PRIMARY KEY,\
    canid INTEGER NOT NULL\
);"],

["CREATE TABLE mdlmngr_connectors_transport_proc (\
    id INTEGER PRIMARY KEY,\
    process TEXT NOT NULL\
);"],

["CREATE TABLE mdlmngr_connectors_transport_tcpv4 (\
    id INTEGER PRIMARY KEY, \
    hostaddr TEXT NOT NULL,\
    hostport INTEGER NOT NULL\
);"],

["CREATE TABLE mdlmngr_ports (\
    id INTEGER PRIMARY KEY AUTOINCREMENT,\
    objectid TEXT NOT NULL,\
    number INTEGER NOT NULL,\
    type INTEGER NOT NULL\
);"],

["CREATE TABLE mdlmngr_ports_serial (\
    id INTEGER PRIMARY KEY, \
    boudrate INTEGER,\
    databits INTEGER,\
    stopbits INTEGER,\
    parity INTEGER\
);"],

["CREATE TABLE mdlmngr_devices (\
    id INTEGER PRIMARY KEY AUTOINCREMENT,\
    objectid TEXT NOT NULL,\
    addr TEXT NOT NULL,\
    name TEXT, \
    type INTEGER NOT NULL,\
    request_timeout INTEGER NOT NULL,\
    read_period INTEGER NOT NULL,\
    state INTEGER NOT NULL,\
    losttime INTEGER NOT NULL\
);"],

["CREATE TABLE mdlmngr_devices_transparent (\
    id INTEGER PRIMARY KEY, \
    plugin_path TEXT NOT NULL,\
    ref_file TEXT \
);"],

["CREATE TABLE mdlmngr_devices_modbus (\
    id INTEGER PRIMARY KEY, \
    ref_file TEXT\
);"],
          
["CREATE TABLE mdlmngr_devices_modbus_tcp (\
    id INTEGER PRIMARY KEY, \
    ref_file TEXT NOT NULL,\
    modbus_addr TEXT NOT NULL,\
    ip TEXT NOT NULL,\
    port INTEGER NOT NULL\
);"],
          
["CREATE TABLE mdlmngr_devices_meter (\
    id INTEGER PRIMARY KEY, \
    ref_file TEXT\
);"],

["CREATE TABLE mdlmngr_devices_snmp (\
    id INTEGER PRIMARY KEY, \
    port INTEGER NOT NULL,\
    community_name TEXT NOT NULL,\
    version TEXT NOT NULL,\
    security_name TEXT NOT NULL,\
    ref_file TEXT,\
    password TEXT NOT NULL\
);"],

["CREATE TABLE mdlmngr_devices_iec61850 (\
    id INTEGER PRIMARY KEY, \
    port INTEGER NOT NULL, \
    dataset_location TEXT NOT NULL, \
    ref_file TEXT \
);"],
          
["CREATE TABLE mdlmngr_devices_pnp (\
    id INTEGER PRIMARY KEY, \
    hwaddr TEXT NOT NULL\
);"],


["CREATE TABLE mdlmngr_sensors (\
    id INTEGER PRIMARY KEY AUTOINCREMENT,\
    objectid TEXT NOT NULL,\
    ionum INTEGER NOT NULL,\
    type INTEGER NOT NULL,\
    read_write INTEGER NOT NULL,\
    name TEXT,\
    alarm_active INTEGER,\
    record_period INTEGER,\
    read_period INTEGER,\
    read_enable INTEGER,\
    record INTEGER,\
    enable INTEGER,\
    is_time_limit_set INTEGER,\
    phy_type INTEGER\
);"],

["CREATE TABLE mdlmngr_sensors_binary (\
    id INTEGER PRIMARY KEY, \
    alarm_value INTEGER NOT NULL, \
    zero_name TEXT, \
    one_name TEXT, \
    time_limit INTEGER NOT NULL, \
    hys_time_limit INTEGER NOT NULL\
);"],

["CREATE TABLE mdlmngr_sensors_float (\
    id INTEGER PRIMARY KEY, \
    unit TEXT NOT NULL, \
    warning_low REAL NOT NULL, \
    warning_high REAL NOT NULL, \
    alarm_low REAL NOT NULL, \
    alarm_high REAL NOT NULL, \
    time_limit INTEGER NOT NULL, \
    hys_time_limit INTEGER NOT NULL,\
    filter_type REAL NOT NULL,\
    filter_value INTEGER NOT NULL,\
    multiplier REAL NOT NULL\
);"],

["CREATE TABLE mdlmngr_sensors_output (\
    id INTEGER PRIMARY KEY, \
    poweron INTEGER,\
    apply INTEGER NOT NULL,\
    output_interval INTEGER NOT NULL\
);"],

["CREATE TABLE mdlmngr_sensors_meter (\
    id INTEGER PRIMARY KEY,\
    obis_code TEXT NOT NULL\
);"],

["CREATE TABLE mdlmngr_sensors_modbus (\
    id INTEGER PRIMARY KEY, \
    regaddr TEXT NOT NULL,\
    read_function_code INTEGER NOT NULL, \
    write_function_code INTEGER NOT NULL, \
    regcnt INTEGER NOT NULL, \
    issigned INTEGER\
);"],

["CREATE TABLE mdlmngr_sensors_snmp (\
    id INTEGER PRIMARY KEY,\
    oid TEXT NOT NULL\
);"],

["CREATE TABLE mdlmngr_sensors_ping(\
    id INTEGER PRIMARY KEY,\
    ip TEXT NOT NULL\
);"],

["CREATE TABLE mdlmngr_sensors_telnet(\
    id INTEGER PRIMARY KEY,\
    ip TEXT NOT NULL,\
    port INTEGER NOT NULL\
);"],

["CREATE TABLE mdlmngr_sensors_regex (\
    id INTEGER PRIMARY KEY,\
    pattern TEXT NOT NULL,\
    replace TEXT NOT NULL\
    );"],

["CREATE TABLE mdlmngr_sensors_iec61850 (\
    id INTEGER PRIMARY KEY,\
    itemid TEXT NOT NULL\
);"],

["CREATE TABLE users_and_groups_users (\
    id INTEGER PRIMARY KEY AUTOINCREMENT,\
    name TEXT NOT NULL,\
    groupid INTEGER NOT NULL,\
    language TEXT NOT NULL,\
    refresh_period INTEGER,\
    cookie_timeout INTEGER, \
    email_addr TEXT NOT NULL,\
    phone_number TEXT  NOT NULL,\
    password TEXT  NOT NULL,\
    password_reminder TEXT\
);",
"insert into users_and_groups_users(groupid, name, language, refresh_period, email_addr, phone_number, password, password_reminder, cookie_timeout)  values('1', 'superuser', 'tr', '300', '', '', 'superuser', '', '1');",
"insert into users_and_groups_users(groupid, name, language, refresh_period, email_addr, phone_number, password, password_reminder, cookie_timeout)  values('2', 'admin', 'tr', '300', '', '', 'admin', '', '1');",
"insert into users_and_groups_users(groupid, name, language, refresh_period, email_addr, phone_number, password, password_reminder, cookie_timeout)  values('3', 'user', 'tr', '300', '', '', 'user', '', '1');"],


["CREATE TABLE users_and_groups_groups (\
    id INTEGER PRIMARY KEY AUTOINCREMENT,\
    type INTEGER NOT NULL, \
    name TEXT NOT NULL,\
    email_calendar TEXT NOT NULL,\
    sms_calendar TEXT NOT NULL,\
    dial_calendar TEXT NOT NULL\
);",
"insert into users_and_groups_groups(type, name, email_calendar, sms_calendar, dial_calendar) values( '0', 'superuser', '*', '*','*');",
"insert into users_and_groups_groups(type, name, email_calendar, sms_calendar, dial_calendar) values( '1', 'admin', '*', '*','*');",
"insert into users_and_groups_groups(type, name, email_calendar, sms_calendar, dial_calendar) values( '2', 'user', '*', '*','*');"],


["CREATE TABLE users_and_groups_sensorid2groups (\
    id INTEGER PRIMARY KEY AUTOINCREMENT,\
    key TEXT NOT NULL,\
    groupid TEXT NOT NULL, \
    sensorid INTEGER NOT NULL,\
    sms_enable INTEGER NOT NULL,\
    dial_enable INTEGER NOT NULL,\
    email_enable INTEGER NOT NULL,\
    injob_alarm INTEGER NOT NULL,\
    outjob_alarm INTEGER NOT NULL\
);"],

["CREATE TABLE croptrol_rulemanager_rules (\
    id INTEGER PRIMARY KEY AUTOINCREMENT, \
    connector INTEGER NOT NULL,\
    name TEXT NOT NULL\
);"],

["CREATE TABLE croptrol_rulemanager_logical_subrules (\
    id INTEGER PRIMARY KEY AUTOINCREMENT,\
    ruleid INTEGER NOT NULL,\
    type INTEGER NOT NULL,\
    value TEXT NOT NULL,\
    inverted INTEGER NOT NULL\
);"],

["CREATE TABLE croptrol_rulemanager_physical_subrules (\
    id INTEGER PRIMARY KEY AUTOINCREMENT,\
    ruleid INTEGER NOT NULL,\
    deviceid INTEGER NOT NULL,\
    sensorid INTEGER NOT NULL,\
    status INTEGER NOT NULL,\
    inverted INTEGER NOT NULL    \
);"],

["CREATE TABLE croptrol_rulemanager_physical_outputs (\
    id INTEGER PRIMARY KEY AUTOINCREMENT,\
    ruleid INTEGER NOT NULL,\
    sensorid INTEGER NOT NULL,\
    value TEXT NOT NULL\
);"],

["CREATE TABLE croptrol_rulemanager_logical_outputs (\
    id INTEGER PRIMARY KEY AUTOINCREMENT,\
    ruleid INTEGER NOT NULL,\
    type INTEGER NOT NULL,\
    content TEXT NOT NULL,\
    groupid INTEGER NOT NULL\
);"],

["CREATE TABLE mdlmngr_config (\
    loglevel INTEGER,\
    canid INTEGER NOT NULL\
);",
"insert into mdlmngr_config(loglevel, canid) values('30', '1');"],

["CREATE TABLE logmngr_config (\
    target TEXT NOT NULL\
);",
"insert into logmngr_config(target) values('log::target::stdout');"],

["CREATE TABLE cfgmngr_config (\
    loglevel INTEGER\
);",
"insert into cfgmngr_config(loglevel) values('30');"],

["CREATE TABLE cbusmngr_config (\
    loglevel INTEGER,\
    interface TEXT NOT NULL\
);",
"insert into cbusmngr_config(loglevel, interface) values('30', 'can0');"],

["CREATE TABLE ntfymngr_config (\
    loglevel INTEGER\
);",
"insert into ntfymngr_config(loglevel) values('255');"],

["CREATE TABLE ntfymngr_sms (\
    id INTEGER PRIMARY KEY AUTOINCREMENT, \
    type INTEGER,\
    retry_count INTEGER\
);"],

["CREATE TABLE ntfymngr_sms_serial (\
    id INTEGER PRIMARY KEY, \
    device_port TEXT,\
    baudrate INTEGER,\
    parity INTEGER,\
    stopbits INTEGER,\
    databits INTEGER,\
    pin INTEGER\
);"],

["CREATE TABLE ntfymngr_sms_can (\
    id INTEGER PRIMARY KEY, \
    canid INTEGER\
);"],

["CREATE TABLE ntfymngr_sms_webservice (\
    id INTEGER PRIMARY KEY, \
    webservice_path TEXT\
);"],

["CREATE TABLE ntfymngr_email (\
    smtp_server TEXT,\
    smtp_port INTEGER,\
    username TEXT,\
    loginname TEXT,\
    password TEXT\
);"],

["CREATE TABLE network_settings(\
    mac_addr TEXT,\
    ip_addr TEXT,\
    gateway_addr TEXT,\
    subnet_mask TEXT,\
    dhcp_enabled INTEGER,\
    xml_server_port INTEGER,\
    dns_server_ip TEXT\
);",
    UpdateNetworkSettings[0]
],
          
["CREATE TABLE ftp_server (\
    ftp_server_dir_size INTEGER,\
    ftp_server_port INTEGER,\
    ftp_anonymous_login INTEGER,\
    ftp_username TEXT,\
    ftp_passwd TEXT\
);",
"insert into  ftp_server (ftp_server_dir_size,ftp_server_port ,ftp_anonymous_login ,ftp_username ,ftp_passwd) values('1024', '25','1', ' ', ' ');"],

["CREATE TABLE xml_servers (\
    id INTEGER PRIMARY KEY AUTOINCREMENT,\
    server_ip TEXT,\
    server_port INTEGER,\
    server_page TEXT ,\
    send_enabled INTEGER,\
    send_interval INTEGER,\
    detailed INTEGER\
);"],

["CREATE TABLE ntp_server(\
    server_ip TEXT,\
    server_ip_backup TEXT,\
    timezone INTEGER,\
    active INTEGER\
    );"],

["CREATE TABLE ppp_settings (\
     ppp_enabled INTEGER,\
     primary_interface TEXT,\
     ping_server_ip TEXT,\
     max_ping_retry_count INTEGER,\
     ping_period INTEGER,\
     max_backup_time INTEGER,\
     static_ip TEXT,\
     username TEXT,\
     password TEXT,\
     apn TEXT,\
     pin_code TEXT\
);"],

["CREATE TABLE syslog_servers (\
    id INTEGER PRIMARY KEY AUTOINCREMENT,\
    server_ip TEXT,\
    server_port INTEGER,\
    active INTEGER\
 );"],

["CREATE TABLE croptrol_config (\
    loglevel INTEGER,\
    hub_timer INTEGER,\
    data_record_timeout INTEGER,\
    status_record_timeout INTEGER,\
    max_row_number INTEGER,\
    row_gap INTEGER);",
    "insert into croptrol_config(loglevel,hub_timer, data_record_timeout, \
    status_record_timeout, max_row_number, row_gap) \
    values('31', '25', '300', '10', '150000', '15000'); "],

["CREATE TABLE system_info (\
    devicename TEXT NOT NULL,\
    firmware_update_ftp_ip TEXT NOT NULL,\
    firmware_update_ftp_user TEXT NOT NULL,\
    firmware_update_ftp_password TEXT NOT NULL,\
    enable_firmware_auto_update INTEGER NOT NULL,\
    firmware_version TEXT NOT NULL,\
    injob TEXT NOT NULL\
);",
"insert into system_info(devicename, firmware_update_ftp_ip, firmware_update_ftp_user, firmware_update_ftp_password, enable_firmware_auto_update, firmware_version, injob) values('Sensplorer','ftp.meg.com.tr', 'SPX', 'sensplorer910', '1','2.0.2','*');"],
          
["CREATE TABLE system_settings(\
    ftp_ip TEXT NOT NULL,\
    ftp_root TEXT NOT NULL,\
    ftp_port INTEGER NOT NULL,\
    ftp_username TEXT NOT NULL,\
    ftp_password TEXT NOT NULL\
);"],
          

["CREATE TABLE ip_cameras(\
    id INTEGER PRIMARY KEY, \
    protocol TEXT NOT NULL,\
    ip TEXT NOT NULL,\
    port INTEGER NOT NULL,\
    name TEXT NOT NULL,\
    link TEXT NOT NULL\
);"],

["CREATE TABLE snmp_server( \
    id INTEGER PRIMARY KEY AUTOINCREMENT, \
    server_ip TEXT NOT NULL,\
    server_port INTEGER NOT NULL, \
    version INTEGER NOT NULL, \
    trap_enabled INTEGER NOT NULL \
);"],

["CREATE TABLE snmp_server_v12c( \
    id INTEGER PRIMARY KEY, \
    community_name TEXT NOT NULL \
);"],

["CREATE TABLE snmp_server_v3( \
    id INTEGER PRIMARY KEY, \
    security_name TEXT NOT NULL, \
    password TEXT NOT NULL \
);"],
          
["CREATE TABLE snmp_manager(\
    version INTEGER NOT NULL,\
    port INTEGER NOT NULL,\
    community_name TEXT NOT NULL,\
    security_name TEXT NOT NULL,\
    security_level INTEGER NOT NULL,\
    auth_protocol TEXT NOT NULL,\
    auth_key TEXT NOT NULL,\
    priv_protocol TEXT NOT NULL,\
    priv_key TEXT NOT NULL\
);"],
          
["CREATE TABLE sensorexport_iec60870(\
    sensorid INTEGER NOT NULL,\
    address TEXT NOT NULL\
);"],
          
["CREATE TABLE sensorexport_snmp(\
    sensorid INTEGER NOT NULL,\
    address TEXT NOT NULL\
);"],
          
          
["CREATE TABLE sensorexport_syslog(\
    sensorid INTEGER NOT NULL\
);"],
          
["CREATE TABLE sensorexport_export_formats(\
    protocol_name TEXT NOT NULL,\
    format INTEGER default('0')\
);",
"insert into sensorexport_export_formats(protocol_name,format) values('iec61850','0');",
"insert into sensorexport_export_formats(protocol_name,format) values('snmp','0');"   
"insert into sensorexport_export_formats(protocol_name,format) values('syslog','0');"                   
    ],
          
["CREATE TABLE mdlmngr_sensors_bitstring(\
    id INTEGER PRIMARY KEY,\
    length INTEGER NOT NULL\
);"],
          

["CREATE TABLE ldap_settings (\
    ldap_server TEXT,\
    ldap_server_port INTEGER,\
    ldap_base_dn TEXT\
);"],         
          
          
          
["CREATE TABLE periodicjobs(\
    jobid INTEGER NOT NULL PRIMARY KEY,\
    mod INTEGER NOT NULL,\
    period INTEGER NOT NULL,\
    weekin INTEGER NOT NULL,\
    weekend INTEGER NOT NULL,\
    injob INTEGER NOT NULL,\
    outjob INTEGER NOT NULL,\
    enable_sms INTEGER NOT NULL,\
    enable_email INTEGER NOT NULL,\
    which_day INTEGER NOT NULL,\
    which_hour INTEGER NOT NULL);",
    "insert into periodicjobs\
    (jobid, mod, period, weekin, weekend, injob, outjob, enable_sms, enable_email, which_day, which_hour) \
    values('0','0','15','1', '1', '1', '1','0','0','0','0');",
    "insert into periodicjobs\
    (jobid, mod, period, weekin, weekend, injob, outjob, enable_sms, enable_email, which_day, which_hour) \
    values('1','0','15','1', '1', '1', '1','0','0','0','0');",
    "insert into periodicjobs\
    (jobid, mod, period, weekin, weekend, injob, outjob, enable_sms, enable_email, which_day, which_hour) \
    values('2','0','15','1', '1', '1', '1','0','0','0','0');",
    "insert into periodicjobs\
    (jobid, mod, period, weekin, weekend, injob, outjob, enable_sms, enable_email, which_day, which_hour) \
    values('3','0','30','1', '1', '1', '1','1','1','0','0');",
    "insert into periodicjobs\
    (jobid, mod, period, weekin, weekend, injob, outjob, enable_sms, enable_email, which_day, which_hour) \
    values('4','0','30','1', '1', '1', '1','1','1','0','0');",
    "insert into periodicjobs\
    (jobid, mod, period, weekin, weekend, injob, outjob, enable_sms, enable_email, which_day, which_hour) \
    values('5','0','30','1', '1', '1', '1','1','1','0','0');",
    "insert into periodicjobs\
    (jobid, mod, period, weekin, weekend, injob, outjob, enable_sms, enable_email, which_day, which_hour) \
    values('6','3','0','1', '1', '1', '1','0','0','1','12');",
    "insert into periodicjobs\
    (jobid, mod, period, weekin, weekend, injob, outjob, enable_sms, enable_email, which_day, which_hour) \
    values('7','3','0','1', '1', '1', '1','0','0','1','12');"]
]


# data = ["CREATE TABLE data_sensor_data(\
#         sensorid INTEGER,\
#         objectid TEXT,\
#         value TEXT,\
#         status INTEGER,\
#         unixtime INTEGER\
#         );",
#         "CREATE TABLE data_sensor_data_latest(\
#         sensorid INTEGER UNIQUE,\
#         objectid TEXT,\
#         value TEXT,\
#         status INTEGER,\
#         previous_status INTEGER,\
#         unixtime INTEGER\
#         );",
# 
#     "CREATE TABLE data_status_changes(\
#         sensorid INTEGER,\
#         objectid TEXT,\
#         status INTEGER,\
#         previous_status INTEGER,\
#         value TEXT,\
#         unixtime INTEGER,\
#         warning_low TEXT,\
#         warning_high TEXT,\
#         alarm_low TEXT,\
#         alarm_high TEXT);",
#     
#     "CREATE TABLE data_lookup_table(\
#         last_rowid INTEGER,\
#         unixtime INTEGER\
#         );"]
data = ["CREATE TABLE data_sensor_data(\
        value TEXT,\
        status INTEGER,\
        unixtime INTEGER\
        );",

    "CREATE TABLE data_status_changes(\
        status INTEGER,\
        previous_status INTEGER,\
        value TEXT,\
        unixtime INTEGER,\
        warning_low TEXT,\
        warning_high TEXT,\
        alarm_low TEXT,\
        alarm_high TEXT);",
    
    "CREATE TABLE data_lookup_table(\
        last_rowid INTEGER,\
        unixtime INTEGER\
        );"]

journal = ["CREATE TABLE journal(\
        id INTEGER PRIMARY KEY AUTOINCREMENT,\
        process_id INTEGER NOT NULL,\
        time INTEGER NOT NULL,\
        priority INTEGER NOT NULL,\
        event TEXT NOT NULL\
        );",

        "CREATE TABLE journal_settings(\
        max_row_number INTEGER NOT NULL,\
        row_gap INTEGER NOT NULL,\
        priority_level  INTEGER NOT NULL\
        );",
        "insert into journal_settings (max_row_number, row_gap, priority_level) values('1000', '100', '1');"
        ]

def createConfig(confPath = config_path):
#     if os.path.exists(confPath) == False or os.path.getsize(confPath) == 0:
#         os.system("rm -rf /media/config/SPXconfig.sqlite")
#         print "SPXconfig.sqlite does not exist or corrupted path:", confPath 
        db = sqlite3.connect(confPath)
        c = db.cursor()
        for i in config:
            for j in i:
                try:
                    c.execute(j)
                except Exception,e:
                    print "Exception:", str(e)
        db.commit()
        db.close()

def createData(dataPath = data_path):
#     if os.path.exists(dataPath) == False or os.path.getsize(dataPath) == 0:
#         os.system("rm -rf /media/mmc/SPXdata.sqlite")
#         print "SPXdata.sqlite does not exist or corrupted path:", dataPath
        db=sqlite3.connect(dataPath)
        c = db.cursor()
        for i in data:
            try:
                c.execute(i)
            except Exception,e:
                print "Exception:", str(e)
        db.commit()
        db.close()

def createJournal(journalPath = journal_path):
        db=sqlite3.connect(journalPath)
        c = db.cursor()
        for i in journal:
            try:
                c.execute(i)
            except Exception,e:
                print "Exception:", str(e)
        db.commit()
        db.close()

def updateAppVersion(version):
    db = sqlite3.connect(config_path)
    c = db.cursor()
    try:
        c.execute("select min(rowid) from system_info;");
        r = c.fetchall()
        if r[0][0] == None:
            raise BaseException("error")
        c.execute("update system_info set firmware_version='" + str(version) + "' where rowid=(select min(rowid) from system_info);");
        print "version updated to ", str(version)
    except BaseException as e:
        c.execute("insert into system_info values('Sensplorer', 'n/a', '" + str(version) + "', '*');");
        print "version updated too ", str(version)
    db.commit()
    db.close()
    

def checkDbStatus():
    try:
        db = sqlite3.connect(CONFIG_DB_PATH, timeout=20)
        c = db.cursor()
        c.execute("PRAGMA integrity_check")
        result = str(c.fetchone()[0])
    
        if result.find("ok") >=0:
            db.close()
            return True
        db.close()
        return False
    except Exception,e:
        exceptionStr = "Exception Happened: checkDbStatus %s" % e
        return False  

def copyConfigToBackUp():
    db = sqlite3.connect(CONFIG_DB_PATH, timeout=20)
    c = db.cursor()
    c.execute("begin immediate;")
    strToExecute = "cp " + config_path + "  " + config_backup_path 
    system(strToExecute)
    db.commit()
    db.close()

def copyConfigFromBackUp():
    strToExecute = "cp " + config_backup_path + "  " + config_path
    system(strToExecute)
    system("sync")
    
def executeDbChange():
    isDbOk = checkDbStatus()
    if isDbOk == True:
        copyConfigToBackUp()
        return 0
    elif isDbOk == False:
        copyConfigFromBackUp()
        return 1
    
