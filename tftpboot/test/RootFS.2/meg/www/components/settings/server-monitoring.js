Settings.serverMonitoringData = {
	id: 0,
	server: "10.1.1.2",
	port: "21",
	period: "30",
	trialnumber: "10"
};

Settings.serverMonitoringList = [
	{
		id: 0,
		name: "Sunucu 1"
	},
	{
		id: 1,
		name: "Sunucu 2"
	},
	{
		id: 2,
		name: "Sunucu 3"
	},
	{
		id: 3,
		name: "Sunucu 4"
	},
];

Settings.callServerMonitoring = function() {
	$("div#settings div.form").html("");
	var html = '<div class="alert"></div>'
				+ '<div class="alertNaN"></div>'
				+ '<ul class="element" id="servermonitoringform" style="display:none;">'
				+	'<li id="server"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="telnetping" style="display: none;"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="serveraddr" style="display: none;"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="serverport" style="display: none;"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="period" style="display: none;"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="trialnumber" style="display: none;"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="groups" style="display: none;"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="buttons"></li>'
				+ '</ul>'
				+ '<div id="alertDelete" style="width: 400px;" class="panelDiv serverMonitoring">'
					+ '<span class="text"></span>'
					+ '<div class="buttons">'
					+ '	<input type="submit" id="buttonRemove" name="buttonRemove" class="submit">'
					+ '	<input type="submit" id="buttonClose" name="buttonClose" class="submit">'
					+ '</div>'
				+ '</div>';
	$("div#settings div.form").html(html);
	Settings.getServerMonitoringList();
};

Settings.getServerMonitoringList = function() {
	var data = $.getJSON("http://" + hostName + "/cgi-bin/networksettings.cgi?getMonitorServers");	 
	data.success(function() {
		Settings.serverMonitoringList = jQuery.parseJSON(data.responseText);
		
		var groups = $.getJSON("http://" + hostName + "/cgi-bin/userandgroups.cgi?getGroups");

		groups.success(function() {
			Settings.groups = jQuery.parseJSON(groups.responseText);
			Settings.createServerMonitoringForm();
		});	

		groups.error(function(){
			Main.alert(language.server_error);	
			Main.unloading();
		});

		Main.unloading();
	});
	data.error(function(jqXHR, textStatus, errorThrown){
		Main.alert(language.server_error);	
		Main.unloading();
	});
};

Settings.getServerMonitoringForm = function(id) {
	var data = $.getJSON("http://" + hostName + "/cgi-bin/networksettings.cgi?getMonitorServer&id=" + id);	 
	data.success(function() {
		Settings.serverMonitoringData = jQuery.parseJSON(data.responseText);
		Settings.createGetServerMonitoringForm();
		Main.unloading();
	});
	data.error(function(jqXHR, textStatus, errorThrown){
		Main.alert(language.server_error);	
		Main.unloading();
	});
};

Settings.removeServerMonitoring = function(id) { 
	$("div#overlay").css("display","none");
	$("#content div#settings div.form div#alertDelete").css("display","none");
	Main.loading();
	var url = "http://" + hostName + "/cgi-bin/networksettings.cgi?removeMonitorServer&id=" + id;
	$.ajax({
	    url : url,
	    type: "POST",
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			Main.unloading();
			Settings.callServerMonitoring();
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
			Main.alert(language.server_error);
			Main.unloading();
	    }
	});
};

Settings.addServerMonitoring = function() {
	Main.loading();
	json = JSON.stringify(Settings.serverMonitoringData);
	var url = "http://" + hostName + "/cgi-bin/networksettings.cgi?addMonitorServer";
	$.ajax({
	    url : url,
	    type: "POST",
	    data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			Main.unloading();
			Main.alert(language.Settings_server_add_success);
			Settings.callServerMonitoring();
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
			Main.alert(language.server_error);
			Main.unloading();
	    }
	});
};

Settings.updateServerMonitoring = function(id) {
	Main.loading();
	json = JSON.stringify(Settings.serverMonitoringData);
	var url = "http://" + hostName + "/cgi-bin/networksettings.cgi?updateMonitorServer&id=" + id;
	$.ajax({
	    url : url,
	    type: "POST",
	    data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			Main.unloading();
			Main.alert(language.Settings_server_update_success);
			Settings.callServerMonitoring();
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
			Main.alert(language.server_error);
			Main.unloading();
	    }
	});
};

Settings.createServerMonitoringForm = function() {
	Main.unloading();

	if(Settings.serverMonitoringList.length == 0) {
		$("li#server").css("display","none");
		$("div#settings div.alertNaN").html(language.settings_server_not);
	} else {
		$("li#server").css("display","block");
		$("div#settings div.alertNaN").html("");
	}

	var item = Settings.serverMonitoringList;
	var id = item.id;
	var server = item.server_ip;
	var port = item.server_port;
	var groupid = item.groupid;
	var period = item.ping_period;
	var trialnumber = item.retry_count;

	var telnetPing = document.createElement("select");
	telnetPing.setAttribute("id","telnetPing");
	var option0 = document.createElement("option");
	option0.setAttribute("value","0");
	option0.innerHTML = language.Settings_server_monitoring_telnet;
	var option1 = document.createElement("option");
	option1.setAttribute("value","1");
	option1.innerHTML = language.Settings_server_monitoring_ping;

	telnetPing.appendChild(option0);
	telnetPing.appendChild(option1);

	$("li#telnetping div.left").html(language.Settings_server_monitoring_accesstype);
	$("li#telnetping div.right").append(telnetPing);

	var serverMonitoringList = document.createElement("select");
	serverMonitoringList.setAttribute("id","serverMonitoringList");
	var option0 = document.createElement("option");
	option0.setAttribute("value","none");
	option0.innerHTML = language.Settings_server_monitoring_select;

	serverMonitoringList.appendChild(option0);

	for(var i = 0; i < Settings.serverMonitoringList.length; i++) {
		var list = Settings.serverMonitoringList[i];
		var option = document.createElement("option");
		option.setAttribute("value",list.id);
		option.innerHTML = list.server_ip;
		serverMonitoringList.appendChild(option);
	}

	$("li#server div.left").html(language.Settings_server_monitoring_list);
	$("li#server div.right").append(serverMonitoringList);

	var serverInput = document.createElement("input");
	serverInput.setAttribute("type","text");
	serverInput.setAttribute("id","serverInput");
	$("li#serveraddr div.left").html(language.Settings_server_monitoring_addr);
	$("li#serveraddr div.right").append(serverInput);

	var portInput = document.createElement("input");
	portInput.setAttribute("type","text");
	portInput.setAttribute("id","portInput");
	$("li#serverport div.left").html(language.Settings_server_monitoring_port);
	$("li#serverport div.right").append(portInput);

	var periodInput = document.createElement("input");
	periodInput.setAttribute("type","text");
	periodInput.setAttribute("id","periodInput");
	$("li#period div.left").html(language.Settings_server_monitoring_period);
	$("li#period div.right").append(periodInput);

	var trialNumberInput = document.createElement("input");
	trialNumberInput.setAttribute("type","text");
	trialNumberInput.setAttribute("id","trialNumberInput");
	$("li#trialnumber div.left").html(language.Settings_server_monitoring_trialnumber);
	$("li#trialnumber div.right").append(trialNumberInput);

	var groupSelect = document.createElement("select");
	groupSelect.setAttribute("id","groups");
	
	for(var i = 0; i < Settings.groups.length; i++) {
		var option = document.createElement("option");
		option.setAttribute("value", Settings.groups[i].id);
		option.innerHTML = Settings.groups[i].name;
		groupSelect.appendChild(option);
	}

	$("li#groups div.left").html(language.Settings_server_monitoring_groups);
	$("li#groups div.right").append(groupSelect);

	var saveInput = document.createElement("input");
	saveInput.setAttribute("type","submit");
	saveInput.setAttribute("id","save");
	saveInput.style.display = "none";
	saveInput.setAttribute("value", language.Settings_xml_server_save);

	var newInput = document.createElement("input");
	newInput.setAttribute("type","submit");
	newInput.setAttribute("id","new");
	newInput.setAttribute("value", language.Settings_xml_server_new);

	var removeInput = document.createElement("input");
	removeInput.setAttribute("type","submit");
	removeInput.setAttribute("id","remove");
	removeInput.style.display = "none";
	removeInput.setAttribute("value", language.Settings_xml_server_remove);

	var cancelInput = document.createElement("input");
	cancelInput.setAttribute("type","submit");
	cancelInput.setAttribute("id","cancelServerMonitoring");
	cancelInput.style.display = "none";
	cancelInput.setAttribute("value", language.Settings_xml_server_cancel);

	$("li#buttons").append(saveInput);
	$("li#buttons").append(newInput);
	$("li#buttons").append(removeInput);
	$("li#buttons").append(cancelInput);

	$("div#settings ul.element").css("display","block");
};

Settings.createGetServerMonitoringForm = function() {
	var item = Settings.serverMonitoringData;
	var id = item.id;
	var accesstype = item.access_type;
	var server = item.server_ip;
	var port = item.server_port;
	var groupid = item.groupid;
	var period = item.ping_period;
	var trialnumber = item.retry_count;

	$("li#server").css("display","none");
	$("li#telnetping").css("display","block");
	$("li#serveraddr").css("display","block");
	$("li#serverport").css("display","block");
	$("li#period").css("display","block");
	$("li#trialnumber").css("display","block");
	$("li#groups").css("display","block");
	$("li#buttons input#new").css("display","none");
	$("li#buttons input#save").css("display","inline-block");
	$("li#buttons input#cancelServerMonitoring").css("display","inline-block");
	$("li#buttons input#remove").css("display","inline-block");
	if(accesstype == 0) {
		$("li#telnetping select option:first").prop("selected",true);
	} else {
		$("li#telnetping select option:last").prop("selected",true);
	}
	$("li#serveraddr input").val(server);
	$("li#serverport input").val(port);
	$("li#period input").val(period);
	$("li#trialnumber input").val(trialnumber);


	for(var i = 0; i < Settings.groups.length; i++) {	
		if(groupid == Settings.groups[i].id) {
			$("li#groups select option:nth-child(" + (i+1) + ")").prop("selected", true);
		}
	}
};

Settings.newServerMonitoringForm = function() {
	$("li#server").css("display","none");
	$("li#telnetping").css("display","block");
	$("li#serveraddr").css("display","block");
	$("li#serverport").css("display","block");
	$("li#period").css("display","block");
	$("li#trialnumber").css("display","block");
	$("li#groups").css("display","block");

	$("li#buttons input#new").css("display","none");
	$("li#buttons input#save").css("display","inline-block");
	$("li#buttons input#cancelServerMonitoring").css("display","inline-block");
	$("li#buttons input#remove").css("display","none");

	$("li#serveraddr input").val("");
	$("li#serverport input").val("");
	$("li#period input").val("");
	$("li#trialnumber input").val("");
	$("li#groups option:first").prop("selected", true);
};

Settings.validateServerMonitoringForm = function(page) {	
	$("div#settings div.alert").html("");
	var alertHtml = $("div#settings div.alert").html();
	$("li#serveraddr").removeClass("alert");
	$("li#serverport").removeClass("alert");
	$("li#period").removeClass("alert");
	$("li#trialnumber").removeClass("alert");

	var ip = $("li#serveraddr input").val();
	var accesstype =  $("li#telnetping select option:selected").val();
	var port = $("li#serverport input").val();
	var period = $("li#period input").val();
	var trialnumber = $("li#trialnumber input").val();
	var groupid = $("li#groups select option:selected").val();

	var iChars = "ğĞüÜşŞıİçÇöÖ";
	var validateip = /^\d{1,3}.\d{1,3}.\d{1,3}.\d{1,3}$/;
	var value = true;

	if(ip.length == 0) {
	    $("li#serveraddr").addClass("alert");
	    alertHtml = language.Settings_error_noempty;
		$("div#settings div.alert").html(alertHtml);
	    value = false;
	} else if(!ip.match(validateip)) {
	    $("li#serveraddr").addClass("alert");
	    alertHtml = language.Settings_xml_error_server;
		$("div#settings div.alert").html(alertHtml);
	    $("li#serveraddr input").focus();
	    value = false;
    }

	/*if(port.length == 0) {
	    $("li#serverport").addClass("alert");
	    alertHtml = alertHtml + "<br />" + language.Settings_error_noempty;
		$("div#settings div.alert").html(alertHtml);
	    value = false;
	} else */if(!isFinite(port)) {
		$("li#serverport").addClass("alert");
		alertHtml = alertHtml + "<br />" + language.Settings_ftp_error_port;
		$("div#settings div.alert").html(alertHtml);			
		value = false;
	}

	if(period.length == 0) {
	    $("li#period").addClass("alert");
	    alertHtml = alertHtml + "<br />" + language.Settings_error_noempty;
		$("div#settings div.alert").html(alertHtml);
	    value = false;
	} else if(!isFinite(period)) {
		$("li#period").addClass("alert");
		alertHtml = alertHtml + "<br />" + language.Settings_xml_error_integer;
		$("div#settings div.alert").html(alertHtml);			
		value = false;
	}

	if(trialnumber.length == 0) {
	    $("li#trialnumber").addClass("alert");
	    alertHtml = alertHtml + "<br />" + language.Settings_error_noempty;
		$("div#settings div.alert").html(alertHtml);
	    value = false;
	} else if(!isFinite(period)) {
		$("li#trialnumber").addClass("alert");
		alertHtml = alertHtml + "<br />" + language.Settings_xml_error_integer;
		$("div#settings div.alert").html(alertHtml);			
		value = false;
	}

	if(value) {

		Settings.serverMonitoringData = {
			access_type: accesstype,
			server_ip: ip,
			server_port: port,
			ping_period: period,
			retry_count: trialnumber,
			groupid: groupid
		};
	}

	return value;
};

Settings.clearServerMonitoringForm = function() {
	
	if(Settings.serverMonitoringList.length == 0) {
		$("li#server").css("display","none");
		$("div#settings div.alertNaN").html(language.settings_server_not);
	} else {
		$("li#server").css("display","block");
		$("div#settings div.alertNaN").html("");
	}

	$("li#server option:first").prop("selected", true);

	$("li#telnetping").css("display","none");
	$("li#serveraddr").css("display","none");
	$("li#serverport").css("display","none");
	$("li#period").css("display","none");
	$("li#trialnumber").css("display","none");
	$("li#groups").css("display","none");

	$("li#buttons input#new").css("display","inline-block");
	$("li#buttons input#save").css("display","none");
	$("li#buttons input#cancelServerMonitoring").css("display","none");
	$("li#buttons input#remove").css("display","none");

	$("li#serveraddr input").val("");
	$("li#serverport input").val("");
	$("li#xmlperiod input").val("");
	$("li#trialnumber input").val("");
	$("li#groups option:first").prop("selected", true);
};

$(function() {
	$("div#settings").on( "click", "ul#servermonitoringform input#save", function() {
		var page = $("li#buttons input#save").attr("data-page");
		if(Settings.validateServerMonitoringForm()) {
			switch(page) {
				case "new":
					Settings.addServerMonitoring();
				break;
				case "update":
					var id = $( "ul#servermonitoringform select#serverMonitoringList option:selected" ).val();
					Settings.updateServerMonitoring(id);
				break;
			};
		}
	});

	$("div#settings").on( "click", "ul#servermonitoringform input#new", function() {
		$("div#settings div.alertNaN").html("");
		Settings.newServerMonitoringForm();
		$("li#buttons input#save").attr("data-page","new");
	});

	$("div#settings").on( "click", "ul#servermonitoringform input#remove", function() {
		var id = $( "ul#servermonitoringform select#serverMonitoringList option:selected" ).val();
		$("div#overlay").css("display","block");
		$("#content div#settings div.form div#alertDelete").css("display","block");
		$("#content div#settings div.form div#alertDelete span.text").html(language.Settings_remove_server);
		$("#content div#settings div.form div#alertDelete input#buttonRemove").val(language.remove);
		$("#content div#settings div.form div#alertDelete input#buttonRemove").attr("data-id",id);
		$("#content div#settings div.form div#alertDelete input#buttonClose").val(language.cancel);
	});

	$("div#settings").on( "click", "div.serverMonitoring input#buttonRemove", function() {
		var id = $( this ).attr("data-id");
		Settings.removeServerMonitoring(id);
	});

	$("div#settings").on( "click", "div.serverMonitoring input#buttonClose", function() {
		$("div#overlay").css("display","none");
		$("#content div#settings div.form div#alertDelete").css("display","none");
	});

	$("div#settings").on( "click", "ul#servermonitoringform input#cancelServerMonitoring", function() {
		Settings.clearServerMonitoringForm();
	});

	$("div#settings").on( "change", "ul#servermonitoringform select#serverMonitoringList", function() {
		var id = $( "ul#servermonitoringform select#serverMonitoringList option:selected" ).val();
		if(id != "none") {
			$("li#buttons input#save").attr("data-page","update");
			Settings.getServerMonitoringForm(id);
		}
	});
});