Settings.systemInfoData = {
	devicename: "",
	firmware_update_ftp_ip: "",
	firmware_version: "",
	mmc_avaliable: "",
	mmc_full: "",
	mmc_usage: "",
	mmc_used: ""
};

Settings.callSystemInfo = function() {	
	$("div#settings div.form").html("");
	var html = '<div class="alert"></div>'
				+ '<ul class="element" id="systeminfo" style="display:none;">'
				+	'<li id="datastatus"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="devicename"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="ftpip"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="ftpuser"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="ftppassword"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="autoupdate"><div class="left"></div><div class="right"></div></li>'
				+	'<li id="saveftpsettings"></li>'
				+	'<li id="version"><div class="left"></div><div class="right"></div></li>'
				+    '<li id="injob"><div class="left"></div><div class="clock"></div></li>'
				+	'<li id="buttons"><div class="left"></div><div class="right"></div></li>'
				+ '</ul>'
				+ '<div id="alertDelete" style="width: 400px;" class="panelDiv systeminfo">'
					+ '<span class="text"></span>'
					+ '<div class="buttons">'
					+ '	<input type="submit" id="buttonOK" name="buttonOK" class="submit">'
					+ '	<input type="submit" id="buttonClose" name="buttonClose" class="submit">'
					+ '</div>'
				+ '</div>'
				+ '<div id="updatescreen" class="updatescreen">'
					+ '<div id="baslik"></div>'
					+ '<div id="textarea"></div>'
					+ '<div id="button">'
						+ '<input type="submit" class="submit" id="close" style="display: inline-block;">'
					+ '</div>'
				+ '</div>';
	$("div#settings div.form").html(html);

	$("div#calender select#time_selection").attr("style", "display:none;");
	$("div#calender input#saveButton").val(language.Settings_calender_save);
	$("div#calender input#cancelButton").val(language.Settings_calender_close);
	$("div#updatescreen div#button input").val(language.close);
	$("div#updatescreen div#baslik").html(language.system_update_log);

	Settings.getSystemInfo();
	CALENDAR = "systeminfo";
};

Settings.getSystemInfo = function() {
	var data = $.getJSON("http://" + hostName + "/cgi-bin/systeminfo.cgi?getSystemInfo");	 
	data.success(function() {
		if(data.responseText != "{}") {
			Settings.systemInfoData = jQuery.parseJSON(data.responseText);
		}
		
		Settings.createSystemInfoForm();
		Main.unloading();
	});
	data.error(function(jqXHR, textStatus, errorThrown){
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
		Main.unloading();
	});
};

Settings.postSystemInfo = function(url) {
	Main.loading();

	var url = "http://" + hostName + "/cgi-bin/systeminfo.cgi?" + url;
	$.ajax({
	    url : url,
	    type: "POST",
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			Main.unloading();
			//Settings.callSmtp();
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Settings.rebootAfterUpdate = function()
{
	$.ajax({
	    url : "http://" + hostName + "/cgi-bin/updateversion.cgi?reboot",
	    type: "POST",
	    dataType: 'text',
	    contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
	    	if (jsON["return"] != "true")
	    	{
	    		if (++Settings.rebootCnt != 3)
	    		{
	    			Settings.rebootAfterUpdate();
	    		}
			}
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
    		if (++Settings.rebootCnt != 3)
    		{
    			Settings.rebootAfterUpdate();
    		}
	    }
	});	
};


Settings.UpdateLogColors = ["black", "green", "orange", "red"];

var UpdatePriorities = 
{
	prNORMAL: 0,
	prSUCCESS: 1,
	prWARNING: 2,
	prERROR: 3
};

Settings.SelectUpdateLogColor = function(priority)
{
	var color;
	switch(parseInt(priority))
	{
		case UpdatePriorities.prNORMAL:
			color = Settings.UpdateLogColors[0];
			break;
		case UpdatePriorities.prSUCCESS:
			color = Settings.UpdateLogColors[1];
			break;
		case UpdatePriorities.prWARNING:
			color = Settings.UpdateLogColors[2];
			break;
		case UpdatePriorities.prERROR:
			color = Settings.UpdateLogColors[3];
			break;
		default:
			color = Settings.UpdateLogColors[0];
	}
	return color;
};

Settings.checkUpdateProcess = function()
{
	var url = "http://" + hostName + "/cgi-bin/updateversion.cgi";
	$.ajax({
	    url : url,
	    type: "GET",
        dataType: 'text',
	    success: function(data, textStatus, jqXHR)
	    {
	    	Settings.updateErrCnt = 0;
			jsON = jQuery.parseJSON(data);
			var html = "";
			var cont = true;
			var result = UpdatePriorities.prSUCCESS;
			for (var i = 0; i < jsON.length; i++)
			{
				if (jsON[i]["continue"] == "0")
				{
					cont = false;
					result = parseInt(jsON[i]["priority"]);
				}
				var color = Settings.SelectUpdateLogColor(jsON[i]["priority"]);
    			html += "<font color='" + color +"'>" + jsON[i]["event"] + "</font><br>";
				console.log("priority:"+ jsON[i]["priority"] + ", " + "continue:"+ jsON[i]["continue"] + ", event:"+ jsON[i]["event"]);
			}
	    	$("div#updatescreen div#textarea").html($("div#updatescreen div#textarea").html() + html);
	    	
			if (cont == true)
	    		setTimeout(Settings.checkUpdateProcess, 1000);
	    	else
	    	{
	    		if (result == UpdatePriorities.prSUCCESS)
	    		{
	    			Main.alert(language.system_update_success);
	    			Settings.rebootCnt = 0;
	    			Settings.rebootAfterUpdate();
	    		}
	    		else //if (result == UpdatePriorities.prERROR)
	    		{
	    			Main.alert(language.system_update_failed);
	    		}
	    	}
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    	if (++Settings.updateErrCnt == 10)
	    	{
		   		 Main.alert(language.server_error + ":" + jqXHR.responseText);
	    	}
	    	else
	    	{
	    		 setTimeout(Settings.checkUpdateProcess, 1000);
	    	}
	    }
	});
};

Settings.updateSystem = function(version) {
	Main.loading();

	$.ajax({
	    url : "http://" + hostName + "/cgi-bin/updateversion.cgi?updateFirmwareVersion&firmware_version="  + version,
	    type: "POST",
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			Main.unloading();
			jsON = jQuery.parseJSON(data);
	    	if (jsON["return"] == "true")
	    	{
	    		Settings.updateErrCnt = 0;
	    		$("div#updatescreen").css("display","block");
	    		$("div#overlay").css("display","block");
	    		setTimeout(Settings.checkUpdateProcess, 1000);
				Main.unloading();
			}
			else
			{
				Main.alert(language.system_update_failed + ":" + jsON["return"] );
			}
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	   		 Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};


Settings.updateInjobHour = function() {
	Main.loading();
	var url = "http://" + hostName + "/cgi-bin/systeminfo.cgi?updateInjobHour&injob=" + Settings.systemInfoData.injob;

	$.ajax({
	    url : url,
	    type: "POST",
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			Main.unloading();
			calenderClear();
			$("div#calender").css("display","none");
			$("div#overlay").css("display","none");
	    },
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Settings.createSystemInfoForm = function() {	
	Main.unloading();
	var item = Settings.systemInfoData;
	var devicename =  item.devicename;
	var ftp_ip = item.firmware_update_ftp_ip;
	var ftp_user = item.firmware_update_ftp_user;
	var ftp_password = item.firmware_update_ftp_password;
	var auto_update = item.enable_firmware_auto_update;
	var version = item.firmware_version;
	var mmc_avaliable = item.mmc_avaliable;
	var mmc_full = item.mmc_full;
	var mmc_usage = item.mmc_usage;
	var mmc_used = item.mmc_used;

	$("li#datastatus div.left").html(language.Settings_system_info_data_status);
	$("li#datastatus div.right").html(mmc_used + " |  " + mmc_full + ' - ' + mmc_usage);

	var dnInput = document.createElement("input");
	dnInput.setAttribute("type","text");
	dnInput.setAttribute("id","dnInput");
	dnInput.setAttribute("value",devicename);
	dnInput.style.width = "90%";
	$("li#devicename div.left").html(language.devicename);
	$("li#devicename div.right").append(dnInput);

	var ipInput = document.createElement("input");
	ipInput.setAttribute("type","text");
	ipInput.setAttribute("id","ipInput");
	ipInput.setAttribute("value",ftp_ip);
	ipInput.style.width = "90%";
	$("li#ftpip div.left").html(language.Settings_system_info_ftp_ip);
	$("li#ftpip div.right").append(ipInput);
	
	var userInput = document.createElement("input");
	userInput.setAttribute("type","text");
	userInput.setAttribute("id","userInput");
	userInput.setAttribute("value",ftp_user);
	userInput.style.width = "90%";
	$("li#ftpuser div.left").html(language.Settings_system_info_ftp_user);
	$("li#ftpuser div.right").append(userInput);
	
	var passInput = document.createElement("input");
	passInput.setAttribute("type","password");
	passInput.setAttribute("id","passInput");
	passInput.setAttribute("value",ftp_password);
	passInput.style.width = "90%";
	$("li#ftppassword div.left").html(language.Settings_system_info_ftp_password);
	$("li#ftppassword div.right").append(passInput);
	
	var autoUpdate = document.createElement("input");
	autoUpdate.setAttribute("type","checkbox");
	autoUpdate.style.width = "20px";
	autoUpdate.style.height = "20px";
	autoUpdate.style.marginTop = "5px";
	autoUpdate.style.padding = "0";
	autoUpdate.setAttribute("id","autoUpdate");
	autoUpdate.setAttribute("value",auto_update);
	autoUpdate.checked = (auto_update == "1" ? true:false);
	$("li#autoupdate div.left").html(language.Settings_system_info_enable_firmware_auto_update);
	$("li#autoupdate div.right").append(autoUpdate);

	var versionInput = document.createElement("input");
	versionInput.setAttribute("type","text");
	versionInput.setAttribute("id","versionInput");
	versionInput.setAttribute("value",version);
	$("li#version div.left").html(language.Settings_system_info_version);
	$("li#version div.right").append(versionInput);

	var saveSystemInfoInput = document.createElement("input");
	saveSystemInfoInput.setAttribute("type","submit");
	saveSystemInfoInput.setAttribute("id","saveftpsettings");
	//saveSystemInfoInput.style.width = "100px";
	//saveSystemInfoInput.style.float = "right";
	saveSystemInfoInput.setAttribute("value", language.save);

	var saveVersionInput = document.createElement("input");
	saveVersionInput.setAttribute("type","submit");
	saveVersionInput.setAttribute("id","save-version");
	saveVersionInput.setAttribute("value", language.Settings_system_info_update);

	var restartInput = document.createElement("input");
	restartInput.setAttribute("type","submit");
	restartInput.setAttribute("id","restart");
	restartInput.style.width = "250px";
	restartInput.setAttribute("value", language.Settings_system_info_restart);

	$("li#buttons").append(restartInput);
	$("li#saveftpsettings").append(saveSystemInfoInput);
	$("li#version  div.right").append(saveVersionInput);

	$("li#injob  div.left").html(language.Settings_system_info_injob);
	$("div#settings ul.element").css("display","block");
};


Settings.validateSystemInfo = function() {	
	$("div#settings div.alert").html("");
	var alertHtml = $("div#settings div.alert").html();
	$("li#devicename").removeClass("alert");
	
	$("li#ftpip").removeClass("alert");
	$("li#ftpuser").removeClass("alert");
	$("li#ftppassword").removeClass("alert");
	
	$("li#version").removeClass("alert");
	
	var devicename 	= $("li#devicename input").val();
	var ftpip 		= $("li#ftpip input").val();
	var ftpuser 	= $("li#ftpuser input").val();
	var ftppassword = $("li#ftppassword input").val();
	var autoupdate 	= ($("li#autoupdate input").prop("checked") ? "1":"0");

	var isvalid = true;

	//validation of devicename
	if(!Validator.validate( { "val" : devicename, "type" : "modulename", "ismandatory" : "true", "obj" : $("li#devicename"), "objfocus" : $("li#devicename input"), "page" : $("div#settings div.alert")}))
		isvalid = false;
	//validation of ftpip
	if(!Validator.validate( { "val" : ftpip, "type" : "domain", "ismandatory" : "false", "obj" : $("li#ftpip"), "objfocus" : $("li#ftpip input"), "page" : $("div#settings div.alert")}))
		isvalid = false;
	//validation of ftpuser
	if(!Validator.validate( { "val" : ftpuser, "type" : "ftpusername", "ismandatory" : "false", "obj" : $("li#ftpuser"), "objfocus" : $("li#ftpuser input"), "page" : $("div#settings div.alert")}))
		isvalid = false;
	//validation of ftppassword
	if(!Validator.validate( { "val" : ftppassword, "type" : "ftppassword", "ismandatory" : "false", "obj" : $("li#ftppassword"), "objfocus" : $("li#ftppassword input"), "page" : $("div#settings div.alert")}))
		isvalid = false;

	if(isvalid) {
		Settings.systemInfoData.devicename = devicename;
		Settings.systemInfoData.firmware_update_ftp_ip = ftpip;
		Settings.systemInfoData.firmware_update_ftp_user = ftpuser;
		Settings.systemInfoData.firmware_update_ftp_password = ftppassword;
		Settings.systemInfoData.enable_firmware_auto_update = autoupdate;
	}

	return isvalid;
};

Settings.validateSystemInfoVersion = function() {	
	$("div#settings div.alert").html("");
	var alertHtml = $("div#settings div.alert").html();
	$("li#ftpip").removeClass("alert");
	$("li#devicename").removeClass("alert");
	$("li#version").removeClass("alert");


	var version = $("li#version input").val();
	
	var isvalid = true;	
	
	//validation of version
	if(!Validator.validate( { "val" : version, "type" : "sensplorerversion", "ismandatory" : "true", "obj" : $("li#version"), "objfocus" : $("li#version input"), "page" : $("div#settings div.alert")}))
		isvalid = false;
	else
		Settings.systemInfoData.firmware_version = version;

	return isvalid;
};


$(function() {

	$("div#settings").on( "click", "ul#systeminfo input#saveftpsettings", function() {
		if(Settings.validateSystemInfo()) {
			var item = Settings.systemInfoData;
			var url = "updateFirmwareUpdateFtpSettings&firmware_update_ftp_ip=" + item.firmware_update_ftp_ip
						+ "&firmware_update_ftp_user=" + item.firmware_update_ftp_user
						+ "&firmware_update_ftp_password=" + item.firmware_update_ftp_password
						+ "&devicename=" + item.devicename
						+ "&enable_firmware_auto_update=" + item.enable_firmware_auto_update;
			Settings.postSystemInfo(url);
		}
	});

	$("div#settings").on( "click", "ul#systeminfo input#save-version", function() {
		if(Settings.validateSystemInfoVersion()) {
			$("div#overlay").css("display","block");
			$("#content div#settings div.form div#alertDelete").css("display","block");
			$("#content div#settings div.form div#alertDelete span.text").html(language.Settings_updateversion);
			$("#content div#settings div.form div#alertDelete input#buttonOK").val(language.update);
			$("#content div#settings div.form div#alertDelete input#buttonClose").val(language.cancel);
			$("#content div#settings div.form div#alertDelete input#buttonOK").addClass("update");
			$("#content div#settings div.form div#alertDelete input#buttonOK").removeClass("reboot");
		}
	});
	
	$("div#settings").on( "click", "div.systeminfo input#buttonOK.update", function() {
		$("#content div#settings div.form div#alertDelete").css("display","none");
		$("div#overlay").css("display","none");
		var item = Settings.systemInfoData;
		Settings.updateSystem(item.firmware_version);
	});

	$("div#settings").on( "click", "ul#systeminfo input#restart", function() {
		$("div#overlay").css("display","block");
		$("#content div#settings div.form div#alertDelete").css("display","block");
		$("#content div#settings div.form div#alertDelete span.text").html(language.Settings_reboot);
		$("#content div#settings div.form div#alertDelete input#buttonOK").val(language.reboot);
		$("#content div#settings div.form div#alertDelete input#buttonClose").val(language.cancel);
		$("#content div#settings div.form div#alertDelete input#buttonOK").addClass("reboot");
		$("#content div#settings div.form div#alertDelete input#buttonOK").removeClass("update");
	});
	
	$("div#settings").on( "click", "div.systeminfo input#buttonOK.reboot", function() {
		var id = $( this ).attr("data-id");
		$("div#overlay").css("display","none");
		$("#content div#settings div.form div#alertDelete").css("display","none");
		Settings.postSystemInfo("reboot");
	});

	$("div#settings").on( "click", "div.systeminfo input#buttonClose", function() {
		$("div#overlay").css("display","none");
		$("#content div#settings div.form div#alertDelete").css("display","none");
	});

	$("div#settings").on( "click", "div.form ul.element li#injob div.clock", function() {
	var splitData = Settings.systemInfoData.injob ;
		bFill=true;
	loadCalender(splitData);
	   	$("select#time_selection option").each(function(){
			var val = $(this).val();
			if(val == language.Settings_calender_select){
				$( this ).prop('selected', true);
			} else {
				$( this ).prop('selected', false);
			}
		});
	   	$("div#calender").css("display","block");
	   	$("div#overlay").css("display","block");
	});


	$("div#calender").on( "click", "input#cancelButton", function() {
		calenderClear();
		$("div#calender").css("display","none");
		$("div#overlay").css("display","none");
	});
	

	$("div#content").on("click", "div#updatescreen input#close", function() {	
			$("div.updatescreen div#textarea").html("");
			$("div#overlay").css("display","none");
			$("div#updatescreen").css("display","none");
		});

});