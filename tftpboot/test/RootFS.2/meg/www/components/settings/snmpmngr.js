
Settings.callSnmpManagerSettings = function()
{
	$("div#settings div.form").html("");
	var html = '<div class="alert"></div>'
				+ '<ul class="element" id="snmpmngrform">'
				+	'<li id="snmp_version">'
					+ '<div class="left"></div>'
					+ '<div class="right">'
						+ '<select>'
							+ '<option id="snmpv1">'
							+ '<option id="snmpv2c">'
							+ '<option id="snmpv3">'
						+ '</select>'
					+ '</div>'
				+ '</li>'
				+	'<li id="port">'
					+ '<div class="left"></div>'
					+ '<div class="right">'
						+ '<input type="text">'
					+ '</div>'
				+ '</li>'
				+	'<li id="community_name">'
					+ '<div class="left"></div>'
					+ '<div class="right">'
						+ '<input type="text">'
					+ '</div>'
				+ '</li>'
				+	'<li id="security_name">'
					+ '<div class="left"></div>'
					+ '<div class="right">'
						+ '<input type="text">'
					+ '</div>'
				+ '</li>'
				+	'<li id="security_level">'
					+ '<div class="left"></div>'
					+ '<div class="right">'
						+ '<select>'
							+ '<option id="noAuthNoPriv">'
							+ '<option id="authNoPriv">'
							+ '<option id="authPriv">'
						+ '</select>'
					+ '</div>'
				+ '</li>'
				+	'<li id="auth_protocol">'
					+ '<div class="left"></div>'
					+ '<div class="right">'
						+ '<select>'
							+ '<option id="md5">'
							+ '<option id="sha">'
						+ '</select>'
					+ '</div>'
				+ '</li>'
				+ '<li id="auth_key">'
					+ '<div class="left"></div>'
					+ '<div class="right">'
						+ '<input type="text">'
					+ '</div>'
				+ '</li>'
				+	'<li id="priv_protocol">'
					+ '<div class="left"></div>'
					+ '<div class="right">'
						+ '<select>'
							+ '<option id="aes">'
							+ '<option id="des">'
						+ '</select>'
					+ '</div>'
				+ '</li>'
				+ '<li id="priv_key">'
					+ '<div class="left"></div>'
					+ '<div class="right">'
						+ '<input type="text">'
					+ '</div>'
				+ '</li>'
				+ '<li id="buttons">'
					+ '	<input type="submit" id="buttonSave" name="buttonSave" class="submit" style="font-size:12px;">'
					+ '	<input type="submit" id="buttonPrtg" name="buttonPrtg" class="submit" style="font-size:12px;">'
					+ '	<input type="submit" id="buttonRefFile" name="buttonRefFile" class="submit" style="font-size:12px;">'
					+ '	<input type="submit" id="buttonMibFile" name="buttonMibFile" class="submit" style="font-size:12px;">'
				+ '</li>'
				+ '</ul>';
	$("div#settings div.form").html(html);

	$("div#settings div.form ul#snmpmngrform li#snmp_version div.left").html(language.Settings_server_snmp_version);
	$("div#settings div.form ul#snmpmngrform li#snmp_version div.right select option#snmpv1").html(language.snmpv1);
	$("div#settings div.form ul#snmpmngrform li#snmp_version div.right select option#snmpv2c").html(language.snmpv2c);
	$("div#settings div.form ul#snmpmngrform li#snmp_version div.right select option#snmpv3").html(language.snmpv3);
	$("div#settings div.form ul#snmpmngrform li#port div.left").html(language.Settings_server_snmp_port);
	$("div#settings div.form ul#snmpmngrform li#community_name div.left").html(language.Settings_server_snmp_community_name);
	$("div#settings div.form ul#snmpmngrform li#security_name div.left").html(language.Settings_server_snmp_security_name);
	$("div#settings div.form ul#snmpmngrform li#security_level div.left").html(language.Settings_server_snmp_security_level);
	$("div#settings div.form ul#snmpmngrform li#security_level div.right select option#noAuthNoPriv").html(language.noauthnopriv);
	$("div#settings div.form ul#snmpmngrform li#security_level div.right select option#authNoPriv").html(language.authnopriv);
	$("div#settings div.form ul#snmpmngrform li#security_level div.right select option#authPriv").html(language.authpriv);
	$("div#settings div.form ul#snmpmngrform li#buttons input#buttonSave").val(language.save);
	$("div#settings div.form ul#snmpmngrform li#buttons input#buttonPrtg").val(language.generateprtg);
	$("div#settings div.form ul#snmpmngrform li#buttons input#buttonRefFile").val(language.generatereffile);
	$("div#settings div.form ul#snmpmngrform li#buttons input#buttonMibFile").val(language.generatemibfile);

	$("div#settings div.form ul#snmpmngrform li#auth_protocol div.right select option#md5").html(language.md5);
	$("div#settings div.form ul#snmpmngrform li#auth_protocol div.right select option#sha").html(language.sha);
	$("div#settings div.form ul#snmpmngrform li#auth_protocol div.left").html(language.auth_protocol);
	$("div#settings div.form ul#snmpmngrform li#auth_key div.left").html(language.auth_key);

	$("div#settings div.form ul#snmpmngrform li#priv_protocol div.right select option#aes").html(language.aes);
	$("div#settings div.form ul#snmpmngrform li#priv_protocol div.right select option#des").html(language.des);
	$("div#settings div.form ul#snmpmngrform li#priv_protocol div.left").html(language.priv_protocol);
	$("div#settings div.form ul#snmpmngrform li#priv_key div.left").html(language.priv_key);

	Settings.getSnmpManagerSettings();
};

Settings.getSnmpManagerSettings = function()
{
	Util.httpReq_R({
		url: "networksettings.cgi?getSnmpManagerSettings",
		type: "GET",
		contentType:"text/json; charset=utf-8",
		callback: "Settings.createSnmpManagerForm"
	});
};

Settings.createSnmpManagerForm = function(data)
{
	Settings.SnmpManager = data;
	$("div#settings div.form ul#snmpmngrform li#snmp_version select").prop("selectedIndex", Settings.SnmpManager.version != undefined ? parseInt(Settings.SnmpManager.version):0);
	$("div#settings div.form ul#snmpmngrform li#port input").val(Settings.SnmpManager.port != undefined ? Settings.SnmpManager.port: "");
	$("div#settings div.form ul#snmpmngrform li#community_name input").val(Settings.SnmpManager.community_name);
	if (Settings.SnmpManager.version == "2")
	{
		$("div#settings div.form ul#snmpmngrform li#security_name input").val(Settings.SnmpManager.security_name != undefined ? Settings.SnmpManager.security_name:"");
		$("div#settings div.form ul#snmpmngrform li#security_level select").prop("selectedIndex", Settings.SnmpManager.security_level != undefined ? parseInt(Settings.SnmpManager.security_level):0);
		if (Settings.SnmpManager.security_level == "1")
		{
			$("div#settings div.form ul#snmpmngrform li#auth_protocol select").prop("selectedIndex", Settings.SnmpManager.auth_protocol != undefined ? parseInt(Settings.SnmpManager.auth_protocol):0);
			$("div#settings div.form ul#snmpmngrform li#auth_key input").val(Settings.SnmpManager.auth_key != undefined ? Settings.SnmpManager.auth_key:"");
		}
		else if (Settings.SnmpManager.security_level == "2")
		{
			$("div#settings div.form ul#snmpmngrform li#auth_protocol select").prop("selectedIndex", Settings.SnmpManager.auth_protocol != undefined ? parseInt(Settings.SnmpManager.auth_protocol):0);
			$("div#settings div.form ul#snmpmngrform li#auth_key input").val(Settings.SnmpManager.auth_key != undefined ? Settings.SnmpManager.auth_key:"");
			$("div#settings div.form ul#snmpmngrform li#priv_protocol select").prop("selectedIndex", Settings.SnmpManager.priv_protocol != undefined ? parseInt(Settings.SnmpManager.priv_protocol):0);
			$("div#settings div.form ul#snmpmngrform li#priv_key input").val(Settings.SnmpManager.priv_key != undefined ? Settings.SnmpManager.priv_key:"");
		}
	}
	Settings.activateSnmpManagerFields();	
};

Settings.activateSnmpManagerFields = function()
{
	var version = $("div#settings div.form ul#snmpmngrform li#snmp_version select").prop("selectedIndex");
	var security_level = $("div#settings div.form ul#snmpmngrform li#security_level select").prop("selectedIndex");

	$("div#settings div.form ul#snmpmngrform li#snmp_version").css("display", "inline-block");
	$("div#settings div.form ul#snmpmngrform li#port").css("display", "inline-block");
	$("div#settings div.form ul#snmpmngrform li#community_name").css("display", "none");
	$("div#settings div.form ul#snmpmngrform li#security_name").css("display", "none");
	$("div#settings div.form ul#snmpmngrform li#security_level").css("display", "none");
	$("div#settings div.form ul#snmpmngrform li#auth_protocol").css("display", "none");
	$("div#settings div.form ul#snmpmngrform li#auth_key").css("display", "none");
	$("div#settings div.form ul#snmpmngrform li#priv_protocol").css("display", "none");
	$("div#settings div.form ul#snmpmngrform li#priv_key").css("display", "none");

	if (version != "2")
		$("div#settings div.form ul#snmpmngrform li#community_name").css("display", "inline-block");

	if (version == "2")
	{	
		$("div#settings div.form ul#snmpmngrform li#security_name").css("display", "inline-block");
		$("div#settings div.form ul#snmpmngrform li#security_level").css("display", "inline-block");
		
		$("div#settings div.form ul#snmpmngrform li#auth_protocol").css("display", "inline-block");
		$("div#settings div.form ul#snmpmngrform li#auth_key").css("display", "inline-block");

		if (security_level == "2")
		{
			$("div#settings div.form ul#snmpmngrform li#priv_protocol").css("display", "inline-block");
			$("div#settings div.form ul#snmpmngrform li#priv_key").css("display", "inline-block");
		}
	}
};

Settings.validateSnmpManagerForm = function(json)
{

	$("div#settings div.alert").html("");
	$("div#settings div.form ul#snmpserverform li").removeClass("alert");
	$("div#settings div.form ul#snmpserverform").removeClass("alert");

	var version 		= $("div#settings div.form ul#snmpmngrform li#snmp_version select").prop("selectedIndex").toString();
	var port 			= $("div#settings div.form ul#snmpmngrform li#port input").val();
	var community_name 	= $("div#settings div.form ul#snmpmngrform li#community_name input").val();
	var security_name 	= $("div#settings div.form ul#snmpmngrform li#security_name input").val();
	var security_level 	= $("div#settings div.form ul#snmpmngrform li#security_level select").prop("selectedIndex").toString();
	var auth_protocol 	= $("div#settings div.form ul#snmpmngrform li#auth_protocol option:selected").val();
	var auth_key 		= $("div#settings div.form ul#snmpmngrform li#auth_key input").val();
	var priv_protocol 	= $("div#settings div.form ul#snmpmngrform li#priv_protocol option:selected").val();
	var priv_key 		= $("div#settings div.form ul#snmpmngrform li#priv_key input").val();

	var isvalid = true;

	json.version = version;
	
	//validation of port
	if(!Validator.validate( { "val" : port, "type" : "port", "ismandatory" : "true", "obj" : $("li#port"), "objfocus" : $("li#port input"), "page" : $("div#settings div.alert")}))
		isvalid = false;
	else
		json.port = port;

	if (version == "0" || version == "1")
	{
		//validation of community_name
		if(!Validator.validate( { "val" : community_name, "type" : "snmpcommunity", "ismandatory" : "true", "obj" : $("li#community_name"), "objfocus" : $("li#community_name input"), "page" : $("div#settings div.alert")}))
			isvalid = false;
		else
			json.community_name = community_name;
		
	}
	else if (version == "2")
	{	
		//validation of security_name
		if(!Validator.validate( { "val" : security_name, "type" : "snmpsecurity", "ismandatory" : "true", "obj" : $("li#security_name"), "objfocus" : $("li#security_name input"), "page" : $("div#settings div.alert")}))
			isvalid = false;
		else
			json.security_name = security_name;			
		json.security_level = security_level;
		
		if (security_level == "2")
		{	
			//validation of priv_key
			if(!Validator.validate( { "val" : priv_key, "type" : "snmpprivkey", "ismandatory" : "true", "obj" : $("li#priv_key"), "objfocus" : $("li#priv_key input"), "page" : $("div#settings div.alert")}))
				isvalid = false;
			else
				json.priv_key = priv_key;	
			json.priv_protocol = priv_protocol;
			
		}
		
		//validation of auth_key
		if(!Validator.validate( { "val" : auth_key, "type" : "snmpauthkey", "ismandatory" : "true", "obj" : $("li#auth_key"), "objfocus" : $("li#auth_key input"), "page" : $("div#settings div.alert")}))
			isvalid = false;
		else
			json.auth_key = auth_key;			
		json.auth_protocol = auth_protocol;		
		
	}
	
	return isvalid;
};

Settings.updateSnmpManagerSettings = function(info)
{

	Util.httpReq_S({
		url: "networksettings.cgi?updateSnmpManagerSettings",
		type: "POST",
		data: JSON.stringify(info),
		contentType: 'text/json; charset=utf-8',
		success: language.Settings_server_update_success
	});
	/*
		Main.loading();
		json = JSON.stringify(info);
		var url = "http://" + hostName + "/cgi-bin/networksettings.cgi?updateSnmpManagerSettings";
		$.ajax({
		    url : url,
		    type: "POST",
		    data : json,
	        dataType: 'text',
	        contentType : 'text/json; charset=utf-8',
		    success: function(Data, textStatus, jqXHR)
		    {
		    	Main.unloading();
		    	Json = jQuery.parseJSON(Data
		    		);
		    	if (Json["return"] == "true")
					Main.alert(language.Settings_server_update_success);
				else
					Main.alert(language.Settings_server_update_failed + ": " + Json["return"]);

			},
		    error: function (jqXHR, textStatus, errorThrown)
		    {
				Main.alert(language.server_error);
				Main.unloading();
		    }
		});
	*/
};

Settings.downloadPRTGFile = function()
{
	Main.loading();
	var url = "http://" + hostName + "/cgi-bin/megweb.cgi?generatePRTGFile";
	$.ajax({
	    url : url,
	    type: "GET",
        dataType: 'text',
	    success: function(Data, textStatus, jqXHR)
	    {
	    	xmlDoc = $.parseXML( Data );
  			$xml = $( xmlDoc );
  			$name = $xml.find( "name" );
  			var strName = $name[0].innerHTML;
  			var devName = strName.split("|");

	    	Main.unloading();	
			var blob = new Blob([Data], {type: "text/xml;charset=utf-8"});
			saveAs(blob,devName[0] +"_PRTGFile.oidlib");
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
			Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
	    }
	});
};

Settings.downloadRefFile = function()
{
	var url = "http://" + hostName + "/cgi-bin/megweb.cgi?generateRefFile";
	$.ajax({
		method:"GET",
		dataType:"json",
		url:url,
		async:false,
		success: function(data){
			var fileUrl = "http://" + hostName + "/SNMP_" + data.mac_addr + ".json";
			console.log(fileUrl);
			window.location = fileUrl; 
		}
	});
};

Settings.downloadOIDFilePDF = function()
{
    var sensorsData = $.getJSON("http://" + hostName + "/cgi-bin/megweb.cgi?generateOIDFile");

    sensorsData.success(function() {
	var sensorsObject = JSON.parse(sensorsData.responseText);


	for(var i=0; i < sensorsObject.length; ++i)
    {
    	sensorsObject[i].id = parseInt(sensorsObject[i].id);
    }

    var sensorsObjectbyId = sensorsObject.slice(0);
		sensorsObjectbyId.sort(function(a,b) {
    	return a.id - b.id;
	});

	var columns0 = ['Sensor Name', 'Sensor Number', 'Sensor Name', 'Sensor Number'];
	var rows0 = [];

    for(var i=0; i < sensorsObjectbyId.length; ++i)
    {
    	var row = [];
		row.push(sensorsObjectbyId[i].name);
		row.push(sensorsObjectbyId[i].id);
		++i;
		if(i < sensorsObjectbyId.length)
		{
			row.push(sensorsObjectbyId[i].name);
			row.push(sensorsObjectbyId[i].id);
		}
		rows0.push(row);
    }


	var columns1 = ['Id','Property', 'Id', 'Property'];
    var rows1 = [
    [1, 'Sensor Id', 					23,'Sensor Warning Low Limit'],
    [2, 'Sensor Object Id', 			24,'Sensor Warning High Limit'],
    [3, 'Sensor Value', 				25,'Sensor Alarm Low Limit'],
    [4, 'Sensor Status', 				26,'Sensor Alarm High Limit'],
    [5, 'Sensor Ionum', 				27,'Sensor Filter Type'],
    [6, 'Sensor Type', 					28,'Sensor Filter Value'],
    [7, 'Sensor Read-Write',			29,'Sensor Multiplier'],
    [8, 'Sensor Name', 					30,'Sensor Item Id'],
    [9, 'Sensor Alarm Active', 			31,'Sensor Obis Code'],
    [10,'Sensor Record Period',			32,'Sensor Register Address'],
    [11,'Sensor Read Period',			33,'Sensor Read Function Code'],
    [12,'Sensor Read Enable',			34,'Sensor Write Function Code'],
    [13,'Sensor Record',				35,'Sensor Register Count'],
    [14,'Sensor Enable',				36,'Sensor Is Signed'],
    [15,'Sensor Physical Type',			37,'Sensor Power On'],
    [16,'Sensor Alarm Value',			38,'Sensor Apply'],
    [17,'Sensor Zero Name',				39,'Sensor Pattern'],
    [18,'Sensor One Name',				40,'Sensor Replace'],
    [19,'Sensor Time Limit',			41,'Sensor Oid'],
    [20,'Sensor Hysteresis Time Limit',	42,'Sensor Ip'],
    [21,'Sensor Length',				43,'Sensor Port'],
    [22,'Sensor Unit']
    ];

    var doc = new jsPDF('p', 'pt');

    doc.setFontSize(15);
	doc.text(20, 30, "Basic OID: .1.3.6.1.4.1.26518.1.1.1.2.1.");

	doc.setFontSize(20);
	doc.setFontType("bold");
    doc.setTextColor(255, 0, 0);
	doc.text(284, 31, "X");

	doc.setFontType("normal");
	doc.setFontSize(15);
    doc.setTextColor(0, 0, 0);
	doc.text(298, 30, ".");

	doc.setFontSize(20);
	doc.setFontType("bold");
    doc.setTextColor(0, 0, 255);
	doc.text(299, 31, "Y");


	doc.setFontType("normal");
    doc.setTextColor(0, 0, 0);
	doc.setFontSize(12);
	doc.text(20, 50, 'You can complete Basic OID from following tables.')

    doc.setFontSize(20);
	doc.setFontType("bold");
    doc.setTextColor(255, 0, 0);
    doc.text(20, 80, 'Properties Table [X]'); 

    doc.autoTable(columns1, rows1, {
        headerStyles: {fillColor: [255, 0, 0]},
        margin: {left: 20, right: 20},
        startY: 90,
        drawHeaderCell: function (cell, data) {
		    cell.styles.textColor = 255;
		    cell.styles.fontSize = 10;
        }
    });

	doc.setFontSize(20);
	doc.setFontType("bold");
    doc.setTextColor(0, 0, 255);
    doc.text(20, doc.autoTableEndPosY() + 30, 'Sensors Table [Y]'); 

    doc.autoTable(columns0, rows0, {
        headerStyles: {fillColor: [0, 0, 255]},
        margin: {left: 20, right: 20},
        startY: doc.autoTableEndPosY() + 40,
        drawHeaderCell: function (cell, data) {
		    cell.styles.textColor = 255;
		    cell.styles.fontSize = 10;
        }
    });

    doc.save('SNMP OID Definition.pdf');

	});

	sensorsData.error(function(jqXHR, textStatus, errorThrown){
		Main.unloading();
		Main.alert("Sensors data alinamadi:" + jqXHR.responseText);
	});
};


$(
	function()
	{
		$("div#settings").on("click", "div.form ul#snmpmngrform li#buttons input#buttonSave", function()
		{
			var json = {};
			if (Settings.validateSnmpManagerForm(json))
			{
				Settings.updateSnmpManagerSettings(json);
			}
		});

		$("div#settings").on("click", "div.form ul#snmpmngrform li#buttons input#buttonPrtg", function()
		{
			var json = {};
			Settings.downloadPRTGFile();
		});

		$("div#settings").on("click", "div.form ul#snmpmngrform li#buttons input#buttonRefFile", function()
		{
			var json = {};
			Settings.downloadRefFile();
		});

		$("div#settings").on("click", "div.form ul#snmpmngrform li#buttons input#buttonMibFile", function()
		{
			var json = {};
			Settings.downloadOIDFilePDF();
		});

		$("div#settings").on("change", "div.form ul#snmpmngrform li#snmp_version select", function()
		{
			Settings.activateSnmpManagerFields();
		});

		$("div#settings").on("change", "div.form ul#snmpmngrform li#security_level select", function()
		{
			Settings.activateSnmpManagerFields();
		});
	}
);