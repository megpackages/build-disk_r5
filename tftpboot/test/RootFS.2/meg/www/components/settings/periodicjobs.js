
           
//check_gsm_modem_alive     period(min)  enable_email injob outjob         
//check_mail_server_alive   period(min)  enable_sms   injob outjob 
//check_network_connection  period(min)  enable_email injob outjob  
//resend_alarms_and_losts   period(hour)    
//periodic_system_wide_status_info  mod(week or day)      which_day  which_hour enable_email enable_sms
//periodic_sensor_based_status_info mod(month, week, day) which_day  which_hour enable_email

/*
 const JobMod MIN	= JobMod(0);
 const JobMod HOUR	= JobMod(1);
 const JobMod DAY	= JobMod(2);
 const JobMod WEEK	= JobMod(3);
 const JobMod MONTH = JobMod(4);
 */


Settings.callPeriodicJobs = function()
{
	$("div#settings div.form").html("");
	var html = '<div class="alert"></div>'
				+'<div id="periodicjobs"></div>';
	$("div#settings div.form").html(html);
	Settings.getPeriodicJobs();
};

Settings.getPeriodicJobs = function()
{
	var data = $.getJSON("http://" + hostName + "/cgi-bin/periodicjobs.cgi?getPeriodicJobs");	 
	data.success(function() {
		Settings.periodicjobs = jQuery.parseJSON(data.responseText);
		Settings.createPeriodicJobsForm();
		Main.unloading();
	});
	data.error(function(jqXHR, textStatus, errorThrown){
	    Main.alert(language.server_error + ":" + jqXHR.responseText);
		Main.unloading();
	});
};

Settings.periodicJobsArrangeColspan = function()
{
	var maxtdnum = 0;
	var lst = $("div#settings div.form div#periodicjobs table tr:not(.Header)");
	for (var i = 0; i < lst.length; i++)
	{
		var lsttdlen = $(lst[i]).children("td:visible").length ;
		if (lsttdlen > maxtdnum)
			maxtdnum = lsttdlen;
	}

	for (var i = 0; i < lst.length; i++)
	{
		var lsttdlen = $(lst[i]).children("td:visible").length ;
		$(lst[i]).children("td.buttons").attr("colspan", (maxtdnum - lsttdlen + 1));
	}
	$("div#settings div.form div#periodicjobs table tr.Header td").attr("colspan", maxtdnum);
};

Settings.createPeriodicJobsForm = function()
{
	var table = document.createElement("table");
	table.setAttribute("id", "periodicjobs");
	for (var i = 0; i < Settings.periodicjobs.length; i++)
	{
		var jobid = parseInt(Settings.periodicjobs[i].jobid);
		switch(jobid)
		{
			case 0:
				var trH = document.createElement("tr");
				trH.setAttribute("class", "Header");
				trH.setAttribute("id", jobid.toString());
				var tdH = document.createElement("td");
				var aH = document.createElement("a");
				aH.innerHTML = eval("language.periodicjobs" + jobid.toString());
				tdH.appendChild(aH);
				trH.appendChild(tdH); 
				
				var trS = document.createElement("tr");
				trS.setAttribute("class", "job");
				trS.setAttribute("id", jobid.toString());
				
				var tdPeriod = document.createElement("td"); 
				var aPeriod = document.createElement("a");
				aPeriod.innerHTML = language.periodicjobs_period_min + ": ";
				tdPeriod.appendChild(aPeriod);
				var inPeriod = document.createElement("input");
				inPeriod.setAttribute("type", "text");
				inPeriod.setAttribute("id", "period");
				inPeriod.setAttribute("data-type", "i");
				inPeriod.setAttribute("minval", "1");
				inPeriod.setAttribute("maxval", "60");
				inPeriod.setAttribute("value", Settings.periodicjobs[i].period);
				tdPeriod.appendChild(inPeriod);
				trS.appendChild(tdPeriod);
				
				var tdEnEmail = document.createElement("td"); 
				var aEmail = document.createElement("a");
				aEmail.innerHTML = language.periodicjobs_enable_email + ": ";
				tdEnEmail.appendChild(aEmail);
				var inEmail = document.createElement("input");
				inEmail.setAttribute("type", "checkbox");
				inEmail.setAttribute("id", "enable_email");
				inEmail.checked = Settings.periodicjobs[i].enable_email == "1";
				tdEnEmail.appendChild(inEmail);
				trS.appendChild(tdEnEmail);
				
				var tdInjob = document.createElement("td"); 
				var aInjob = document.createElement("a");
				aInjob.innerHTML = language.periodicjobs_injob + ": ";
				tdInjob.appendChild(aInjob);
				var inInjob = document.createElement("input");
				inInjob.setAttribute("type", "checkbox");
				inInjob.setAttribute("id", "injob");
				inInjob.checked = Settings.periodicjobs[i].injob == "1";
				tdInjob.appendChild(inInjob);
				trS.appendChild(tdInjob);
				
				var tdOutjob = document.createElement("td"); 
				var aOutjob = document.createElement("a");
				aOutjob.innerHTML = language.periodicjobs_outjob + ": ";
				tdOutjob.appendChild(aOutjob);
				var inOutjob = document.createElement("input");
				inOutjob.setAttribute("type", "checkbox");
				inOutjob.setAttribute("id", "outjob");
				inOutjob.checked = Settings.periodicjobs[i].outjob == "1";
				tdOutjob.appendChild(inOutjob);
				trS.appendChild(tdOutjob);
				
				var tdButton = document.createElement("td");
				tdButton.setAttribute("class", "buttons");
				var button = document.createElement("input");
				button.setAttribute("type", "submit");
				button.setAttribute("id", jobid.toString());
				button.setAttribute("value", language.save);
				tdButton.appendChild(button);
				trS.appendChild(tdButton);
				
				table.appendChild(trH);
				table.appendChild(trS);
			break;
			case 1:
				var trH = document.createElement("tr");
				trH.setAttribute("class", "Header");
				trH.setAttribute("id", jobid.toString());
				var tdH = document.createElement("td");
				var aH = document.createElement("a");
				aH.innerHTML = eval("language.periodicjobs" + jobid.toString());
				tdH.appendChild(aH);
				trH.appendChild(tdH); 
				
				var trS = document.createElement("tr");
				trS.setAttribute("class", "job");
				trS.setAttribute("id", jobid.toString());
				
				var tdPeriod = document.createElement("td"); 
				var aPeriod = document.createElement("a");
				aPeriod.innerHTML = language.periodicjobs_period_min + ": ";
				tdPeriod.appendChild(aPeriod);
				var inPeriod = document.createElement("input");
				inPeriod.setAttribute("type", "text");
				inPeriod.setAttribute("id", "period");
				inPeriod.setAttribute("data-type", "i");
				inPeriod.setAttribute("minval", "1");
				inPeriod.setAttribute("maxval", "60");
				inPeriod.setAttribute("value", Settings.periodicjobs[i].period);
				tdPeriod.appendChild(inPeriod);
				trS.appendChild(tdPeriod);
				
				var tdEnSms = document.createElement("td"); 
				var aSms = document.createElement("a");
				aSms.innerHTML = language.periodicjobs_enable_sms + ": ";
				tdEnSms.appendChild(aSms);
				var inSms = document.createElement("input");
				inSms.setAttribute("type", "checkbox");
				inSms.setAttribute("id", "enable_sms");
				inSms.checked = Settings.periodicjobs[i].enable_sms == "1";
				tdEnSms.appendChild(inSms);
				trS.appendChild(tdEnSms);
				
				var tdInjob = document.createElement("td"); 
				var aInjob = document.createElement("a");
				aInjob.innerHTML = language.periodicjobs_injob + ": ";
				tdInjob.appendChild(aInjob);
				var inInjob = document.createElement("input");
				inInjob.setAttribute("type", "checkbox");
				inInjob.setAttribute("id", "injob");
				inInjob.checked = Settings.periodicjobs[i].injob == "1";
				tdInjob.appendChild(inInjob);
				trS.appendChild(tdInjob);
				
				var tdOutjob = document.createElement("td"); 
				var aOutjob = document.createElement("a");
				aOutjob.innerHTML = language.periodicjobs_outjob + ": ";
				tdOutjob.appendChild(aOutjob);
				var inOutjob = document.createElement("input");
				inOutjob.setAttribute("type", "checkbox");
				inOutjob.setAttribute("id", "outjob");
				inOutjob.checked = Settings.periodicjobs[i].outjob == "1";
				tdOutjob.appendChild(inOutjob);
				trS.appendChild(tdOutjob);
				
				var tdButton = document.createElement("td");
				tdButton.setAttribute("class", "buttons");
				var button = document.createElement("input");
				button.setAttribute("type", "submit");
				button.setAttribute("id", jobid.toString());
				button.setAttribute("value", language.save);
				tdButton.appendChild(button);
				trS.appendChild(tdButton);
				
				table.appendChild(trH);
				table.appendChild(trS);
			break;
			case 2:
				var trH = document.createElement("tr");
				trH.setAttribute("class", "Header");
				trH.setAttribute("id", jobid.toString());
				var tdH = document.createElement("td");
				var aH = document.createElement("a");
				aH.innerHTML = eval("language.periodicjobs" + jobid.toString());
				tdH.appendChild(aH);
				trH.appendChild(tdH); 
				
				var trS = document.createElement("tr");
				trS.setAttribute("class", "job");
				trS.setAttribute("id", jobid.toString());
				
				var tdPeriod = document.createElement("td"); 
				var aPeriod = document.createElement("a");
				aPeriod.innerHTML = language.periodicjobs_period_min + ": ";
				tdPeriod.appendChild(aPeriod);
				var inPeriod = document.createElement("input");
				inPeriod.setAttribute("type", "text");
				inPeriod.setAttribute("id", "period");
				inPeriod.setAttribute("data-type", "i");
				inPeriod.setAttribute("minval", "1");
				inPeriod.setAttribute("maxval", "60");
				inPeriod.setAttribute("value", Settings.periodicjobs[i].period);
				tdPeriod.appendChild(inPeriod);
				trS.appendChild(tdPeriod);
				
				var tdEnSms = document.createElement("td"); 
				var aSms = document.createElement("a");
				aSms.innerHTML = language.periodicjobs_enable_sms + ": ";
				tdEnSms.appendChild(aSms);
				var inSms = document.createElement("input");
				inSms.setAttribute("type", "checkbox");
				inSms.setAttribute("id", "enable_sms");
				inSms.checked = Settings.periodicjobs[i].enable_sms == "1";
				tdEnSms.appendChild(inSms);
				trS.appendChild(tdEnSms);
				
				var tdInjob = document.createElement("td"); 
				var aInjob = document.createElement("a");
				aInjob.innerHTML = language.periodicjobs_injob + ": ";
				tdInjob.appendChild(aInjob);
				var inInjob = document.createElement("input");
				inInjob.setAttribute("type", "checkbox");
				inInjob.setAttribute("id", "injob");
				inInjob.checked = Settings.periodicjobs[i].injob == "1";
				tdInjob.appendChild(inInjob);
				trS.appendChild(tdInjob);
				
				var tdOutjob = document.createElement("td"); 
				var aOutjob = document.createElement("a");
				aOutjob.innerHTML = language.periodicjobs_outjob + ": ";
				tdOutjob.appendChild(aOutjob);
				var inOutjob = document.createElement("input");
				inOutjob.setAttribute("type", "checkbox");
				inOutjob.setAttribute("id", "outjob");
				inOutjob.checked = Settings.periodicjobs[i].outjob == "1";
				tdOutjob.appendChild(inOutjob);
				trS.appendChild(tdOutjob);
				
				var tdButton = document.createElement("td");
				tdButton.setAttribute("class", "buttons");
				var button = document.createElement("input");
				button.setAttribute("type", "submit");
				button.setAttribute("id", jobid.toString());
				button.setAttribute("value", language.save);
				tdButton.appendChild(button);
				trS.appendChild(tdButton);
				
				table.appendChild(trH);
				table.appendChild(trS);
			break;
			case 3:
			case 4:
			case 5:
				var trH = document.createElement("tr");
				trH.setAttribute("class", "Header");
				trH.setAttribute("id", jobid.toString());
				var tdH = document.createElement("td");
				var aH = document.createElement("a");
				aH.innerHTML = eval("language.periodicjobs" + jobid.toString());
				tdH.appendChild(aH);
				trH.appendChild(tdH); 
				
				var trS = document.createElement("tr");
				trS.setAttribute("class", "job");
				trS.setAttribute("id", jobid.toString());
				
				var tdPeriod = document.createElement("td"); 
				var aPeriod = document.createElement("a");
				aPeriod.innerHTML = language.periodicjobs_period_min + ": ";
				tdPeriod.appendChild(aPeriod);
				var inPeriod = document.createElement("input");
				inPeriod.setAttribute("type", "text");
				inPeriod.setAttribute("id", "period");
				inPeriod.setAttribute("data-type", "i");
				inPeriod.setAttribute("minval", "10");
				inPeriod.setAttribute("maxval", "1440");
				inPeriod.setAttribute("value", Settings.periodicjobs[i].period);
				tdPeriod.appendChild(inPeriod);
				trS.appendChild(tdPeriod);
				
				var tdEnable = document.createElement("td"); 
				var aEnable = document.createElement("a");
				aEnable.innerHTML = language.periodicjobs_enable + ": ";
				tdEnable.appendChild(aEnable);
				var inEnable = document.createElement("input");
				inEnable.setAttribute("type", "checkbox");
				inEnable.setAttribute("id", "enable");
				inEnable.checked = Settings.periodicjobs[i].enable_email == "1" || Settings.periodicjobs[i].enable_sms == "1";
				tdEnable.appendChild(inEnable);
				trS.appendChild(tdEnable);
				
				var tdButton = document.createElement("td");
				tdButton.setAttribute("class", "buttons");
				var button = document.createElement("input");
				button.setAttribute("type", "submit");
				button.setAttribute("id", jobid.toString());
				button.setAttribute("value", language.save);
				tdButton.appendChild(button);
				trS.appendChild(tdButton);
				
				table.appendChild(trH);
				table.appendChild(trS);
			break;
			case 6:
				var trH = document.createElement("tr");
				trH.setAttribute("class", "Header");
				trH.setAttribute("id", jobid.toString());
				var tdH = document.createElement("td");
				var aH = document.createElement("a");
				aH.innerHTML = eval("language.periodicjobs" + jobid.toString());
				tdH.appendChild(aH);
				trH.appendChild(tdH); 
				
				var trS = document.createElement("tr");
				trS.setAttribute("class", "job");
				trS.setAttribute("id", jobid.toString());
				
				var tdMod = document.createElement("td"); 
				tdMod.setAttribute("id", "mod");
				var aMod = document.createElement("a");
				aMod.innerHTML = language.periodicjobs_mod + ": ";
				tdMod.appendChild(aMod);
				var selMod = document.createElement("select");
				selMod.setAttribute("id", "mod");
				var optDay = document.createElement("option");
				optDay.setAttribute("value", "2");// day
				optDay.innerHTML = language.periodicjobs_mod_day;
				selMod.appendChild(optDay);
				var optWeek = document.createElement("option");
				optWeek.setAttribute("value", "3");// week
				optWeek.innerHTML = language.periodicjobs_mod_week;
				selMod.appendChild(optWeek);
				var selected = parseInt(Settings.periodicjobs[i].mod);
				if (selected != 2 && selected != 3) selected = 2;
				selMod.selectedIndex = selected - 2;
				tdMod.appendChild(selMod);
				trS.appendChild(tdMod);
				
				var tdDay = document.createElement("td"); 
				tdDay.setAttribute("id", "which_day");
				tdDay.setAttribute("which_day", Settings.periodicjobs[i].which_day);
				tdDay.setAttribute("day_mod", selected.toString());
				var aDay = document.createElement("a");
				aDay.innerHTML = language.periodicjobs_which_day + ": ";
				tdDay.appendChild(aDay);
				
				var selDay = document.createElement("select"); 
				selDay.setAttribute("id","which_day");
	
				for(var j = 0; j < 7; j++) {
					var opt = document.createElement("option");
					opt.setAttribute("value", j.toString());
					opt.selected = (j == parseInt(Settings.periodicjobs[i].which_day));
					opt.innerHTML = eval("language.periodicjobs_day" + j.toString());
					selDay.appendChild(opt);
				}
				
				tdDay.appendChild(selDay);
				trS.appendChild(tdDay);
				
				var tdHour = document.createElement("td"); 
				tdHour.setAttribute("id", "which_hour");
				var aHour = document.createElement("a");
				aHour.innerHTML = language.periodicjobs_which_hour + ": ";
				tdHour.appendChild(aHour);
				
				var selHour = document.createElement("select"); 
				selHour.setAttribute("id","which_hour");
	
				for(var j = 0; j < 24; j++) {
					var opt = document.createElement("option");
					opt.setAttribute("value", j.toString());
					opt.selected = (j == parseInt(Settings.periodicjobs[i].which_hour));
					opt.innerHTML = eval("language.periodicjobs_hour" + j.toString());
					selHour.appendChild(opt);
				}
				
				tdHour.appendChild(selHour);
				trS.appendChild(tdHour);
				
				var tdEnSms = document.createElement("td"); 
				var aSms = document.createElement("a");
				aSms.innerHTML = language.periodicjobs_enable_sms + ": ";
				tdEnSms.appendChild(aSms);
				var inSms = document.createElement("input");
				inSms.setAttribute("type", "checkbox");
				inSms.setAttribute("id", "enable_sms");
				inSms.checked = Settings.periodicjobs[i].enable_sms == "1";
				tdEnSms.appendChild(inSms);
				trS.appendChild(tdEnSms);
				
				var tdEnEmail = document.createElement("td"); 
				var aEmail = document.createElement("a");
				aEmail.innerHTML = language.periodicjobs_enable_email + ": ";
				tdEnEmail.appendChild(aEmail);
				var inEmail = document.createElement("input");
				inEmail.setAttribute("type", "checkbox");
				inEmail.setAttribute("id", "enable_email");
				inEmail.checked = Settings.periodicjobs[i].enable_email == "1";
				tdEnEmail.appendChild(inEmail);
				trS.appendChild(tdEnEmail);
				
				var tdButton = document.createElement("td");
				tdButton.setAttribute("class", "buttons");
				var button = document.createElement("input");
				button.setAttribute("type", "submit");
				button.setAttribute("id", jobid.toString());
				button.setAttribute("value", language.save);
				tdButton.appendChild(button);
				trS.appendChild(tdButton);
				
				if (selected == 2)
				{
					$(trS).children("td#which_day").css("display", "none");
					//$(trS).children("td#which_hour").css("display", "none");
				}
				
				table.appendChild(trH);
				table.appendChild(trS);
			break;
			case 7:
				var trH = document.createElement("tr");
				trH.setAttribute("class", "Header");
				trH.setAttribute("id", jobid.toString());
				var tdH = document.createElement("td");
				var aH = document.createElement("a");
				aH.innerHTML = eval("language.periodicjobs" + jobid.toString());
				tdH.appendChild(aH);
				trH.appendChild(tdH); 
				
				var trS = document.createElement("tr");
				trS.setAttribute("class", "job");
				trS.setAttribute("id", jobid.toString());
				
				var tdMod = document.createElement("td"); 
				tdMod.setAttribute("id", "mod");
				var aMod = document.createElement("a");
				aMod.innerHTML = language.periodicjobs_mod + ": ";
				tdMod.appendChild(aMod);
				var selMod = document.createElement("select");
				selMod.setAttribute("id", "mod");
				var optDay = document.createElement("option");
				optDay.setAttribute("value", "2");// day
				optDay.innerHTML = language.periodicjobs_mod_day;
				selMod.appendChild(optDay);
				var optWeek = document.createElement("option");
				optWeek.setAttribute("value", "3");// week
				optWeek.innerHTML = language.periodicjobs_mod_week;
				selMod.appendChild(optWeek);
				var optMonth = document.createElement("option");
				optMonth.setAttribute("value", "4");// month
				optMonth.innerHTML = language.periodicjobs_mod_month;
				selMod.appendChild(optMonth);
				var selected = parseInt(Settings.periodicjobs[i].mod);
				if (selected != 2 && selected != 3 && selected != 4) selected = 2;
				selMod.selectedIndex = selected - 2;
				tdMod.appendChild(selMod);
				trS.appendChild(tdMod);
				
				var tdDay = document.createElement("td"); 
				tdDay.setAttribute("id", "which_day");
				tdDay.setAttribute("which_day", Settings.periodicjobs[i].which_day);
				tdDay.setAttribute("day_mod", selected.toString());
				var aDay = document.createElement("a");
				aDay.innerHTML = language.periodicjobs_which_day + ": ";
				tdDay.appendChild(aDay);
				
				var selDay = document.createElement("select"); 
				selDay.setAttribute("id","which_day");
				if (selected == 3) // week
					for(var j = 0; j < 7; j++) {
						var opt = document.createElement("option");
						opt.setAttribute("value", j.toString());
						opt.selected = (j == parseInt(Settings.periodicjobs[i].which_day));
						opt.innerHTML = eval("language.periodicjobs_day" + j.toString());
						selDay.appendChild(opt);
					}
				else if (selected == 4) // month
					for(var j = 1; j <= 31; j++) {
						var opt = document.createElement("option");
						opt.setAttribute("value", j.toString());
						opt.selected = (j == parseInt(Settings.periodicjobs[i].which_day));
						opt.innerHTML = j.toString();
						selDay.appendChild(opt);
					}
				
				tdDay.appendChild(selDay);
				trS.appendChild(tdDay);
				
				var tdHour = document.createElement("td"); 
				tdHour.setAttribute("id", "which_hour");
				var aHour = document.createElement("a");
				aHour.innerHTML = language.periodicjobs_which_hour + ": ";
				tdHour.appendChild(aHour);
				
				var selHour = document.createElement("select"); 
				selHour.setAttribute("id","which_hour");
	
				for(var j = 0; j < 24; j++) {
					var opt = document.createElement("option");
					opt.setAttribute("value", j.toString());
					opt.selected = (j == parseInt(Settings.periodicjobs[i].which_hour));
					opt.innerHTML = eval("language.periodicjobs_hour" + j.toString());
					selHour.appendChild(opt);
				}
				
				tdHour.appendChild(selHour);
				trS.appendChild(tdHour);
				
				var tdEnEmail = document.createElement("td"); 
				var aEmail = document.createElement("a");
				aEmail.innerHTML = language.periodicjobs_enable_email + ": ";
				tdEnEmail.appendChild(aEmail);
				var inEmail = document.createElement("input");
				inEmail.setAttribute("type", "checkbox");
				inEmail.setAttribute("id", "enable_email");
				inEmail.checked = Settings.periodicjobs[i].enable_email == "1";
				tdEnEmail.appendChild(inEmail);
				trS.appendChild(tdEnEmail);
				
				var tdButton = document.createElement("td");
				tdButton.setAttribute("class", "buttons");
				var button = document.createElement("input");
				button.setAttribute("type", "submit");
				button.setAttribute("id", jobid.toString());
				button.setAttribute("value", language.save);
				tdButton.appendChild(button);
				trS.appendChild(tdButton);
				
				if (selected == 2)
				{
					$(trS).children("td#which_day").css("display", "none");
				//	$(trS).children("td#which_hour").css("display", "none");
				}
				
				table.appendChild(trH);
				table.appendChild(trS);
			break;
		}
	}
	
	$("div#settings div.form div#periodicjobs").html(table);
	Settings.periodicJobsArrangeColspan();
	
};

Settings.validateUpdatePeriodicJob = function(id, json)
{
	var elements = $("table#periodicjobs tr:not(.Header)#" + id + " td input[type!='submit'],\
					 table#periodicjobs tr:not(.Header)#" + id + " select");
	var alertHtml = "";
			
	$("div.form div.alert").html("");
	$("div.form input").removeClass("alert");
	var isvalid = true;
	for (var i = 0; i < elements.length; i++)
	{
		var element 	= $(elements[i]);
		var dataType 	= element.attr("data-type");
		var dataName 	= element.attr("id");
		var minval 		= element.attr("minval");
		var maxval 		= element.attr("maxval");
		var jsON = {
			type: dataType,
			min: minval,
			max: maxval
		};

		if (element.is("input") && element.attr("type") != "checkbox")
		{
			var value = element.val();
			jsON.val  = value;
			jsON.ismandatory = "true";
			jsON.obj  		 = element;
			jsON.objfocus  	 = element;
			jsON.page 		 = $("div.form div.alert");
			jsON.errprefix   = eval("language.periodicjobs_" + dataName) + ":";
			
			//validation of element
			if(!Validator.validate(jsON))
				isvalid = false;
			else if(isvalid)
				json[dataName] = value;
		}
		else if (element.is("select"))
		{
			var data_name = element.children("option:selected").attr("value");
			console.log(data_name);
			if(typeof data_name != "undefined"){
				json[dataName] = data_name.toString();
			}
			//json[dataName] = element.children("option:selected").attr("value").toString();
			if (dataName == "which_day")
			{
				$("table#periodicjobs tr:not(.Header)#" + id + " td#which_day select").closest("td").attr("which_day", json[dataName]);
				$("table#periodicjobs tr:not(.Header)#" + id + " td#which_day select").closest("td").attr("day_mod", $("table#periodicjobs tr:not(.Header)#" + id + " td#mod select option:selected").attr("value"));
			}
		}
		else if (element.is("input") && element.attr("type") == "checkbox")
		{
			json[dataName] = element.prop("checked") ? "1":"0";
		}
	}
	return isvalid;
};

Settings.updatePeriodicJob = function(id)
{
	Main.loading();	
	var json = {};
	if (!Settings.validateUpdatePeriodicJob(id, json))
	{
		Main.unloading();
		return;
	}
	if (id == 3 || id == 4 || id == 5) // resendalarms, warnings and losts
	{
		json["enable_sms"] = json["enable"];
		json["enable_email"] = json["enable"];	
		json["enable"] = undefined;
	}
	json = JSON.stringify(json);

	var url = "http://" + hostName + "/cgi-bin/periodicjobs.cgi?updatePeriodicJob&jobid=" + id;
	$.ajax({
	    url : url,
	    type: "POST",
	    data : json,
        dataType: 'text',
        contentType : 'text/json; charset=utf-8',
	    success: function(data, textStatus, jqXHR)
	    {
			Main.unloading();
			$("div.form div.alert").html("");
			$("div.form input").removeClass("alert");
		},
	    error: function (jqXHR, textStatus, errorThrown)
	    {
	    	Main.alert(language.server_error + ":" + jqXHR.responseText);
			Main.unloading();
			$("div.form div.alert").html("");
			$("div.form input").removeClass("alert");
	    }
	});
};

$(function() {
	$("div#settings").on( "click", "div.form table#periodicjobs input[type='submit']", function()
	{
		var id = $(this).attr("id");
		Settings.updatePeriodicJob(id);
	});
	
	$("div#settings").on("change", "table#periodicjobs tr:not(.Header) td#mod select", function()
	{
		var tdParent = $(this).closest("td");
		var trParent = $(this).closest("tr");
		var selected = $(this).prop("selectedIndex") + 2;
		if (selected == 2) // day
		{
			trParent.children("td#which_day").css("display", "none");
			//trParent.children("td#which_hour").css("display", "none");
		}
		else
		{
			trParent.children("td#which_day").children("select").children("option").remove();
			if (selected == 3) // week
				for(var j = 0; j < 7; j++) {
					var opt = document.createElement("option");
					opt.setAttribute("value", j.toString());
					if (trParent.children("td#which_day").attr("day_mod") == "3" && 
					trParent.children("td#which_day").attr("which_day") ==  j.toString())
						opt.selected = true;
					opt.innerHTML = eval("language.periodicjobs_day" + j.toString());
					trParent.children("td#which_day").children("select").append(opt);
				}
			else if (selected == 4) // month
				for(var j = 1; j <= 31; j++) {
					var opt = document.createElement("option");
					opt.setAttribute("value", j.toString());
					opt.innerHTML = j.toString();
					if (trParent.children("td#which_day").attr("day_mod") == "4" && 
					trParent.children("td#which_day").attr("which_day") ==  j.toString())
						opt.selected = true;
					trParent.children("td#which_day").children("select").append(opt);
				}
			trParent.children("td#which_day").css("display","table-cell");
			trParent.children("td#which_hour").css("display", "table-cell");
		}
		Settings.periodicJobsArrangeColspan();
	});
});
